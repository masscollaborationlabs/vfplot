Profiling
---------

Some scripts for profiling with [gprof][1].  To do this run

```
./configure --enable-profile
make
make profile
make spotless
```

which will create a file `prof/<version>.prof`.

The data here is only of interest to developers, so the directory
is excluded from the library distribution archives.

[1]: http://sourceware.org/binutils/docs/gprof/

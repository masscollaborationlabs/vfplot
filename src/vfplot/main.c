#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <errno.h>

#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#include <plot/units.h>
#include <log.h>

#include "options.h"
#include "vfplot.h"

static int wrap(struct gengetopt_args_info*);

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;

  if (options(argc, argv, &info) != 0)
    {
      fprintf(stderr, "failed to parse command line\n");
      return EXIT_FAILURE;
    }

  int err = wrap(&info);

  options_free(&info);

  return err;
}

static int set_verbosity(struct gengetopt_args_info*);
static int set_logging(struct gengetopt_args_info*);
static int get_options(struct gengetopt_args_info*, opt_t*);

static int wrap(struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return ERROR_OK;
    }

  if (info->version_given)
    {
      options_print_version();
      return ERROR_OK;
    }

  if (set_logging(info) != 0)
    {
      fprintf(stderr, "failed setup of logging\n");
      return EXIT_FAILURE;
    }

  if (set_verbosity(info) != 0)
    {
      fprintf(stderr, "failed setup of verbosity\n");
      return EXIT_FAILURE;
    }

  opt_t opt;

  switch (get_options(info, &opt))
    {
    case ERROR_OK: break;
    case ERROR_NODATA:
      /* eg, we printed a help message */
      return EXIT_SUCCESS;
    default:
      log_error("error processing options");
      return EXIT_FAILURE;
    }

  log_info("This is %s (version %s)", OPTIONS_PACKAGE, OPTIONS_VERSION);

  int err = vfplot(&opt);

  vfplot_clean(&opt);

  if (err != ERROR_OK)
    {
      const char *msg;

      switch (err)
        {
        case ERROR_USER: msg = "unfortunate option selection?"; break;
        case ERROR_READ_OPEN: msg = "failed to read file"; break;
        case ERROR_WRITE_OPEN: msg = "failed to write file"; break;
        case ERROR_MALLOC: msg = "out of memory"; break;
        case ERROR_BUG: msg = "probably a bug"; break;
        case ERROR_LIBGSL: msg = "error from libgsl call"; break;
        case ERROR_NODATA: msg = "no data"; break;
        case ERROR_PTHREAD: msg = "thread error"; break;
        default: msg = "unknown error - weird";
        }

      log_error("failure plotting: %s", msg);

      return EXIT_FAILURE;
    }

  log_info("done.");

  return EXIT_SUCCESS;
}

static int scan_length(const char *p, const char *name, double *x)
{
  char c;
  double u;

  switch (sscanf(p, "%lf%c", x, &c))
    {
    case 0:
      log_error("%s option missing an argument", name);
      return ERROR_USER;
    case 1:
      c = 'p';
      /* fallthrough */
    case 2:
      if ((u = unit_ppt(c)) <= 0)
        {
          log_error("unknown unit %c in %s %s", c, name, p);
          unit_list_stream(stderr);
          return ERROR_USER;
        }
      break;
    default:
      return ERROR_BUG;
    }

  *x *= u;

  return ERROR_OK;
}

/* crop is 1, 2, or 4 lengths */

static int scan_crop(const char *p, crop_t *crop)
{
  const char fmt[] = "%m[^/]/%m[^/]/%m[^/]/%m[^/]";
  char *s1, *s2, *s3, *s4;
  double W, E, S, N;
  int err;

  switch (sscanf(p, fmt, &s1, &s2, &s3, &s4))
    {
    case 1:
      if ((err = scan_length(s1, "crop", &W)) != ERROR_OK)
        return err;
      free(s1);
      E = S = N = W;
      break;

    case 2:
      if ((err = scan_length(s1, "crop", &W)) != ERROR_OK)
        return err;
      free(s1);
      E = W;
      if ((err = scan_length(s2, "crop", &S)) != ERROR_OK)
        return err;
      free(s2);
      N = S;
      break;

    case 3:
      free(s1); free(s2); free(s3);
      log_error("malformed --crop argument: %s", p);
      return ERROR_USER;

    case 4:
      if ((err = scan_length(s1, "crop", &W)) != ERROR_OK)
        return err;
      free(s1);
      if ((err = scan_length(s2, "crop", &E)) != ERROR_OK)
        return err;
      free(s2);
      if ((err = scan_length(s3, "crop", &S)) != ERROR_OK)
        return err;
      free(s3);
      if ((err = scan_length(s4, "crop", &N)) != ERROR_OK)
        return err;
      free(s4);
      break;

    default:
      return ERROR_BUG;
    }

  crop->x.min = W;
  crop->x.max = E;
  crop->y.min = S;
  crop->y.max = N;

  return ERROR_OK;
}

/*
  this simplifies processing options with several possible string argument
  and assigning an enum.

  note: we use ints throughout since enums are an implemententation-defined
  integer-type, so casting a pointer to an enum need not be an *int, it might
  be an ushort* or whatever. An int is big enough, just use temporary int and
  assign the result to the enum.
*/

typedef struct
{
  char *name, *description;
  int value;
} string_opt_t;

#define SO_NULL {NULL, NULL, 0}
#define SO_IS_NULL(x) ((x->name) == NULL)

static void string_opt_list(FILE *st, string_opt_t *ops, const char *name)
{
  int len = 0;
  for (string_opt_t *op = ops ; ! SO_IS_NULL(op) ; op++)
    {
      int n = strlen(op->name);
      if (n > len) len = n;
    }

  fprintf(st, "%s\n", name);
  for (string_opt_t *op = ops ; ! SO_IS_NULL(op) ; op++)
    fprintf(st, " - %-*s : %s\n", len, op->name, op->description);
}

static int string_opt(string_opt_t *ops,
                      const char *name,
                      const char *key,
                      int *val)
{
  if (strcmp(key, "list") == 0)
    {
      string_opt_list(stdout, ops, name);
      return ERROR_NODATA;
    }

  for (string_opt_t *op = ops ; ! SO_IS_NULL(op) ; op++)
    {
      if (strcmp(op->name, key) == 0)
        {
          *val = op->value;
          return ERROR_OK;
        }
    }

  log_error("no %s called %s", name, key);
  string_opt_list(stderr, ops, name);

  return ERROR_USER;
}

/*
  scans arg for a fill (unless given is zero) and puts the result
  in *pF
*/

static int scan_fill(int given, const char *arg, fill_t *pF)
{
  fill_t F = { .type = fill_none };

  if (given)
    {
      unsigned char k[3];

      switch (sscanf(arg, "%hhu/%hhu/%hhu", k + 0, k + 1, k + 2))
        {
        case 1:
          F.type = fill_grey;
          F.grey = k[0];
          break;

        case 3:
          F.type = fill_rgb;
          F.rgb.r = k[0];
          F.rgb.g = k[1];
          F.rgb.b = k[2];
          break;

        default :
          log_error("malformed fill %s", arg);
          return ERROR_USER;
        }
    }

  *pF = F;

  return ERROR_OK;
}

static int scan_pen(int given, const char *str, pen_t *pen)
{
  double width;
  int grey;

  if (given)
    {
      char *p;

      if ((p = strchr(str, '/')))
        {
          *p = '\0'; p++;
          grey = atoi(p);

          if ((grey < 0) || (grey > 255))
            {
              log_error("bad pen grey (%i)", grey);
              return ERROR_USER;
            }
        }
      else
        grey = 0;

      int err = scan_length(str, "pen-width", &width);

      if (err != ERROR_OK)
        return err;

      if (width < 0.0)
        {
          log_error("pen width is negative (%g)", width);
          return ERROR_USER;
        }
    }
  else
    {
      width = 0.0;
      grey = 0;
    }

  pen->width = width;
  pen->grey = grey;

  return ERROR_OK;
}

static int get_options(struct gengetopt_args_info *info, opt_t *opt)
{
  size_t nf = info->inputs_num;

  if (nf > INPUT_FILES_MAX)
    {
      options_print_help();
      log_error("sorry, at most %i input files", INPUT_FILES_MAX);
      return ERROR_USER;
    }

  opt->input.n = nf;

  for (size_t i = 0 ; i < nf ; i++)
    opt->input.paths[i] = info->inputs[i];

  /*
    vfplot options, these are the resonsibility of the vfplot
    program
  */

  opt->domain.path = info->domain_arg;
  opt->dump.vectors.path = info->dump_vectors_arg;
  opt->dump.domain.path = info->dump_domain_arg;

  opt->statistics.path = NULL;

  if (info->statistics_given)
    {
#ifdef WITH_JSON
        opt->statistics.path = info->statistics_arg;
#else
        log_error("statistics requires JSON support");
        return ERROR_USER;
#endif
    }

  opt->test = test_none;

  if (info->test_given)
    {
      string_opt_t o[] = {
        {"circular", "uniform circular field", test_circular},
        {"cylinder", "inviscid flow around a cylinder", test_cylinder},
        {"electro2", "two-point electrotatic", test_electro2},
        {"electro3", "three-point electrotatic", test_electro3},
        {"smoska", "the Smolik-Skala field", test_smoska},
        SO_NULL
      };
      int test, err = string_opt(o, "test field", info->test_arg, &test);

      if (err != ERROR_OK)
        return err;

      opt->test = test;
    }

  if (info->format_arg == NULL)
    return ERROR_BUG;
  else
    {
      int format;
      string_opt_t o[] = {
        {"auto", "automatically determine type", format_auto},
        {"csv", "comma-separated, see vfplot-csv(5)", format_csv},
        {"mat", "matlab binary format", format_mat},
        {"gfs", "gerris flow-solver simulation file", format_gfs},
        {"grd2", "pair of GMT grd files", format_grd2},
        SO_NULL
      };
      int err = string_opt(o, "format of input file", info->format_arg, &format);
      if (err != ERROR_OK)
        return err;
      opt->input.format = format;
    }

  opt->input.grid = NULL;

  if (info->grid_size_given)
    {
      field_grid_t *grid;

      if ((grid = malloc(sizeof(field_grid_t))) == NULL)
        return ERROR_MALLOC;

      size_t *size = grid->size;

      if (sscanf(info->grid_size_arg, "%zi/%zi", &(size[0]), &(size[1])) == 2)
        opt->input.grid = grid;
      else
        {
          free(grid);
          log_error("malformed --grid-size: %s", info->grid_size_arg);
          return ERROR_USER;
        }
    }

  opt->input.range = NULL;

  if (info->grid_range_given)
    {
      field_range_t *range;

      if ((range = malloc(sizeof(field_range_t))) == NULL)
        return ERROR_MALLOC;

      if (sscanf(info->grid_range_arg,
                 "%lf/%lf/%lf/%lf",
                 &(range->x.min),
                 &(range->x.max),
                 &(range->y.min),
                 &(range->y.max)) == 4)
        opt->input.range = range;
      else
        {
          free(range);
          log_error("malformed --grid-range: %s", info->grid_range_arg);
          return ERROR_USER;
        }
    }

  /* graphics state */

  opt->state.action = state_none;
  opt->state.path = NULL;

  if (info->graphic_state_given)
    {

#ifdef WITH_JSON
#ifdef HAVE_STAT

      char *path = info->graphic_state_arg;
      struct stat s;

      if (stat(path, &s) == 0)
        {
          if (! S_ISREG(s.st_mode))
            {
              log_error("path %s exists but is not regular", path);
              return ERROR_USER;
            }

          if (! (s.st_size > 0))
            {
              log_error("path %s exists but is zero sized", path);
              return ERROR_BUG;
            }

          opt->state.action = state_read;
          opt->state.path = path;
        }
      else
        {
          if (errno == ENOENT)
            {
              opt->state.action = state_write;
              opt->state.path = path;
            }
          else
            {
              log_error("stat of %s : %s", path, strerror(errno));
              return ERROR_BUG;
            }
        }

#else

      log_error("graphics state requires stat()");
      return ERROR_USER;

#endif
#else

      log_error("graphics-state requires JSON support");
      return ERROR_USER;

#endif
    }

  /*
    libplot options, these are in the vpopt_t structure contained in
    opt->plot, and this is passed to the later call to plot_adaptive(),
    plot_output() and so on
  */

  opt->plot.file.output.path = info->output_arg;

  if (info->output_format_arg == NULL)
    return ERROR_BUG;
  else
    {
      int output_format;
      string_opt_t o[] = {
        {"eps", "encapulated PostScript", output_format_eps},
        {"povray", "POV-Ray", output_format_povray},
        SO_NULL
      };
      int err = string_opt(o, "output file format",
                           info->output_format_arg, &output_format);
      if (err != ERROR_OK)
        return err;
      opt->plot.file.output.format = output_format;
    }

  if (! info->epsilon_arg)
    return ERROR_BUG;
  else
    {
      int err = scan_length(info->epsilon_arg, "epsilon", &(opt->plot.arrow.epsilon));
      if (err != ERROR_OK)
        return err;
    }

  if (! info->pen_arg)
    return ERROR_BUG;
  else
    {
      int err = scan_pen(1, info->pen_arg, &(opt->plot.arrow.pen));
      if (err != ERROR_OK)
        return err;
    }

  if (info->sort_arg == NULL)
    return ERROR_BUG;
  else
    {
      string_opt_t o[] = {
        {"none", "no sorting", sort_none},
        {"longest", "longest shaft-length", sort_longest},
        {"shortest", "shortest shaft-length", sort_shortest},
        {"bendiest", "smallest radius of curvature", sort_bendiest},
        {"straightest", "largest radius of curvature", sort_straightest},
        SO_NULL
      };
      int sort, err = string_opt(o, "sort strategy", info->sort_arg, &sort);
      if (err != ERROR_OK)
        return err;
      opt->plot.arrow.sort = sort;
    }

  if (info->symbol_arg == NULL)
    return ERROR_BUG;
  else
    {
      string_opt_t o[] = {
        {"arrow", "arrow with a curved shaft", symbol_arrow},
        {"triangle", "curved triangle", symbol_triangle},
        SO_NULL
      };
      int symbol, err = string_opt(o, "symbol type", info->symbol_arg, &symbol);
      if (err != ERROR_OK)
        return err;
      opt->plot.arrow.symbol = symbol;
    }

  if (info->aspect_given)
    {
      if (info->aspect_arg <= 0.0)
        {
          log_error("aspect (%f) must be positive", info->aspect_arg);
          return ERROR_USER;
        }
      opt->plot.arrow.aspect = info->aspect_arg;
    }
  else
    {
      switch (opt->plot.arrow.symbol)
        {
        case symbol_arrow:
          opt->plot.arrow.aspect = 8.0;
          break;
        case symbol_triangle:
          opt->plot.arrow.aspect = 4.0;
          break;
        default:
          return ERROR_BUG;
        }
    }

  {
    int err = scan_pen(info->ellipse_given,
                       info->ellipse_pen_arg,
                       &(opt->plot.ellipse.pen));
    if (err != ERROR_OK)
      return err;
  }

  {
    int err = scan_fill(info->ellipse_fill_given,
                        info->ellipse_fill_arg,
                        &(opt->plot.ellipse.fill));
    if (err != ERROR_OK)
      return err;
  }

  {
    int err = scan_fill(info->fill_given,
                        info->fill_arg,
                        &(opt->plot.arrow.fill));
    if (err != ERROR_OK)
      return err;
  }

  if (info->head_arg == NULL)
    return ERROR_BUG;
  else
    {
      if (sscanf(info->head_arg,
                 "%lf/%lf",
                 &(opt->plot.arrow.head.length),
                 &(opt->plot.arrow.head.width)
                 ) != 2)
        {
          log_error("malformed head %s", info->head_arg);
          return ERROR_USER;
        }
    }

#ifdef HAVE_PTHREAD_H

#if (defined _SC_NPROCESSORS_ONLN) && (defined HAVE_SYSCONF)
#define SCTCOUNT 1
#else
#define SCTCOUNT 0
#endif

  if (info->threads_arg < 0)
    {
      log_error("bad number of threads (%i) specified",
                info->threads_arg);
      return ERROR_USER;
    }

  /*
    this is a nonstandard macro defined on AIX systems
    but not, it seems, on linux
  */

#ifdef PTHREAD_THREADS_MAX

  if (info->threads_arg >= PTHREAD_THREADS_MAX)
    {
      log_error("too many threads (%i) specified, maximum is %i",
                info->threads_arg,
                PTHREAD_THREADS_MAX);
      return ERROR_USER;
    }

#endif

  if (info->threads_arg == 0)
    {

#if SCTCOUNT

      long nproc = sysconf(_SC_NPROCESSORS_ONLN);
      opt->plot.threads = (nproc > 0 ? nproc : 1);

#else
      if (info->thread_given)
        {
          log_error("option -j0, cannot determine the number of processors");
          log_error("present on this system, please set the number of threads");
          log_error("to use explicitly");
          return ERROR_USER;
        }
      else
        opt->plot.threads = 1;

#endif

    }
  else
    opt->plot.threads = info->threads_arg;

#else

  if (info->threads_given)
    {
      log_error("option -j: compiled without pthread support");
      return ERROR_USER;
    }

  opt->plot.threads = 1;

#endif

  opt->plot.page.type = specify_scale;
  opt->plot.page.scale = 1;

  if (info->height_given)
    {
      if (info->width_given)
        {
          log_error("only one of width or height can be specified");
          return ERROR_USER;
        }

      int err = scan_length(info->height_arg, "height",
                            &(opt->plot.page.height));
      if (err != ERROR_OK)
        return err;

      opt->plot.page.type = specify_height;
    }
  else
    {
      if (! info->width_arg)
        return ERROR_BUG;

      int err = scan_length(info->width_arg, "width",
                            &(opt->plot.page.width));
      if (err != ERROR_OK)
        return err;

      opt->plot.page.type = specify_width;
    }

  if (info->crop_given)
    {
      opt->plot.crop.active = true;
      int err = scan_crop(info->crop_arg, &(opt->plot.crop));
      if (err != ERROR_OK)
        return err;
    }
  else
    opt->plot.crop.active = false;

  if (! info->length_arg)
    return ERROR_BUG;
  else
    {
      char *p;
      int err;

      if ((p = strchr(info->length_arg, '/')))
        {
          *p = '\0'; p++;

          err = scan_length(p, "length-max",
                            &(opt->plot.arrow.length.max));
          if (err != ERROR_OK)
            return err;
        }
      else
        opt->plot.arrow.length.max = HUGE_VAL;

      err = scan_length(info->length_arg, "length-min",
                        &(opt->plot.arrow.length.min));
      if (err != ERROR_OK)
        return err;
    }

  /*
    we assign these (they both have defaults), but might not use
    the scale value if the mode is adaptive and --scale is not
    specified, see below
  */

  opt->plot.arrow.scale = info->scale_arg;
  opt->plot.arrow.n = info->number_arg;

  {
    int err = scan_pen(info->domain_pen_given, info->domain_pen_arg,
                       &(opt->plot.domain.pen));
    if (err != ERROR_OK)
      return err;
  }

  /*
    placement stategy - the opt->place enum holds the choice,
    but we need to fill in the placement-specific options
    into the libplot options opt->plot
  */

  if (info->placement_arg == NULL)
    return ERROR_BUG;
  else
    {
      string_opt_t o[] = {
        {"hedgehog", "arrows on a grid", place_hedgehog},
        {"adaptive", "adaptively placed arrows", place_adaptive},
        SO_NULL
      };

      int place;
      int err = string_opt(o, "placement strategy", info->placement_arg, &place);

      if (err != ERROR_OK)
        return err;

      /* placement specific options */

      opt->place = place;

      switch (place)
        {
        case place_hedgehog:
          opt->plot.arrow.autoscale = false;
          break;

        case place_adaptive :

          if (info->scale_given)
            {
              if (info->number_given)
                {
                  log_error("cannot specify --scale and --number in adaptive");
                  return ERROR_USER;
                }
              else
                opt->plot.arrow.autoscale = false;
            }
          else
            opt->plot.arrow.autoscale = true;

          opt->plot.place.adaptive.breakout = break_none;
          if (info->cache_arg < 2)
            {
              log_error("metric tensor cache size %i too small",
                        info->cache_arg);
              return ERROR_USER;
            }
          opt->plot.place.adaptive.mtcache = info->cache_arg;
          opt->plot.place.adaptive.histogram = info->histogram_arg;

          if (info->break_given)
            {
              /*
                infidelity - the command-line options

                   --placement hedgehog --break list

                will not give the expected results (since this code
                is not reached in that case).  Not a biggie as long as
                adaptive placement is the default though.
              */

              string_opt_t o[] = {
                {"grid", "initial grid", break_grid},
                {"super", "superposition phase", break_super},
                {"midclean", "middle of cleaning", break_midclean},
                {"postclean", "after cleaning", break_postclean},
                {"none", "no breakpoint", break_none},
                SO_NULL
              };

              int brk, err = string_opt(o, "breakpoint", info->break_arg, &brk);

              if (err != ERROR_OK)
                return err;

              opt->plot.place.adaptive.breakout = brk;
            }

          if (!info->iterations_arg)
            return ERROR_BUG;

          int k[2];

          switch (sscanf(info->iterations_arg, "%i/%i", k+0, k+1))
            {
            case 1:
              opt->plot.place.adaptive.iter.main  = k[0];
              opt->plot.place.adaptive.iter.euler = 10;
              break;
            case 2:
              opt->plot.place.adaptive.iter.main  = k[0];
              opt->plot.place.adaptive.iter.euler = k[1];
              break;
            default :
              log_error("malformed iteration %s", info->iterations_arg);
              return ERROR_USER;
            }

          opt->plot.place.adaptive.iter.populate = 0;

          const char *default_animate_dir = ".";

          if (info->animate_dir_given)
            opt->plot.place.adaptive.animate = info->animate_dir_arg;
          else if (info->animate_given)
            opt->plot.place.adaptive.animate = default_animate_dir;
          else
            opt->plot.place.adaptive.animate = NULL;

          if (info->timestep_arg <= 0)
            {
              log_error("timestep must be positive, not %g",
                        info->timestep_arg);
              return ERROR_USER;
            }

          opt->plot.place.adaptive.timestep = info->timestep_arg;

          opt->plot.place.adaptive.kedrop = info->ke_drop_arg;

          if (! info->margin_arg)
            return ERROR_BUG;
          else
            {
              double major, minor, rate;
              char *pma, *pmi;

              pma = info->margin_arg;

              if ((pmi = strchr(pma, '/')))
                {
                  *pmi = '\0'; pmi++;

                  char *pra;

                  if ((pra = strchr(pmi, '/')))
                    {
                      *pra = '\0'; pra++;
                      rate = atof(pra);
                    }
                  else
                    rate = 0.5;
                }
              else
                {
                  rate = 0.5;
                  pmi  = pma;
                }

              if ((err = scan_length(pma, "major margin", &major)) != ERROR_OK)
                return err;

              if ((err = scan_length(pmi, "minor margin", &minor)) != ERROR_OK)
                return err;

              if (!((major > 0.0) && (minor > 0.0)))
                {
                  log_error("margin: the major/minor must be positive, not %g/%g",
                            major, minor);
                  return ERROR_USER;
                }

              opt->plot.place.adaptive.margin.major = major;
              opt->plot.place.adaptive.margin.minor = minor;
              opt->plot.place.adaptive.margin.rate = rate;
            }

          if ((err = scan_pen(info->network_pen_given,
                              info->network_pen_arg,
                              &(opt->plot.place.adaptive.network.pen))) != ERROR_OK)
            return err;

          if (info->overfill_arg <= 0)
            {
              log_error("overfill value must be positive, not %f",
                        info->overfill_arg);
              return ERROR_NODATA;
            }

          opt->plot.place.adaptive.overfill = info->overfill_arg;
        }
    }

  return ERROR_OK;
}

static int set_logging(struct gengetopt_args_info *info)
{
  if (info->log_path_given)
    {
      int level = log_level_of_string(info->log_level_arg);

      if (level < 0)
        {
          fprintf(stderr, "bad log level %s\n", info->log_level_arg);
          return 1;
        }

      FILE *fd;

      if ((fd = fopen(info->log_path_arg, "a")) != NULL)
        log_add_decorated(fd, level);
      else
        {
          fprintf(stderr, "failed to open %s\n", info->log_path_arg);
          return 1;
        }
    }

  return 0;
}

static int set_verbosity(struct gengetopt_args_info *info)
{
  if (info->verbose_given)
    {
      if (info->output_given)
        log_add_plain(stdout, LOG_INFO);
      else
        {
          log_add_plain(stderr, LOG_INFO);
          log_warn("verbose output to stderr");
        }
    }
  else
    log_add_decorated(stderr, LOG_WARN);

  return 0;
}

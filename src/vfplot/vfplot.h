/*
  vfplot.h
  Copyright (c)_ J.J. Green 2007, 2021
*/

#ifndef VFPLOT_H
#define VFPLOT_H

#include <stddef.h>
#include <plot/plot.h>
#include <vf/field.h>

typedef enum {
  test_none,
  test_circular,
  test_cylinder,
  test_electro2,
  test_electro3,
  test_smoska
} test_type_t;

typedef enum { place_hedgehog, place_adaptive } place_type_t;
typedef enum { state_none, state_read, state_write } state_action_t;

typedef struct opt_t
{
  test_type_t test;
  place_type_t place;
  struct
  {
    field_format_t format;
    const field_grid_t *grid;
    const field_range_t *range;
    size_t n;
    const char *paths[INPUT_FILES_MAX];
  } input;
  struct
  {
    const char *path;
  } domain;
  struct
  {
    struct {
      const char *path;
    } vectors, domain;
  } dump;
  struct
  {
    const char *path;
  } statistics;
  struct
  {
    state_action_t action;
    const char *path;
  } state;
  plot_opt_t plot;
} opt_t;


void vfplot_clean(opt_t*);
int vfplot(opt_t*);

#endif

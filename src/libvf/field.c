#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include <log.h>

#include <plot/error.h>
#include <plot/anynan.h>

#include "vf/field.h"

#include "field-csv.h"
#include "field-grd2.h"
#include "field-mat.h"
#include "field-gfs.h"
#include "field-type.h"

bbox_t field_bbox(const field_t *field)
{
  return bilinear_bbox_get(field->v);
}

void field_destroy(field_t *field)
{
  if (field != NULL)
    {
      if (field->v)
        bilinear_destroy(field->v);
      if (field->k)
        bilinear_destroy(field->k);
    }
  free(field);
}

static field_format_t detect_format(size_t, const char**);

field_t* field_read(field_format_t format,
                    int n, const char **file,
                    const field_grid_t *grid,
                    const field_range_t *range)
{
  field_t *field = NULL;

  if (format == format_auto)
    {
      field_format_t detected_format = detect_format(n, file);

      if (detected_format == format_unknown)
        {
          log_error("failed autodetect of format - please use -F");
          return NULL;
        }

      format = detected_format;
    }

  /* we typically can't coerce the input grid size */

  if (grid != NULL)
    {
      switch (format)
        {
        case format_grd2:
        case format_gfs:
        case format_mat:
          log_error("option --grid-size not available for input format");
          return NULL;
        case format_csv:
          log_info("field grid is %zi x %zi", grid->size[0], grid->size[1]);
          break;
        default:
          log_error("unhandle grid case");
          return NULL;
        }
    }

  /*
    we probably can coerce the range for other formats, but I can't
    see an obvious use-case for it ...
  */

  if (range != NULL)
    {
      switch (format)
        {
        case format_grd2:
        case format_gfs:
        case format_mat:
          log_warn("--grid-range has no effect here");
          break;
        case format_csv:
          log_info("field range is %g/%g/%g/%g",
                   range->x.min,
                   range->x.max,
                   range->y.min,
                   range->y.max);
          break;
        default:
          log_error("unhandle range case");
          return NULL;
        }
    }

  switch (format)
    {
    case format_grd2:
      if (n != 2)
	{
	  log_error("grd2 format requires exactly 2 files, %i given", n);
	  break;
	}
      field = field_read_grd2(file[0], file[1]);
      break;

    case format_gfs:
      if (n != 1)
	{
	  log_error("gfs format requires exactly 1 file, %i given", n);
	  break;
	}
      field = field_read_gfs(file[0]);
      break;

    case format_csv:
      if (n != 1)
	{
	  log_error("CSV format requires exactly 1 file, %i given", n);
	  break;
	}
      field = field_read_csv(file[0], grid, range);
      break;

    case format_mat:
      if (n != 1)
	{
	  log_error("mat format requires exactly 1 file, %i given", n);
	  break;
	}
      field = field_read_mat(file[0]);
      break;

    default:
      log_error("unhandled read case");
      return NULL;
    }

  if (field != NULL)
    {
      bilinear_t *k;

      if ((k = bilinear_curvature(field->v)) != NULL)
	{
	  field->k = k;
	  return field;
	}

      field_destroy(field);
    }

  return NULL;
}

/*
  return the format of the files which are its arguments -- this must
  not return format_auto!
*/

typedef struct
{
  char magic[4];
  field_format_t format;
} mjrow_t;

static mjrow_t mjtab[] = {
  {{ '#',    ' ', 'G', 'e'   }, format_gfs},
  {{ 'C',    'D', 'F', '\x1' }, format_grd2},
  {{ '\x89', 'H', 'D', 'F'   }, format_grd2},
  {{ 'M',    'A', 'T', 'L'   }, format_mat},
};
static size_t nmj = sizeof(mjtab) / sizeof(mjrow_t);

static bool same_magic(char *a, char *b)
{
  for (size_t i = 0 ; i < 4 ; i++)
    if (a[i] != b[i])
      return false;
  return true;
}

/*
  detecting a CSV from the first 4 characters is not the most robust
  operation; for our expected input we would expect just integers,
  commas and spaces ... this would fail for octal or hex notation,
  should b easy to add if needed ...
*/

static bool csv_magic_char(char a)
{
  return
    (('0' <= a) && (a <= '9')) ||
    (a == ' ') ||
    (a == ',')
    ;
}

static bool csv_magic(char a[static 4])
{
  bool valid = true;

  for (size_t i = 0 ; (i < 4) && valid ; i++)
    valid &= csv_magic_char(a[i]);

  return valid;
}

static int read_magic_stream(char *m, FILE *st)
{
  for (size_t i = 0 ; i < 4 ; i++)
    {
      int j;
      if ((j = fgetc(st)) == EOF)
	return 1;
      m[i] = (unsigned char)j;
    }

  if (ferror(st))
    return 1;

  return 0;
}

static int read_magic(char *m, const char *file)
{
  FILE *st;
  int err;

  if ((st = fopen(file, "r")) == NULL)
    err = 1;
  else
    {
      err = read_magic_stream(m, st);
      fclose(st);
    }

  return err;
}

/*
  returns the common format of the array of n files, or format_unknown
  if any of the files are of different formats
*/

static field_format_t detect_format(size_t n, const char **file)
{
  if (n < 1)
    return format_unknown;

  char magic[n][4];

  for (size_t i = 0 ; i < n ; i++)
    {
      if (read_magic(magic[i], file[i]) != 0)
	return format_unknown;
    }

  field_format_t format[n];

  for (size_t i = 0 ; i < n ; i++)
    {
      format[i] = format_unknown;

      for (size_t j = 0 ; j < nmj ; j++)
	{
	  if (same_magic(magic[i], mjtab[j].magic))
            format[i] = mjtab[j].format;
	}
    }

  for (size_t i = 0 ; i < n ; i++)
    {
      if ((format[i] == format_unknown))
        if (csv_magic(magic[i]))
          format[i] = format_csv;
    }

  for (size_t i = 0 ; i < n - 1 ; i++)
    if (format[i] != format[i + 1])
      return format_unknown;

  return format[0];
}

static int vector(void *vfield, double x, double y, double *t, double *m)
{
  field_t *field = vfield;
  double v[2];

  if (bilinear_eval(field->v, x, y, v) != 0)
    return ERROR_BUG;

  if (anynan(2, v))
    return ERROR_NODATA;

  *t = atan2(v[1], v[0]);
  *m = hypot(v[1], v[0]) * field->scale;

  return ERROR_OK;
}

static int curvature(void *vfield, double x, double y, double *k)
{
  field_t *field = vfield;
  double curv[1];

  if (bilinear_eval(field->k, x, y, curv) != 0)
    return ERROR_BUG;

  if (anynan(1, curv))
    return ERROR_NODATA;

  *k = curv[0];

  return ERROR_OK;
}

static domain_t* domain(const void *arg)
{
  const field_t *f = arg;
  return bilinear_domain(f->v);
}

static void scale(void *arg, double M)
{
  field_t *field = arg;
  field->scale = M;
}

vf_methods_t field_methods =
  {
   .vector = vector,
   .curvature = curvature,
   .domain = domain,
   .scale = scale
  };

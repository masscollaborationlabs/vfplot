#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>

#include <log.h>

#include "vf/field.h"
#include "field-grd2.h"
#include "field-type.h"

#ifdef WITH_NETCDF

#include <netcdf.h>
#include <string.h>

#define ORDER_XY 1
#define ORDER_YX 2

field_t* field_read_grd2(const char *grdu, const char *grdv)
{
  const char *grd[2] = {grdu, grdv};
  int ncid[2], err;

  /* open files */

  for (size_t i = 0 ; i < 2 ; i++)
    {
      if ((err = nc_open(grd[i], NC_NOWRITE, ncid + i)) != NC_NOERR)
	{
	  log_error("grid %s : %s", grd[i], nc_strerror(err));
	  return NULL;
	}
    }

  size_t dlen[2][2];
  double dmin[2][2], dmax[2][2];
  int order[2];
  int vid[2];

  for (size_t i = 0 ; i < 2 ; i++)
    {
      /* z variable id */

      if ((err = nc_inq_varid(ncid[i], "z", &(vid[i]))) != NC_NOERR)
	{
	  log_error("error getting z id from %s: %s",
                    grd[i], nc_strerror(err));
	  return NULL;
	}

      /* get the variable's dimensions */

      int ndims;

      if ((err = nc_inq_varndims(ncid[i], vid[i], &ndims)) != NC_NOERR)
	{
	  log_error("error getting number of dimensions from %s : %s",
                    grd[i], nc_strerror(err));
	  return NULL;
	}

      if (ndims != 2)
	{
	  log_error("strange dimensions from %s (%i)", grd[i], ndims);
	  return NULL;
	}

      int dimid[2];

      if ((err = nc_inq_vardimid(ncid[i], vid[i], dimid)) != NC_NOERR)
	{
	  log_error("error getting dimensions ids from %s : %s",
                    grd[i], nc_strerror(err));
	  return NULL;
	}

      char name[2][NC_MAX_NAME];

      for (size_t j = 0 ; j < 2 ; j++)
	{
	  if ((err = nc_inq_dim(ncid[i], dimid[j], name[j], &(dlen[i][j]))) != NC_NOERR)
	    {
	      log_error("error getting dimension %zi details: %s",
                        j, nc_strerror(err));
	      return NULL;
	    }
	}

      /*
	GMT grd files usually have the dimensions of z being (y,x) but I don't
	know if this is always the case -- so here we determine the order and
	later we switch the insert order
      */

      if (strcmp("x", name[0]) == 0)
	{
	  if (strcmp("y", name[1]) == 0)
            order[i] = ORDER_XY;
	  else
	    {
	      log_error("file has no y dimension (%s instead)", name[1]);
	      return NULL;
	    }
	}
      else if (strcmp("y", name[0]) == 0)
	{
	  if (strcmp("x", name[1]) == 0)
            order[i] = ORDER_YX;
	  else
	    {
	      log_error("file has no x dimension (%s instead)", name[1]);
	      return NULL;
	    }
	}
      else
	{
	  log_error("file has no x or y dimension (%s, %s instead)",
                    name[0], name[1]);
	  return NULL;
	}

      /* read dimensional extrema for bounding box */

      for (size_t j = 0 ; j < 2 ; j++)
	{
	  size_t
            minidx = 0,
            maxidx = dlen[i][j] - 1;

          err = nc_get_var1_double(ncid[i], dimid[j], &minidx, &(dmin[i][j]));

	  if (err != NC_NOERR)
	    {
	      log_error("error getting dim %zi min from %s: %s",
                        j, grd[i], nc_strerror(err));
	      return NULL;
	    }

          err = nc_get_var1_double(ncid[i], dimid[j], &maxidx, &(dmax[i][j]));

	  if (err != NC_NOERR)
	    {
	      log_error("error getting dim %zi max from %s: %s",
                        j, grd[i], nc_strerror(err));
	      return NULL;
	    }
	}
    }

  size_t nx[2], ny[2];

  for (size_t i = 0 ; i < 2 ; i++)
    {
      if (order[i] == ORDER_XY)
        {
          nx[i] = dlen[i][0];
          ny[i] = dlen[i][1];
        }
      else
        {
          nx[i] = dlen[i][1];
          ny[i] = dlen[i][0];
        }
    }

  if ((nx[0] != nx[1]) || (ny[0] != ny[1]))
    {
      log_error("incompatible dimensions: %zi x %zi, %zi x %zi",
                nx[0], ny[0], nx[1], ny[1]);
      return NULL;
    }

  bbox_t bbox[2];

  for (size_t i = 0 ; i < 2 ; i++)
    {
      if (order[i] == ORDER_XY)
        {
          bbox[i].x.min = dmin[i][0];
          bbox[i].x.max = dmax[i][0];
          bbox[i].y.min = dmin[i][1];
          bbox[i].y.max = dmax[i][1];
        }
      else
        {
          bbox[i].x.min = dmin[i][1];
          bbox[i].x.max = dmax[i][1];
          bbox[i].y.min = dmin[i][0];
          bbox[i].y.max = dmax[i][0];
        }
    }

  if ((bbox[0].x.min != bbox[1].x.min) ||
      (bbox[0].x.max != bbox[1].x.max) ||
      (bbox[0].y.min != bbox[1].y.min) ||
      (bbox[0].y.max != bbox[1].y.max))
    {
      log_error("incompatible extents:");
      for (size_t i = 0 ; i < 2 ; i++)
        log_error("[%f, %f] x [%f, %f]",
                  bbox[i].x.min, bbox[i].x.max,
                  bbox[i].y.min, bbox[i].y.max);
      return NULL;
    }

  bilinear_t *B;

  if ((B = bilinear_new(nx[0], ny[0], 2)) == NULL)
    return NULL;

  bilinear_bbox_set(B, bbox[0]);

  for (size_t i = 0 ; i < nx[0] ; i++)
    {
      for (size_t j = 0 ; j < ny[0] ; j++)
        {
          double z[2];

          for (size_t k = 0 ; k < 2 ; k++)
            {
              size_t idx[2];

              if (order[k] == ORDER_XY)
                {
                  idx[0] = i;
                  idx[1] = j;
                }
              else
                {
                  idx[0] = j;
                  idx[1] = i;
                }

              if ((err = nc_get_var1_double(ncid[k], vid[k], idx, &(z[k]))) != NC_NOERR)
                {
                  log_error("error getting z[%zi][%zi] from %s: %s",
                            idx[0], idx[1], grd[k], nc_strerror(err));
                  goto error_exit;
                }
            }

          bilinear_z_set(B, i, j, z);
        }
    }

  for (size_t i = 0 ; i < 2 ; i++)
    nc_close(ncid[i]);

  field_t *F;

  if ((F = malloc(sizeof(field_t))) == NULL)
    goto error_exit;

  F->v = B;
  F->k = NULL;

  return F;

 error_exit:

  bilinear_destroy(B);
  return NULL;
}

#else

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

field_t* field_read_grd2(const char *grdu, const char *grdv)
{
  log_error("compiled without NetCDF support");
  return NULL;
}

#pragma GCC diagnostic pop

#endif

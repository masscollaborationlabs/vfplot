#ifndef FIELD_GRD2_H
#define FIELD_GRD2_H

#include "vf/field.h"

field_t* field_read_grd2(const char*, const char*);

#endif

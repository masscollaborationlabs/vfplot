#ifndef VF_FIELD_H
#define VF_FIELD_H

#include <vf/type.h>
#include <plot/bbox.h>

#define INPUT_FILES_MAX 2

typedef struct {
  size_t size[2];
} field_grid_t;

typedef struct {
  struct { double min, max; } x, y;
} field_range_t;

typedef enum {
  format_auto,
  format_csv,
  format_grd2,
  format_gfs,
  format_mat,
  format_unknown
} field_format_t;

typedef struct field_t field_t;

field_t* field_read(field_format_t,
                    int, const char**,
                    const field_grid_t*,
                    const field_range_t*);
void field_destroy(field_t*);
bbox_t field_bbox(const field_t*);
int field_scale(field_t*, double);

extern vf_methods_t field_methods;

#endif

#ifndef VF_SMOSKA_H
#define VF_SMOSKA_H

#include <vf/type.h>

typedef struct
{
  double scale;
} smoska_field_t;

extern vf_methods_t smoska_methods;

#endif

#ifndef VF_CYLINDER_H
#define VF_CYLINDER_H

#include <vf/type.h>

typedef struct
{
  double radius, speed, gamma, x, y, scale;
} cylinder_field_t;

extern vf_methods_t cylinder_methods;

#endif

#ifndef VF_ELECTRO_H
#define VF_ELECTRO_H

#include <vf/type.h>

typedef struct
{
  double Q, x, y;
} charge_t;

typedef struct
{
  size_t n;
  charge_t *charge;
  double scale;
} electro_field_t;

extern vf_methods_t electro_methods;

#endif

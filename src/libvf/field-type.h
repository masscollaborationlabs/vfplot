#ifndef FIELD_TYPE_H
#define FIELD_TYPE_H

#include <plot/bilinear.h>

struct field_t
{
  double scale;
  bilinear_t *v, *k;
};

#endif

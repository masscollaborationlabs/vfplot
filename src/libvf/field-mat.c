#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>

#include <log.h>

#include "vf/field.h"

#include "field-mat.h"
#include "field-type.h"

/* read matlab mat file with libmatio */

#ifdef WITH_MATLAB

#include <matio.h>

field_t* field_read_mat(const char *file)
{
  /* read matrices into dat*, an possibly ranges into rng* */

  mat_t *mat;

  if ((mat = Mat_Open(file, MAT_ACC_RDONLY)) == NULL)
    {
      log_error("failed read of %s", file);
      return NULL;
    }

  char
    *datn[2] = {"u", "v"},
    *rngn[2] = {"xrange", "yrange"};
  matvar_t
    *datv[2],
    *rngv[2];

  for (size_t i = 0 ; i < 2 ; i++)
    {
      if ((datv[i] = Mat_VarRead(mat,datn[i])) == NULL)
	{
	  log_error("failed read of matrix %s from %s", datn[i], file);
	  return NULL;
	}
    }

  for (size_t i = 0 ; i < 2 ; i++)
    rngv[i] = Mat_VarRead(mat, rngn[i]);

  Mat_Close(mat);

  /* check data is feasible */

  for (size_t i = 0 ; i < 2 ; i++)
    {
      if (rngv[i] == NULL)
        continue;

      if (rngv[i]->isComplex)
	{
	  log_error("%s is complex, expecting real", rngn[i]);
	  return NULL;
	}

      int rank;

      if ((rank = rngv[i]->rank) != 2)
	{
	  log_error("%s is not a matrix (rank %i)", rngn[i], rank);
	  return NULL;
	}

      size_t *n = rngv[i]->dims;

      if (n[0] * n[1] != 2)
	{
 	  log_error("%s not two-element (%zix%zi)", rngn[i], n[0], n[1]);
	  return NULL;
	}
    }

  for (size_t i = 0 ; i < 2 ; i++)
    {
      int rank;

      if ((rank = datv[i]->rank) != 2)
	{
	  log_error("%s is not a matrix (rank %i)", datn[i], rank);
	  return NULL;
	}
    }

  /* get the data matrices' order and check that they are the same */

  size_t dn[2];

  for (size_t i = 0 ; i < 2 ; i++)
    {
      if (datv[0]->dims[i] != datv[1]->dims[i])
	{
	  log_error("data matrices have different orders:");
	  for (size_t j = 0 ; j < 2 ; j++)
	    log_error("%s is %zi x %zi",
		    datn[j],
		    datv[j]->dims[0],
		    datv[j]->dims[1]);
	  return NULL;
	}
      dn[i] = datv[0]->dims[i];
    }

  /* read the ranges, or determine them from the data */

  double rng[2][2];

  for (size_t i = 0 ; i < 2 ; i++)
    {
      if (rngv[i])
	{
	  int ct = rngv[i]->class_type;

	  switch (ct)
	    {
	    case MAT_C_DOUBLE:
	      for (size_t j = 0 ; j < 2 ; j++)
		rng[i][j] = ((double*)(rngv[i]->data))[j];
	      break;
	    default:
	      log_error("%s class type %i not handled yet",
                        rngn[i], ct);
	      return NULL;
	    }
	}
      else
	{
	  rng[i][0] = 0;
	  rng[i][1] = dn[1];
	}
    }

  /* recover matio resources */

  for (size_t i = 0 ; i < 2 ; i++)
    if (rngv[i] != NULL)
      Mat_VarFree(rngv[i]);

  /* setup bilinear interpolants for u, v */

  bilinear_t *B;

  if ((B = bilinear_new(dn[0], dn[1], 2)) == NULL)
    return NULL;

  bbox_t bbox =
    {
     {rng[0][0], rng[0][1]},
     {rng[1][0], rng[1][1]}
    };

  bilinear_bbox_set(B, bbox);

  /* load data */

  for (size_t i = 0 ; i < dn[0] ; i++)
    {
      for (size_t j = 0 ; j < dn[1] ; j++)
        {
          size_t idx = dn[0] * j + i;
          double z[2];

          for (size_t k = 0; k < 2 ; k++)
            {
              void *data = datv[k]->data;
              int ct = datv[k]->class_type;

              switch (ct)
                {
                case MAT_C_DOUBLE:
                  z[k] = ((double*)data)[idx];
		  break;
		case MAT_C_SINGLE:
		  z[k] = ((float*)data)[idx];
		  break;
		case MAT_C_INT32:
		  z[k] = ((mat_int32_t*)data)[idx];
		  break;
		case MAT_C_UINT32:
		  z[k] = ((mat_uint32_t*)data)[idx];
		  break;
		case MAT_C_INT16:
		  z[k] = ((mat_int16_t*)data)[idx];
		  break;
		case MAT_C_UINT16:
		  z[k] = ((mat_uint16_t*)data)[idx];
		  break;
		case MAT_C_INT8:
		  z[k] = ((mat_int8_t*)data)[idx];
		  break;
		case MAT_C_UINT8:
		  z[k] = ((mat_uint8_t*)data)[idx];
		  break;
		default:
		  log_error("%s class type %i not handled yet", datn[k], ct);
                  goto error_exit;
		}
	    }
          bilinear_z_set(B, i, j, z);
	}
    }

  /* release matio resurces */

  for (size_t i = 0 ; i < 2 ; i++)
    Mat_VarFree(datv[i]);

  /* output */

  field_t *F;

  if ((F = malloc(sizeof(field_t))) == NULL)
    goto error_exit;

  F->v = B;
  F->k = NULL;

  return F;

 error_exit:

  bilinear_destroy(B);
  return NULL;
}

#else

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

field_t* field_read_mat(const char *file)
{
  log_error("compiled without Matlab file support");
  return NULL;
}

#pragma GCC diagnostic pop

#endif

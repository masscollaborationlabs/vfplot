#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdlib.h>

#include "vf/electro.h"

#define A4SCALE 3e-4

static int vector(void *arg, double x, double y, double *t, double *m)
{
  electro_field_t *field = arg;
  size_t n = field->n;
  charge_t *c = field->charge;
  double x0 = 0.0, y0 = 0.0;

  for (size_t i = 0 ; i < n ; i++)
    {
      double R = hypot(x - c[i].x, y - c[i].y);
      double R3 = R * R * R;

      x0 += c[i].Q * (x - c[i].x) / R3;
      y0 += c[i].Q * (y - c[i].y) / R3;
    }

  *t = atan2(y0, x0);
  *m = A4SCALE * hypot(x0, y0) * field->scale;

  return 0;
}

/*
   generated a domain [-1,1]x[-1,1], with as many
   holes as are needed
*/

static domain_t* domain(const void *arg)
{
  const electro_field_t *field = arg;
  domain_t *dom;

  if ((dom = domain_new()) != NULL)
    {
      bbox_t b = {{-1, 1}, {-1, 1}};
      int err = 0;

      {
        polygon_t p;

        if (polygon_rect(b, &p) != 0)
          err++;
        else
          {
            if (domain_insert(dom, &p) != 0)
              err++;
            polygon_clear(&p);
          }
      }

      for (size_t i = 0 ; i < field->n ; i++)
        {
          vec_t v = {field->charge[i].x, field->charge[i].y};
          polygon_t p;

          if (polygon_ngon(0.15, v, 64, &p) != 0)
            err++;
          else
            {
              if (domain_insert(dom, &p) != 0)
                err++;
              polygon_clear(&p);
            }
        }

      if ((err == 0) && (domain_orientate(dom) == 0))
        return dom;

      domain_destroy(dom);
    }
  return NULL;
}

static void scale(void *arg, double M)
{
  electro_field_t *electro = arg;
  electro->scale = M;
}

vf_methods_t electro_methods =
  {
   .vector = vector,
   .curvature = NULL,
   .domain = domain,
   .scale = scale
  };

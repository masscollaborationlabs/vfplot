Vector field library
--------------------

We have two sorts of vector fields

- Those which are _analytic_, defined by some formula, usually
  parameterised in some manner.  So the `cylinder`, `circular`
  and so-on

- Fields defined by _data_, we have several readers for different
  file-formats

All need to provide a minimal API which provides a _vector_ function,
when passed coordinates _(x, y)_, assigns the vector (as direction and
magnitude) at that point.  It may also provide a _curvature_ function,
which provides the (signed) curvature at that point (this is optional,
since one can calculate the curvature from the vector function).

It must also provide a _domain_ as defined in `libplot`, so a set
of polygons indicating which points are inside and which outside.
For the analytic fields this is somewhat arbitrary, but for the
data fields there is a natural domain, the boundary of the data.
In practice, the use of this domain may be overridden by the caller
(the `vfplot` option `--domain` reads a user-specified file to do
this) so the vector-field domain is a fallback.

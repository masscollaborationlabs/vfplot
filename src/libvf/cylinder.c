#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdlib.h>

#include "vf/cylinder.h"

#define A4SCALE 3e-3

static int vector(void *arg, double x, double y, double *t, double *m)
{
  cylinder_field_t *field = arg;
  double
    a = field->radius,
    G = field->gamma,
    V = field->speed,
    x0 = x - field->x,
    y0 = y - field->y,
    R2 = x0 * x0 + y0 * y0,
    R4 = R2 * R2,
    a2 = a * a;

  if (R2 < a2)
    return 1;

  double
    u = V * (1 - a2 * (x0 * x0 - y0 * y0) / R4) - G * y0 / (2 * R2 * M_PI),
    v = -V * a2 * 2 * x0 * y0 / R4 + G * x0 / (2 * R2 * M_PI);

  *t = atan2(v, u);
  *m = A4SCALE * hypot(u, v) * field->scale;

  return 0;
}

static domain_t* domain(const void *arg)
{
  const cylinder_field_t *field = arg;
  domain_t *dom;

  if ((dom = domain_new()) != NULL)
    {
      bbox_t b = {{-1, 1}, {-1, 1}};
      vec_t v = {field->x, field->y};
      int err = 0;

      {
        polygon_t p;

        if (polygon_rect(b, &p) != 0)
          err++;
        else
          {
            if(domain_insert(dom, &p) != 0)
              err++;
            polygon_clear(&p);
          }
      }

      {
        polygon_t p;

        if (polygon_ngon(field->radius, v, 32, &p) != 0)
          err++;
        else
          {
            if (domain_insert(dom, &p) != 0)
              err++;
            polygon_clear(&p);
          }
      }

      if ((err == 0) && (domain_orientate(dom) == 0))
        return dom;

      domain_destroy(dom);
    }

  return NULL;
}

static void scale(void *arg, double M)
{
  cylinder_field_t *field = arg;
  field->scale = M;
}

vf_methods_t cylinder_methods =
  {
   .vector = vector,
   .curvature = NULL,
   .domain = domain,
   .scale = scale
  };

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>

#include <log.h>

#include "vf/field.h"
#include "field-gfs.h"
#include "field-type.h"

#ifdef WITH_GERRIS

#include <gfs.h>
#include <plot/error.h>

/* 2^n for non-negative integer n < "bits per word" */

#define POW2(x) (1 << (int)(x))

/*
  there is a more-or-less identical function in (ftt_cell_bbox) in
  the ftt.c of libgfs, but using a GtsBbox_t rather than our bbox_t,
  and with 1.99999 instead of 2.0 (to avoid geometric degeneracy
  presumably)
*/

static void ftt_bbox(FttCell *cell, gpointer data)
{
  FttVector p;
  ftt_cell_pos(cell, &p);

  double size = ftt_cell_size(cell) / 2.0;
  bbox_t
    bb = {{p.x - size, p.x + size}, {p.y - size, p.y + size}},
    *pbb = *(bbox_t**)data;

  if (pbb != NULL)
    bbox_join(&bb, pbb, pbb);
  else
    {
      if ((pbb = malloc(sizeof(bbox_t))) != NULL)
        {
          *pbb = bb;
          *(bbox_t**)data = pbb;
        }
    }
}

/*
  this is a bit tricky - we want the i, j location of the centre
  point so that we can call the bilinear setz() functions, and one
  could probably do this cleverly by tracking the FTT_CELL_ID() of
  the cells as we traverse the tree. Here we hack it instead and
  calculate the (integer) i, js from the (double) x, y values of the
  centre-point.  this has caused some pain.

  the ffts_t structure is the data used by ftt_sample()
*/

typedef struct
{
  bilinear_t *B;
  int depth;
  GfsVariable *u, *v;
} ftts_t;

static void ftt_sample(FttCell *cell, gpointer data)
{
  ftts_t ftts = *((ftts_t*)data);
  int level = ftt_cell_level(cell);
  double size = ftt_cell_size(cell);
  bbox_t bbox = bilinear_bbox_get(ftts.B);

  /* the centre-point of the cell */

  FttVector p;
  ftt_cell_pos(cell, &p);

  /* the number in each directon we will sample */

  size_t n = POW2(ftts.depth - level);

  /*
    cell coordinates of p with respect to the grid with
    separation 'size' inside the bounding-box, this depends
    on the level (since 'size' does)
  */

  int
    ic = floor((p.x - bbox.x.min) / size),
    jc = floor((p.y - bbox.y.min) / size);

  /* subgrid extent */

  double
    xmin = p.x - size / 2.0,
    ymin = p.y - size / 2.0,
    d = size / n;

  for (size_t i = 0 ; i < n ; i++)
    {
      double x = xmin + (i + 0.5) * d;
      size_t ig = ic * n + i;

      for (size_t j = 0 ; j < n ; j++)
	{
	  double y = ymin + (j + 0.5) * d;
	  size_t jg = jc * n + j;

	  /*
            ig, jg are the global indicies, so give a sample
            point for the bilinear struct. Note that (x, y) and
            (x0, y0) should be the same, else the bilinear and
            octree grids are not aligned.
	  */

	  FttVector p = { .x = x, .y = y };
	  double v[2];

          v[0] = gfs_interpolate(cell, p, ftts.u);
          v[1] = gfs_interpolate(cell, p, ftts.v);

	  bilinear_z_set(ftts.B, ig, jg, v);
	}
    }
}

field_t* field_read_gfs(const char *file)
{
  int argc = 0;
  gfs_init(&argc, NULL);

  FILE *st;

  if ((st = fopen(file, "r")) == NULL)
    {
      log_error("failed to open %s", file);
      return NULL;
    }

  GtsFile *fp = gts_file_new(st);
  GfsSimulation *sim;

  if ((sim = gfs_simulation_read(fp)) == NULL)
    {
      log_error("file %s not a valid simulation file", file);
      log_error("line %d:%d: %s", fp->line, fp->pos, fp->error);
      return NULL;
    }

  gts_file_destroy(fp);
  fclose(st);

  GfsDomain *gdom = GFS_DOMAIN(sim);

  /* find the bounding box */

  bbox_t *bbox = NULL;

  gfs_domain_cell_traverse(gdom,
			   FTT_PRE_ORDER,
			   FTT_TRAVERSE_ALL,
			   0,
                           ftt_bbox,
			   &bbox);

  if (bbox == NULL)
    {
      log_error("failed to determine bounding box");
      return NULL;
    }

  /* tree depth and discretisation size */

  int
    depth = gfs_domain_depth(gdom),
    nw = round(POW2(depth) * bbox_width(bbox)),
    nh = round(POW2(depth) * bbox_height(bbox));

  /* shave bbox for node-aligned rather than pixel */

  double
    dx = bbox_width(bbox) / (2.0 * nw),
    dy = bbox_height(bbox) / (2.0 * nh);

  bbox->x.min += dx;
  bbox->x.max -= dx;
  bbox->y.min += dy;
  bbox->y.max -= dy;

  /* create & intialise mesh */

  bilinear_t *B;

  if ((B = bilinear_new(nw, nh, 2)) == NULL)
    return NULL;

  bilinear_bbox_set(B, *bbox);

  ftts_t ftts = { .B = B, .depth = depth };

  if ((ftts.u = gfs_variable_from_name(gdom->variables, "U")) == NULL)
    {
      log_error("no variable U");
      goto error_exit;
    }

  if ((ftts.v = gfs_variable_from_name(gdom->variables, "V")) == NULL)
    {
      log_error("no variable V");
      goto error_exit;
    }

  gfs_domain_cell_traverse(gdom,
			   FTT_PRE_ORDER,
			   FTT_TRAVERSE_LEAFS,
			   -1,
			   ftt_sample,
			   &ftts);

  /* clean up */

  free(bbox);
  gts_object_destroy(GTS_OBJECT(sim));

  /* results */

  field_t *F;

  if ((F = malloc(sizeof(field_t))) == NULL)
    goto error_exit;

  F->v = B;
  F->k = NULL;

  return F;

 error_exit:

  bilinear_destroy(B);
  return NULL;
}

#else

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

field_t* field_read_gfs(const char *file)
{
  log_error("compiled without Gerris (gfs) support");
  return NULL;
}

#pragma GCC diagnostic pop

#endif

#ifndef FIELD_GFS_H
#define FIELD_GFS_H

#include "vf/field.h"

field_t* field_read_gfs(const char*);

#endif

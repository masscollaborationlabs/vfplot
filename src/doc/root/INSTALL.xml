<?xml version='1.0' encoding="UTF-8"?>
<article xmlns="http://docbook.org/ns/docbook" version="5.0" xml:lang="en">

<title>vfplot source installation</title>

<section><title>Executive summary</title>

<para>
  Building vfplot is quite standard: the usual
</para>
<para><programlisting>
./configure
make
sudo make install
</programlisting></para>
<para>
  Use the <userinput>--prefix</userinput> option to
  <userinput>./configure</userinput> if you want to install
  elsewhere than <filename>/usr/local</filename>.
</para>
<para>
  See <userinput>./configure --help</userinput> for a full list of
  configuration options.
</para>

</section>

<section><title>Optional features</title>

<section><title>POSIX threads</title>
<para>
  If the <userinput>--enable-pthread</userinput> configuration option is
  given and the pthread (POSIX thread) library is found then vfplot will
  support multithreaded force calculations giving a substantial speedup on
  multiprocessor hardware.
</para>
<para>
  This option is enabled by default, if you do not want POSIX threading
  support then use the <userinput>--disable-pthread</userinput> option.
</para>
</section>

<section><title>NetCDF</title>
<para>
  If the <userinput>--enable-netcdf</userinput> configuration option
  given and the
  <ulink url="https://www.unidata.ucar.edu/software/netcdf/">NetCDF</ulink>
  library is found then vfplot will be able to read GMT grd files.
</para>
<para>
  This option is enabled by default, if you do not want NetCDF support
  then use the <userinput>--disable-netcdf</userinput> option.
</para>
</section>

<section><title>JSON</title>
<para>
  If the <userinput>--enable-json</userinput> configuration option is
  given and the
  <ulink url="https://github.com/akheron/jansson">Jansson</ulink>
  library is found then vfplot will be able to read and write JSON
  files.
</para>
<para>
  This option is enabled by default, if you do not want JSON support
  then use the <userinput>--disable-json</userinput> option.
</para>
</section>

<section><title>Matlab</title>
<para>
  If the <userinput>--enable-matlab</userinput> configuration option is
  given and the
  <ulink url="https://sourceforge.net/projects/matio/">matio</ulink>
  library is found then vfplot will be able to read mat (Matlab binary)
  files.
</para>
<para>
  This option is disabled by default.
</para>
</section>

<section><title>Gerris</title>
<para>
  If the <userinput>--enable-gerris</userinput> configuration option is
  given and the <ulink url="https://gfs.sourceforge.net/">Gerris</ulink>
  libraries are found, then vfplot will be able to read Gerris GFS
  files.
</para>
<para>
  This option is disabled by default.
</para>
</section>

</section>

<section><title>Platform specific notes</title>

<para>
  The program should compile on any modern POSIX operating system with a
  C99 compiler. It is developed on Linux on the AMD64 architectures with
  the GNU C compiler.
</para>

<section><title>Windows XP with Cygwin</title>

<para>
  Earlier version of the program were tested on Windows XP with the
  <ulink url="http://www.cygwin.com/">Cygwin</ulink> POSIX compatibility
  layer.
</para>

<para>
  To build, first use the Cygwin startup.exe program to install the gcc
  and make packages. Then obtain, compile and install the netcdf and
  libmatio libraries listed above. Finally, configure with
  <userinput>--disable-pthread</userinput> then make and install as usual.
</para>

</section>

<section><title>OS X</title>

<para>
  The POSIX thread (pthread) library supplied with the OS does not
  include some functionality on which vfplot relies (in particular,
  <emphasis>barriers</emphasis>).  That functionality can be provided
  by a built-in implementation with the
  <userinput>--enable-pthread-extra</userinput>
  configuration option.
</para>

</section>

</section>

</article>

/*
  main.c
  J.J.Green 2007, 2021
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "options.h"
#include "gfs-csv.h"

#include <log.h>

#include <stdlib.h>
#include <stdio.h>

static int wrap(struct gengetopt_args_info*);

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;

  if (options(argc, argv, &info) != 0)
    {
      fprintf(stderr, "failed to parse command line\n");
      return EXIT_FAILURE;
    }

  int err = wrap(&info);

  options_free(&info);

  return err;
}

static int get_options(struct gengetopt_args_info*, gfs_csv_t*);

static int wrap(struct gengetopt_args_info *info)
{
  gfs_csv_t opt;

  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  if (get_options(info, &opt) != 0)
    {
      fprintf(stderr, "error processing options\n");
      return EXIT_FAILURE;
    }

  if (opt.verbose)
    log_add_plain(stdout, LOG_INFO);
  else
    log_add_decorated(stderr, LOG_WARN);

  log_info("This is %s (version %s)", OPTIONS_PACKAGE, OPTIONS_VERSION);

  int err = gfs_csv(&opt);

  if (err == 0)
    {
      log_info("done.");
      return EXIT_SUCCESS;
    }
  else
    {
      log_warn("failure");
      return EXIT_FAILURE;
    }
}

static int get_options(struct gengetopt_args_info *info, gfs_csv_t *opt)
{
  size_t nfile = info->inputs_num;

  if (nfile > 1)
    {
      fprintf(stderr, "sorry, at most 1 input file\n");
      return 1;
    }

  opt->file.in = (nfile ? info->inputs[0] : NULL);
  opt->file.out = (info->output_given ? info->output_arg : NULL);
  opt->verbose = info->verbose_flag;
  opt->index = info->index_flag;

  if ((opt->scalar.n = info->scalar_given) == 0)
    {
      fprintf(stderr, "at least one --scalar required\n");
      return 1;
    }

  if ((opt->scalar.v = info->scalar_arg) == NULL)
    {
      fprintf(stderr, "scalar agument null?\n");
      return 1;
    }

  if ((opt->delimiter = info->delimiter_arg) == NULL)
    {
      fprintf(stderr, "no delimiter\n");
      return 1;
    }

  if (opt->verbose && (! opt->file.out))
    {
      fprintf(stderr, "can't have verbose output without -o option!\n");
      return 1;
    }

  return 0;
}

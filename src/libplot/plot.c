#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/plot.h"
#include "plot/limits.h"
#include "plot/status.h"
#include "plot/postscript.h"
#include "plot/macros.h"
#include "plot/error.h"

#include <geom2d/vec.h>
#include <geom2d/sincos.h>

#include <log.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <assert.h>

#define DEG_PER_RAD (180.0/M_PI)
#define ELLIPSE_GREY 0.7

static int longest(const void *a, const void *b)
{
  return ((const arrow_t*)a)->length > ((const arrow_t*)b)->length;
}

static int shortest(const void *a, const void *b)
{
  return ((const arrow_t*)a)->length < ((const arrow_t*)b)->length;
}

static int bendiest(const void *a, const void *b)
{
  return ((const arrow_t*)a)->curv > ((const arrow_t*)b)->curv;
}

static int straightest(const void *a, const void *b)
{
  return ((const arrow_t*)a)->curv < ((const arrow_t*)b)->curv;
}

int plot_opt_init(bbox_t b, plot_opt_t *opt)
{
  int err;

  if ((err = page_complete(&b, &(opt->page))) != ERROR_OK)
    return err;

  opt->bbox = b;

  log_info("plot geometry %.0f x %.0f pt",
           opt->page.width,
           opt->page.height);

  return ERROR_OK;
}

static double ucunit(unsigned char c)
{
  return c / 255.0;
}

/* callbackss for domain iterate */

static int vdwe_polygon(FILE *st, polygon_t *p)
{
  size_t n = p->n;
  const vec_t *v = p->v;

  if (n < 2)
    return ERROR_BUG;

  fprintf(st, "newpath\n");
  fprintf(st, "%.2f %.2f moveto\n", v[0].x, v[0].y);
  for (size_t i = 1 ; i < n ; i++)
    fprintf(st, "%.2f %.2f lineto\n", v[i].x, v[i].y);
  fprintf(st, "closepath\n");
  fprintf(st, "stroke\n");

  return 0;
}

static int vdwp_polygon(FILE *st, polygon_t *p)
{
  size_t n = p->n;
  const vec_t *v = p->v;

  if (n < 2)
    return ERROR_BUG;

  fprintf(st, "merge {\n");

  for (size_t i = 0 ; i < n - 1 ; i++)
    {
      fprintf(st, "D(%.2f, %.2f, %.2f, %.2f)\n",
              v[i].x, v[i].y,
              v[i + 1].x, v[i + 1].y);
    }

  fprintf(st, "D(%.2f, %.2f, %.2f, %.2f)\n",
          v[n - 1].x, v[n - 1].y,
          v[0].x, v[0].y);

  fprintf(st, "}\n");

  return 0;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int vdwe_node(polygon_t *p, void *arg, int level)
{
  FILE *fd = arg;
  return vdwe_polygon(fd, p);
}

#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int vdwp_node(polygon_t *p, void *arg, int level)
{
  FILE *fd = arg;
  return vdwp_polygon(fd, p);
}

#pragma GCC diagnostic pop

static int plot_domain_write_eps(FILE *st, const domain_t *dom, pen_t pen)
{
  fprintf(st, "gsave\n");
  fprintf(st, "%.2f setlinewidth\n", pen.width);
  fprintf(st, "%.2f setgray\n", pen.grey / 255.0);
  fprintf(st, "%i setlinejoin\n", PS_LINEJOIN_MITER);

  int err = domain_each((domain_t*)dom, vdwe_node, st);

  fprintf(st, "grestore\n");

  return err;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int plot_domain_write_povray(FILE *st, const domain_t *dom, pen_t pen)
{
  fprintf(st, "object {\n");
  fprintf(st, "union {\n");

  int err = domain_each((domain_t*)dom, vdwp_node, st);

  fprintf(st, "}\n");
  fprintf(st, "texture { vfplot_domain_texture }\n");
  fprintf(st, "}\n");

  return err;
}

#pragma GCC diagnostic pop

/*
  distance between (x, y) and (x cos t, x sin t) where xt = y. this is well
  approximated by y^2/2x (up to t^2 in taylor) if it needs to be faster
*/

static double aberration(double x, double y)
{
  double t = y / x;
  double st, ct;

  sincos(t, &st, &ct);

  return hypot(x * (1 - ct), y - x * st);
}

static int timestring(int n, char *buf)
{
  time_t t;

  time(&t);

  struct tm *tm = gmtime(&t);

  return (strftime(buf, n, "%a %d %b %Y, %H:%M:%S %Z", tm) ? 0 : 1);
}

/* utility structs for the plot_<format> functions */

typedef struct
{
  bool_t arrows, ellipses, domain, network;
} draw_stroke_t;

typedef struct
{
  bool_t arrows, ellipses;
} draw_fill_t;

typedef struct
{
  bool_t arrows, ellipses, domain, network;
} draw_t;

#define TIMESTRING_LEN 32

static int plot_eps(FILE *st,
                    const domain_t *dom,
                    const arrows_t *A,
                    const nbrs_t *N,
                    const plot_opt_t *opt,
                    bool report)
{
  draw_fill_t fill;
  draw_stroke_t stroke;
  draw_t draw;

  stroke.arrows = (opt->arrow.pen.width > 0);
  stroke.ellipses = (opt->ellipse.pen.width > 0);
  stroke.domain = (opt->domain.pen.width > 0);
  stroke.network = (opt->place.adaptive.network.pen.width > 0);

  fill.arrows = (opt->arrow.fill.type != fill_none);
  fill.ellipses = (opt->ellipse.fill.type != fill_none);

  draw.network = stroke.network && (N->n > 1);
  draw.domain = stroke.domain;
  draw.arrows = draw.ellipses = (A->n > 0);
  draw.arrows &= (stroke.arrows || fill.arrows);
  draw.ellipses &= (stroke.ellipses || fill.ellipses);

  /* this needed if we draw the ellipses */

  arrow_opt_t arrow_opt = {
    .M = opt->place.adaptive.margin.rate,
    .bmaj = opt->place.adaptive.margin.major,
    .bmin = opt->place.adaptive.margin.minor,
    .scale = 1.0
  };

  /* file header */

  int PSlevel = 1;
  double margin = opt->domain.pen.width / 2;
  const page_t *page = &(opt->page);
  const crop_t *crop = &(opt->crop);
  double xmin, xmax, ymin, ymax;

  if (crop->active)
    {
      xmin = crop->x.min - margin;
      ymin = crop->y.min - margin;
      xmax = page->width - crop->x.max + margin;
      ymax = page->height - crop->y.max + margin;
    }
  else
    {
      xmin = -margin;
      ymin = -margin;
      xmax = page->width + margin;
      ymax = page->height + margin;
    }

  char tmstr[TIMESTRING_LEN];

  if (timestring(TIMESTRING_LEN, tmstr) != 0)
    log_error("output timestring truncated to %s", tmstr);

  fprintf(st,
          "%%!PS-Adobe-3.0 EPSF-3.0\n"
          "%%%%BoundingBox: %i %i %i %i\n"
          "%%%%Title: %s\n"
          "%%%%Creator: %s (version %s)\n"
          "%%%%CreationDate: %s\n"
          "%%%%LanguageLevel: %i\n"
          "%%%%EndComments\n",
          (int)floor(xmin),
          (int)floor(ymin),
          (int)ceil(xmax),
          (int)ceil(ymax),
          (opt->file.output.path ? opt->file.output.path : "<stdout>"),
          "vfplot",
          VERSION,
          tmstr,
          PSlevel);

  /* prologue */

  fprintf(st, "%i dict begin\n", 50);

  /*
    arrow fill definitions which depend on symbol type and plot descriptors - we
    generate a fill/stroke command for later use in the CL, CR and S macros.
  */

  size_t fcn = 256;
  char fillcmd[fcn];

  switch (opt->arrow.fill.type)
    {
    case fill_none:
      if (stroke.arrows)
        snprintf(fillcmd, fcn, "stroke");
      break;

    case fill_grey:
      if (stroke.arrows)
        {
          if (fill.arrows)
            snprintf(fillcmd, fcn,
                     "gsave %.3f setgray fill grestore stroke",
                     ucunit(opt->arrow.fill.grey));
          else
            snprintf(fillcmd, fcn, "stroke");
        }
      else
        {
          if (fill.arrows)
            snprintf(fillcmd, fcn,
                     "%.3f setgray fill",
                     ucunit(opt->arrow.fill.grey));
        }
      break;

    case fill_rgb:
      if (stroke.arrows)
        {
          if (fill.arrows)
            snprintf(fillcmd, fcn,
                     "gsave %.3f %.3f %.3f setrgbcolor fill grestore stroke",
                     ucunit(opt->arrow.fill.rgb.r),
                     ucunit(opt->arrow.fill.rgb.g),
                     ucunit(opt->arrow.fill.rgb.b));
          else
            snprintf(fillcmd, fcn, "stroke");
        }
      else
        {
          if (fill.arrows)
            snprintf(fillcmd, fcn,
                     "%.3f %.3f %.3f setrgbcolor fill",
                     ucunit(opt->arrow.fill.rgb.r),
                     ucunit(opt->arrow.fill.rgb.g),
                     ucunit(opt->arrow.fill.rgb.b));
        }
      break;
    }

  /* per-symbol definitions */

  switch (opt->arrow.symbol)
    {
    case symbol_arrow:
      fprintf(st,
              "/HLratio %.3f def\n"
              "/HWratio %.3f def\n",
              opt->arrow.head.length,
              opt->arrow.head.width);
      break;

    case symbol_triangle:
      fprintf(st,
              "/tan {dup sin 2 1 roll cos div} def\n"
              "/RAD {57.295779 div} def\n");
      break;
    }

  /* arrow prologue */

  if (draw.arrows)
    {
      switch (opt->arrow.symbol)
	{
	case symbol_arrow:
          fprintf(st,
                  "/CLR {\n"
                  "gsave\n"
                  "/yr exch def\n"
                  "translate rotate\n"
                  "1 yr scale\n"
                  "/rm exch def\n"
                  "/phi exch def\n"
                  "/sw exch def\n"
                  "/sw2 sw 2 div def\n"
                  "/hw2 sw2 HWratio mul def\n"
                  "/hl sw HLratio mul def\n"
                  "/rso rm sw2 add def\n"
                  "/rsi rm sw2 sub def\n"
                  "/rho rm hw2 add def\n"
                  "/rhi rm hw2 sub def\n"
                  "0 0 rso 0 phi arc\n"
                  "0 0 rsi phi 0 arcn\n"
                  "rhi 0 lineto\n"
                  "rm hl neg lineto\n"
                  "rho 0 lineto closepath\n"
                  "%s\n"
                  "grestore } def\n", fillcmd);
          break;

	case symbol_triangle:

          /*
            the position of the bezier control points in the
            following is rather subtle. In the case of the
            best approximation of a circular arc by a Bezier
            curve, Goldapp [1] shows that the control points
            should be tangent to the circle at the endpoints
            and a distance hr away where

              h = 4/3 tan t/4

            and r is the radius. Note that

              hr = (1/3 t +  1/144 t^3 + ..)r
		 = L/3 + ...

            When we consider a circular arc with r0 r1 as
            radii and width w = r0-r1 we find good results
            with L/3 and w/3 as the offsets -- when we
            upgrade to

              L/3 -> rh
              w/3 -> wh/t

            we get even better results (visually, possibly
            optimally). See also the file wedge.eps in this
            package.

            [1] M. Goldapp "Approximation of circular arcs by
            cubic polynomials"  Computer Aided Geometric
            Design, 5 (1991), 227-238
          */

          fprintf(st,
                  "/CLR {\n"
                  "gsave\n"
                  "/yr exch def\n"
                  "translate rotate\n"
                  "1 yr scale\n"
                  "/rm exch def\n"
                  "/t exch def\n"
                  "/w exch def\n"
                  "%s"
                  "/w2 w 2 div def\n"
                  "/ro rm w2 add def\n"
                  "/ri rm w2 sub def\n"
                  "/h 4 3 div t 4 div tan mul def\n"
                  "/rmh rm h mul def\n"
                  "/wh2t w2 h mul t RAD div def\n"
                  "/ct t cos def\n"
                  "/st t sin def\n"
                  "newpath\n"
                  "ro 0 moveto\n"
                  "ro wh2t sub\n"
                  "ro h mul\n"
                  "rm wh2t add ct mul rmh st mul add\n"
                  "rm wh2t add st mul rmh ct mul sub\n"
                  "rm ct mul\n"
                  "rm st mul\n"
                  "curveto\n"
                  "rm wh2t sub ct mul rmh st mul add\n"
                  "rm wh2t sub st mul rmh ct mul sub\n"
                  "ri wh2t add\n"
                  "ri h mul\n"
                  "ri 0\n"
                  "curveto\n"
                  "closepath\n"
                  "%s\n"
                  "grestore } def\n",
                  "1 -1 scale t neg rotate\n",
                  fillcmd);
          break;
	}

      fprintf(st, "/CL {-1 CLR} def\n");
      fprintf(st, "/CR {1  CLR} def\n");

      switch (opt->arrow.symbol)
	{
	case symbol_arrow:
          fprintf(st,
                  "/S {\n"
                  "gsave\n"
                  "translate rotate\n"
                  "/length exch def\n"
                  "/shaftwidth exch def\n"
                  "/l2 length 2 div def\n"
                  "/sw2 shaftwidth 2 div def\n"
                  "/hw2 sw2 HWratio mul def\n"
                  "/hl shaftwidth HLratio mul def\n"
                  "l2 sw2 moveto \n"
                  "l2 neg sw2 lineto\n"
                  "l2 neg sw2 neg lineto\n"
                  "l2 sw2 neg lineto\n"
                  "l2 hw2 neg lineto\n"
                  "l2 hl add 0 lineto\n"
                  "l2 hw2 lineto\n"
                  "closepath\n"
                  "%s\n"
                  "grestore } def\n", fillcmd);
	  break;

	case symbol_triangle:
          fprintf(st,
                  "/S {\n"
                  "gsave\n"
                  "translate rotate\n"
                  "/length exch def\n"
                  "/width exch def\n"
                  "/l2 length 2 div def\n"
                  "/w2 width 2 div def\n"
                  "l2 neg w2 moveto \n"
                  "l2 0 lineto\n"
                  "l2 neg w2 neg lineto\n"
                  "closepath\n"
                  "%s\n"
                  "grestore } def\n", fillcmd);
	  break;
	}
    }

  /* ellipse prologue */

  if (draw.ellipses)
    {
      fprintf(st,
              "/E {\n"
              "/y exch def\n"
              "/x exch def\n"
              "/yrad exch def\n"
              "/xrad exch def\n"
              "/theta exch def\n"
              "/savematrix matrix currentmatrix def\n"
              "x y translate\n"
              "theta rotate\n"
              "xrad yrad scale\n"
              "0 0 1 0 360 arc\n"
              "savematrix setmatrix\n");

      switch (opt->ellipse.fill.type)
        {
        case fill_none:
          break;

        case fill_grey:
          if (stroke.ellipses)
            {
              if (fill.ellipses)
                fprintf(st,
                        "gsave %.3f setgray fill grestore\n",
                        ucunit(opt->ellipse.fill.grey));
              fprintf(st, "stroke\n");
            }
          else
            {
              if (fill.ellipses)
                fprintf(st,
                        "%.3f setgray fill\n",
                        ucunit(opt->ellipse.fill.grey));
              else
                return ERROR_BUG;
            }
          break;

        case fill_rgb:
          fprintf(st,
                  "gsave %.3f %.3f %.3f setrgbcolor fill grestore\n",
                  ucunit(opt->ellipse.fill.rgb.r),
                  ucunit(opt->ellipse.fill.rgb.g),
                  ucunit(opt->ellipse.fill.rgb.b));
          break;
        }

      if (stroke.ellipses)
        fprintf(st, "stroke\n");

      fprintf(st, "} def\n");
    }

  /* the plot object */

  struct {
    size_t circular, straight, toolong, tooshort, toobendy;
  } count = {0, 0, 0, 0, 0};

  /* start clip */

  if (crop->active)
    {
      fprintf(st, "gsave\n");
      fprintf(st, "%.2f %.2f moveto\n", xmin, ymin);
      fprintf(st, "0 %.2f rlineto\n", ymax - ymin);
      fprintf(st, "%.2f 0 rlineto\n", xmax - xmin);
      fprintf(st, "0 %.2f rlineto\n", ymin - ymax);
      fprintf(st, "closepath\n");
      fprintf(st, "clip\n");
      fprintf(st, "newpath\n");
    }

  /* domain */

  if (draw.domain)
    {
      if (plot_domain_write_eps(st, dom, opt->domain.pen))
        return ERROR_BUG;
    }

  /* ellipses */

  if (draw.ellipses)
    {
      fprintf(st, "gsave\n");

      if (stroke.ellipses)
        {
          fprintf(st, "%.2f setlinewidth\n", opt->ellipse.pen.width);
          fprintf(st, "%i setlinejoin\n", PS_LINEJOIN_ROUND);
          fprintf(st, "%.3f setgray\n", opt->ellipse.pen.grey / 255.0);
        }

      for (size_t i = 0 ; i < A->n ; i++)
        {
          ellipse_t e;

          arrow_ellipse(A->v + i, &arrow_opt, &e);
          fprintf(st, "%.2f %.2f %.2f %.2f %.2f E\n",
                  e.theta*DEG_PER_RAD + 180.0,
                  e.major,
                  e.minor,
                  e.centre.x,
                  e.centre.y);
        }

      fprintf(st, "grestore\n");
    }

  /* network */

  if (draw.network)
    {
      pen_t pen = opt->place.adaptive.network.pen;

      fprintf(st, "gsave\n");
      fprintf(st, "%.2f setlinewidth\n", pen.width);
      fprintf(st, "%i setlinecap\n", PS_LINECAP_ROUND);
      fprintf(st, "%.3f setgray\n", pen.grey / 255.0);

      for (size_t i = 0 ; i < N->n ; i++)
        {
          vec_t
            w0 = N->v[i].a.v,
            w1 = N->v[i].b.v;

          fprintf(st, "newpath\n");
          fprintf(st, "%.2f %.2f moveto\n", w0.x, w0.y);
          fprintf(st, "%.2f %.2f lineto\n", w1.x, w1.y);
          fprintf(st, "closepath\n");
          fprintf(st, "stroke\n");
        }

      fprintf(st, "grestore\n");
    }

  /* arrows */

  if (draw.arrows)
    {
      fprintf(st, "gsave\n");

      if (stroke.arrows)
        {
          fprintf(st, "%.2f setlinewidth\n", opt->arrow.pen.width);
          fprintf(st, "%i setlinejoin\n", PS_LINEJOIN_ROUND);
          fprintf(st, "%.3f setgray\n", opt->arrow.pen.grey / 255.0);
        }

      for (size_t i = 0 ; i < A->n ; i++)
	{
	  arrow_t a = A->v[i];

	  /* skip arrows with small radius of curvature */

	  if (a.curv > 1 / RADCRV_MIN)
	    {
	      count.toobendy++;
	      continue;
	    }

	  if (a.length > opt->arrow.length.max)
	    {
	      count.toolong++;
	      continue;
	    }

	  if (a.length < opt->arrow.length.min)
	    {
	      count.tooshort++;
	      continue;
	    }

	  /* tuncate arrows which are too snakey */

	  double
            cmax = 2 * M_PI * CIRCULARITY_MAX,
            psi = a.length * a.curv;

	  if (psi > cmax)
	    {

              /* see src/misc/clang-fpe */

#ifdef __clang__
              assert(a.curv != 0);
#endif

	      psi = cmax;
	      a.length = cmax / a.curv;
	    }

	  /*
	    head correction, we place the arrow's head a distance from the
            end of the shaft -- the distance is chosen so the area of shaft
            lost is equal to the area of the head.
	  */

	  double hc = 0;

	  switch (opt->arrow.symbol)
	    {
	    case symbol_arrow:
	      hc = 0.5 * opt->arrow.head.length * opt->arrow.head.width * a.width;
	      break;
	    case symbol_triangle:
	      hc = 0.0;
	      break;
	    }

	  /*
	    Decide between straight and curved arrow.  We draw a straight
            arrow if the ends of the stem differ from the curved arrow by
            less than user epsilon.
	  */

	  double sth, cth;
	  sincos(a.theta, &sth, &cth);

          double eps = opt->arrow.epsilon;

          if ((a.curv > 0) &&
              (aberration((1 / a.curv) + (a.width / 2), a.length / 2) > eps))
	    {
	      /* circular arrow */

	      double
                r = 1 / a.curv,
                R = r * cos(psi / 2);

              /* xi is the angle accounting for the head area */

              double xi = hc * a.curv;

              switch (a.bend)
                {
                case rightward:
                  fprintf(st, "%.2f %.2f %.2f %.2f %.2f %.2f CR\n",
                          a.width,
                          (psi - xi) * DEG_PER_RAD,
                          r,
                          (a.theta - psi / 2 + xi) * DEG_PER_RAD + 90.0,
                          a.centre.x + R * sth,
                          a.centre.y - R * cth);
                  break;

                case leftward:
                  fprintf(st, "%.2f %.2f %.2f %.2f %.2f %.2f CL\n",
                          a.width,
                          (psi -xi) * DEG_PER_RAD,
                          r,
                          (a.theta + psi / 2 - xi) * DEG_PER_RAD - 90.0,
                          a.centre.x - R * sth,
                          a.centre.y + R * cth);
                  break;
                }
              count.circular++;
            }
          else
            {
              /*
                straight arrow - our postscript function S draws an arrow who's
                shaft is centred at (x0, y0), so to account for the head and
                to centre the headed arrow in the ellipse we need to shift the
                centre by hc/2 in the direction opposite to the arrow direction
              */

              fprintf(st, "%.2f %.2f %.2f %.2f %.2f S\n",
                      a.width,
                      a.length - hc,
                      a.theta * DEG_PER_RAD,
                      a.centre.x - hc * cth / 2,
                      a.centre.y - hc * sth / 2);

              count.straight++;
            }
        }

      fprintf(st, "grestore\n");
    }

  /* end clip */

  if (crop->active)
    fprintf(st, "grestore\n");

  /* footer */

  fprintf(st, "end\n");

  /* end file */

  fprintf(st,
          "showpage\n"
          "%%%%EOF\n");

  /* user info if reporting enabled */

  if (report)
    {
      log_info("output");

      status("total", A->n);
      status("circular", count.circular);
      status("straight", count.straight);

      if (count.toolong) status("too long", count.toolong);
      if (count.tooshort) status("too short", count.tooshort);
      if (count.toobendy) status("too curved", count.toobendy);
    }

  return ERROR_OK;
}

static int plot_pov(FILE *st,
                    const domain_t *dom,
                    const arrows_t *A,
                    const nbrs_t *N,
                    const plot_opt_t *opt, bool report)
{
  draw_fill_t fill;
  draw_stroke_t stroke;
  draw_t draw;

  stroke.arrows = (opt->arrow.pen.width > 0.0);
  stroke.ellipses = (opt->ellipse.pen.width > 0.0);
  stroke.domain = (opt->domain.pen.width > 0.0);
  stroke.network = (opt->place.adaptive.network.pen.width > 0.0);

  fill.arrows = (opt->arrow.fill.type != fill_none);
  fill.ellipses = (opt->ellipse.fill.type != fill_none);

  draw.network = stroke.network && (N->n > 1);
  draw.domain = stroke.domain;
  draw.arrows = draw.ellipses = (A->n > 0);
  draw.arrows &= fill.arrows;
  draw.ellipses &= fill.ellipses;

  /*
    warn ineffective options, note that we don't do this for the
    arrow stroke since that is enabled by default and the user
    would need to add --pen 0 to supress the warning, perhaps
    move the default out of options.ggo and into main.c (but
    then you don't get the value with --help)
  */

  if (stroke.ellipses)
    log_warn("ellipse pen has no effect in POV-Ray output");

  /* this needed if we draw the ellipses */

  arrow_opt_t arrow_opt = {
    .M = opt->place.adaptive.margin.rate,
    .bmaj = opt->place.adaptive.margin.major,
    .bmin = opt->place.adaptive.margin.minor,
    .scale = 1.0
  };

  /* file header */

  char tmstr[TIMESTRING_LEN];

  if (timestring(TIMESTRING_LEN, tmstr) != 0)
    log_warn("output timestring truncated to %s", tmstr);

  fprintf(st,
          "/*\n"
          "  %s\n"
          "  output from %s (version %s)\n"
          "  %s\n"
          "*/\n",
          (opt->file.output.path ? opt->file.output.path : "stdout"),
          "vfplot", VERSION,
          tmstr);

  /* prologue */

  fprintf(st, "#include \"shapes.inc\"\n");

  /* global definitions */

  fprintf(st,
          "#ifndef (vfplot_edge_radius)\n"
          "#declare vfplot_edge_radius = 0.1;\n"
          "#end\n"
          "#local ER = vfplot_edge_radius;\n");

  /*
    arrow fill definitions which depend on symbol type and plot
    descriptors - for eps we generate a fill/stroke command for
    later use in the CL, CR and S macros, for povray we write
    out a texture declaration
  */

  switch (opt->arrow.fill.type)
    {
    case fill_none:
      break;

    case fill_grey:
      fprintf(st,
              "#ifndef (vfplot_arrow_texture)\n"
              "#declare vfplot_arrow_texture = \n"
              "  texture {\n"
              "    pigment { color rgb %.3f }\n"
              "  };\n"
              "#end\n",
              ucunit(opt->arrow.fill.grey));
      break;

    case fill_rgb:
      fprintf(st,
              "#ifndef (vfplot_arrow_texture)\n"
              "#declare vfplot_arrow_texture =\n"
              "  texture {\n"
              "    pigment { color rgb <%.3f, %3f, %3f> }\n"
              "  };\n"
              "#end\n",
              ucunit(opt->arrow.fill.rgb.r),
              ucunit(opt->arrow.fill.rgb.g),
              ucunit(opt->arrow.fill.rgb.b));
      break;
    }

  /* per-symbol definitions */

  switch (opt->arrow.symbol)
    {
    case symbol_arrow:
      fprintf(st,
              "#local HL = %.3f;\n"
              "#local HW = %.3f;\n",
              opt->arrow.head.length,
              opt->arrow.head.width);
      break;

    case symbol_triangle:
      break;
    }

  /* arrow prologue */

  if (draw.arrows)
    {
      switch (opt->arrow.symbol)
        {
        case symbol_arrow:
          fprintf(st,
                  "#macro CLR(X, Y, theta, R, phi, W, SGN)\n"
                  "#ifdef (vfplot_clr)\n"
                  "  vfplot_clr(X, Y, theta, R, phi, W, SGN)\n"
                  "#else\n"
                  "  object{\n"
                  "    merge {\n"
                  "      intersection {\n"
                  "            torus {R, W/2}\n"
                  "            Wedge(phi)\n"
                  "            rotate <90, 0, 90>\n"
                  "      }\n"
                  "      Round_Cone_Merge(<R, 0, 0>, HW*W/2, <R, (ER-HL)*W, 0>, ER*W, ER*W)\n"
                  "      object {\n"
                  "            Round_Cylinder_Merge(<R, -ER*W/2, 0>, <R, ER*W, 0>, W/2, ER*W/2)\n"
                  "            rotate <0, 0, phi>\n"
                  "      }\n"
                  "    }\n"
                  "    scale <1, SGN, 1>\n"
                  "    rotate <0, 0, theta>\n"
                  "    translate <X, Y, 0>\n"
                  "  }\n"
                  "#end\n"
                  "#end\n"
                  );
          break;

        case symbol_triangle:

          fprintf(st,
                  "#macro CLR(X, Y, theta, R, phi, W, SGN)\n"
                  "#ifdef (vfplot_clr)\n"
                  "  vfplot_clr(X, Y, theta, R, phi, W, SGN)\n"
                  "#else\n"
                  "  object {\n"
                  "    #local NS = 5;\n"
                  "    #local DT = phi/NS;\n"
                  "    #local WMIN = ER*W;\n"
                  "    merge {\n"
                  "      intersection {\n"
                  "        object{\n"
                  "          Wedge(phi + degrees(WMIN/(2*R)))\n"
                  "          rotate <90, 0, 90>\n"
                  "        }\n"
                  "        sphere_sweep {\n"
                  "          cubic_spline\n"
                  "          NS+3, \n"
                  "          #local N = -1;\n"
                  "          #while (N < NS+2)\n"
                  "            vrotate(<R, 0, 0>, <0, 0, DT*N>), ((N/NS)*WMIN + ((NS-N)/NS)*W)/2\n"
                  "            #declare N = N+1;\n"
                  "          #end\n"
                  "        }\n"
                  "      }\n"
                  "      Round_Cylinder_Merge(<R, ER*W/2, 0>, <R, -ER*W, 0>, W/2, ER*W/2)\n"
                  "    }\n"
                  "    rotate <0, 0, -phi>\n"
                  "    scale <1, -1, 1>\n"
                  "    scale <1, SGN, 1>\n"
                  "    rotate <0, 0, theta>\n"
                  "    translate <X, Y, 0>\n"
                  "  }\n"
                  "#end\n"
                  "#end\n");
          break;
        }

      fprintf(st,
              "#macro CL(X, Y, theta, R, phi, W)\n"
              "  CLR(X, Y, theta, R, phi, W, -1)\n"
              "#end\n");
      fprintf(st,
              "#macro CR(X, Y, theta, R, phi, W)\n"
              "  CLR(X, Y, theta, R, phi, W, 1)\n"
              "#end\n");

      switch (opt->arrow.symbol)
        {
        case symbol_arrow:
          fprintf(st,
                  "#macro S(X, Y, theta, L, W)\n"
                  "#ifdef (vfplot_s)\n"
                  "  vfplot_s(X, Y, theta, L, W)\n"
                  "#else\n"
                  "  object {\n"
                  "    merge {\n"
                  "      Round_Cylinder_Merge(<-L/2, 0, 0>, <L/2, 0, 0>, W/2, ER*W)\n"
                  "      Round_Cone_Merge(<L/2, 0, 0>, HW*W/2, <L/2+(HL-ER)*W, 0, 0>, ER*W, ER*W)\n"
                  "    }\n"
                  "    rotate <0, 0, theta>\n"
                  "    translate <X, Y, 0>\n"
                  "  }\n"
                  "#end\n"
                  "#end\n");
          break;

        case symbol_triangle:
          fprintf(st,
                  "#macro S(X, Y, theta, L, W)\n"
                  "#ifdef (vfplot_s)\n"
                  "  vfplot_s(X, Y, theta, L, W)\n"
                  "#else\n"
                  "  object {\n"
                  "    Round_Cone_Merge(<-L/2, 0, 0>, W/2, <L/2, 0, 0>, ER*W, ER*W)\n"
                  "    rotate <0, 0, theta>\n"
                  "    translate <X, Y, 0>\n"
                  "  }\n"
                  "#end\n"
                  "#end\n");
          break;
        }
    }

  /* ellipse prologue */

  if (draw.ellipses)
    {
      fprintf(st,
              "#macro E(X, Y, a, b, theta)\n"
              "#ifdef (vfplot_e)\n"
              "  vfplot_e(X, Y, a, b, theta)\n"
              "#else\n"
              "  sphere {\n"
              "    <0, 0, 0>, 1\n"
              "    scale <a, b, b>\n"
              "    rotate <0, 0, theta>\n"
              "    translate <X, Y, 0>\n"
              "  }\n"
              "#end\n"
              "#end\n");

      switch (opt->ellipse.fill.type)
        {
        case fill_none:
          break;

        case fill_grey:
          fprintf(st,
                  "#ifndef (vfplot_ellipse_texture)\n"
                  "#declare vfplot_ellipse_texture =\n"
                  "  texture {\n"
                  "    pigment { color rgb %.3f }\n"
                  "  };\n"
                  "#end\n",
                  ucunit(opt->ellipse.fill.grey));
          break;

        case fill_rgb:
          fprintf(st,
                  "#ifndef (vfplot_ellipse_texture)\n"
                  "#declare vfplot_ellipse_texture =\n"
                  "  texture {\n"
                  "    pigment { color rgb <%.3f, %.3f, %.3f> }\n"
                  "  };\n"
                  "#end\n",
                  ucunit(opt->ellipse.fill.rgb.r),
                  ucunit(opt->ellipse.fill.rgb.g),
                  ucunit(opt->ellipse.fill.rgb.b));
          break;
        }
    }

  /* domain prologue */

  if (draw.domain)
    {
      pen_t pen = opt->domain.pen;

      fprintf(st,
              "#local DW = %.2f;\n", pen.width);

      fprintf(st,
              "#ifndef (vfplot_domain_depth)\n"
              "#declare vfplot_domain_depth = %.2f;\n"
              "#end\n"
              "#local DD = vfplot_domain_depth;\n",
              pen.width * 5);

      fprintf(st,
              "#macro D(x1, y1, x2, y2)\n"
              "#ifdef (vfplot_d)\n"
              "  vfplot_d(x1, y1, x2, y2)\n"
              "#else\n"
              "  object {\n"
              "    #local len = vlength(<x2-x1, y2-y1>);\n"
              "    #local theta = atan2(y2-y1, x2-x1);\n"
              "    merge {\n"
              "      cylinder { <0, 0, -DD/2>, <0, 0, DD/2>, DW/2 }\n"
              "      box { <0, -DW/2, -DD/2>, <len, DW/2, DD/2> }\n"
              "    }\n"
              "    rotate <0, 0, degrees(theta)>\n"
              "    translate <x1, y1>\n"
              "  }\n"
              "#end\n"
              "#end\n"
              );

      fprintf(st,
              "#ifndef (vfplot_domain_texture)\n"
              "#declare vfplot_domain_texture =\n"
              "   texture {\n"
              "     pigment { color rgb %.2f }\n"
              "   };\n"
              "#end\n",
              pen.grey / 255.0);
    }

  /* network prologue */

  if (draw.network)
    {
      pen_t pen = opt->place.adaptive.network.pen;

      fprintf(st,
              "#local NE = %.2f;\n", pen.width);

      fprintf(st,
              "#ifndef (vfplot_network_node)\n"
              "#declare vfplot_network_node = %.2f;\n"
              "#end\n"
              "#local NN = vfplot_network_node;\n",
              pen.width*2);

      fprintf(st,
              "#macro N(x1, y1, x2, y2)\n"
              "#ifdef (vfplot_n)\n"
              "  vfplot_n((x1, y1, x2, y2)\n"
              "#else\n"
              "  object {\n"
              "    merge {\n"
              "      cylinder { <x1, y1, 0>, <x2, y2, 0>, NE/2 }\n"
              "      sphere { <x1, y1, 0>, NN/2 }\n"
              "      sphere { <x2, y2, 0>, NN/2 }\n"
              "    }\n"
              "  }\n"
              "#end\n"
              "#end\n"
              );

      fprintf(st,
              "#ifndef (vfplot_network_texture)\n"
              "#declare vfplot_network_texture =\n"
              "   texture {\n"
              "     pigment { color rgb %.2f }\n"
              "   };\n"
              "#end\n",
              pen.grey / 255.0);
    }

  /* the plot object */

  struct {
    int circular, straight, toolong, tooshort, toobendy;
  } count = {0, 0, 0, 0, 0};

  /* domain */

  if (draw.domain)
    {
      if (plot_domain_write_povray(st, dom, opt->domain.pen) != 0)
        return ERROR_BUG;
    }

  /* ellipses */

  if (draw.ellipses)
    {
      fprintf(st, "object {\n");
      fprintf(st, "union {\n");

      for (size_t i = 0 ; i < A->n ; i++)
        {
          ellipse_t e;

          arrow_ellipse(A->v + i, &arrow_opt, &e);

          fprintf(st, "E(%.2f, %.2f, %.2f, %.2f, %.2f)\n",
                  e.centre.x,
                  e.centre.y,
                  e.major,
                  e.minor,
                  e.theta * DEG_PER_RAD + 180);
        }

      fprintf(st, "}\n");
      fprintf(st, "texture { vfplot_ellipse_texture }\n");
      fprintf(st, "}\n");
    }

  /* network */

  if (draw.network)
    {
      fprintf(st, "object {\n");
      fprintf(st, "merge {\n");

      for (size_t i = 0 ; i < N->n ; i++)
        {
          vec_t
            w0 = N->v[i].a.v,
            w1 = N->v[i].b.v;

          fprintf(st, "N(%.2f, %.2f, %.2f, %.2f)\n",
                  w0.x, w0.y, w1.x, w1.y);
        }

      fprintf(st, "}\n");
      fprintf(st, "texture { vfplot_network_texture }\n");
      fprintf(st, "}\n");
    }

  /* arrows */

  if (draw.arrows && fill.arrows)
    {
      fprintf(st, "object {\n");
      fprintf(st, "union {\n");

      for (size_t i = 0 ; i < A->n ; i++)
	{
	  arrow_t a = A->v[i];

	  /* skip arrows with small radius of curvature */

	  if (a.curv > 1 / RADCRV_MIN)
	    {
	      count.toobendy++;
	      continue;
	    }

	  if (a.length > opt->arrow.length.max)
	    {
	      count.toolong++;
	      continue;
	    }

	  if (a.length < opt->arrow.length.min)
	    {
	      count.tooshort++;
	      continue;
	    }

	  /* tuncate arrows which are too snakey */

	  double
            cmax = CIRCULARITY_MAX * 2 * M_PI,
            psi = a.length * a.curv;

	  if (psi > cmax)
	    {
              /* see src/misc/clang-fpe */

#ifdef __clang__
              assert(a.curv != 0);
#endif

	      psi = cmax;
	      a.length = cmax / a.curv;
	    }

	  /*
	    head correction, we place the arrow's head a distance
	    from the end of the shaft -- the distance is chosen
	    so the area of shaft lost is equal to the area of the
	    head.
	  */

	  double hc = 0;

	  switch (opt->arrow.symbol)
	    {
	    case symbol_arrow:
	      hc = 0.5 * opt->arrow.head.length * opt->arrow.head.width * a.width;
	      break;
	    case symbol_triangle:
	      hc = 0.0;
	      break;
	    }

	  /*
	    Decide between straight and curved arrow.  We draw a straight
            arrow if the ends of the stem differ from the curved arrow by
            less  than user epsilon.
	  */

	  double sth, cth;
	  sincos(a.theta, &sth, &cth);

          double eps = opt->arrow.epsilon;

          if ((a.curv > 0) &&
              (aberration((1 / a.curv) + (a.width / 2), a.length / 2) > eps))
	    {
	      /* circular arrow */

	      double
                r = 1 / a.curv,
                R = r * cos(psi / 2);

	      /* xi is the angle accounting for the head area */

	      double xi = hc * a.curv;

	      switch (a.bend)
		{
		case rightward:
                  fprintf(st, "CR(%.2f, %.2f, %.2f, %.2f, %.2f, %.2f)\n",
                          a.centre.x + R * sth,
                          a.centre.y - R * cth,
                          (a.theta - psi / 2 + xi) * DEG_PER_RAD + 90,
                          r,
                          (psi - xi) * DEG_PER_RAD,
                          a.width);
		  break;

		case leftward:
                  fprintf(st, "CL(%.2f, %.2f, %.2f, %.2f, %.2f, %.2f)\n",
                          a.centre.x - R * sth,
                          a.centre.y + R * cth,
                          (a.theta + psi / 2 - xi) * DEG_PER_RAD - 90,
                          r,
                          (psi - xi) * DEG_PER_RAD,
                          a.width);
		  break;
		}
	      count.circular++;
	    }
	  else
	    {
	      /*
		straight arrow - our postscript function S draws an arrow who's
                shaft is centred at (x0, y0), so to account for the head and
		to centre the headed arrow in the ellipse we need to shift the
                centre by hc/2 in the direction opposite to the arrow direction
	      */

              fprintf(st, "S(%.2f, %.2f, %.2f, %.2f, %.2f)\n",
                      a.centre.x - hc * cth / 2,
                      a.centre.y - hc * sth / 2,
                      a.theta * DEG_PER_RAD,
                      a.length - hc,
                      a.width);

	      count.straight++;
	    }
	}

      fprintf(st, "}\n");
      fprintf(st, "texture { vfplot_arrow_texture }\n");
      fprintf(st, "}\n");
    }

  /* user info */

  if (report)
    {
      log_info("output");

      status("total", A->n);
      status("circular", count.circular);
      status("straight", count.straight);

      if (count.toolong) status("too long", count.toolong);
      if (count.tooshort) status("too short", count.tooshort);
      if (count.toobendy) status("too curved", count.toobendy);
    }

  return ERROR_OK;
}

static int plot_scaled(FILE *st,
                       const domain_t *dom,
                       const arrows_t *A,
                       const nbrs_t *N,
                       const plot_opt_t *opt, bool report)
{
  switch (opt->file.output.format)
    {
    case output_format_eps:
      return plot_eps(st, dom, A, N, opt, report);
    case output_format_povray:
      return plot_pov(st, dom, A, N, opt, report);
    default:
      log_error("bad output format");
      return ERROR_BUG;
    }
}

/*
  copy and scale const arguments, we're not just doing this for the sake
  of it; in the case that we make an animation then we repeatedly pass the
  arrows, network and domain to plot_output, so that can't mutate it
*/

static int plot_stream(FILE *st,
                       const domain_t *dom,
                       const arrows_t *A,
                       const nbrs_t *N,
                       const plot_opt_t *opt,
                       bool report)
{
  int err;
  double
    M = opt->page.scale,
    x0 = opt->bbox.x.min,
    y0 = opt->bbox.y.min;
  vec_t v0 = {x0, y0};
  domain_t *dom_scaled = domain_clone(dom);

  if (!dom_scaled)
    {
      log_error("failed domain clone");
      return ERROR_BUG;
    }

  if (domain_scale(dom_scaled, M, x0, y0) != 0)
    {
      err = ERROR_BUG;
      goto domain_cleanup;
    }

  arrows_t A_scaled = { .n = 0, .v = NULL };

  if (A->n > 0)
    {
      if (A->v == NULL)
	{
	  err = ERROR_BUG;
	  goto domain_cleanup;
	}

      size_t szA = A->n * sizeof(arrow_t);

      if ((A_scaled.v = malloc(szA)) == NULL)
	{
	  err = ERROR_MALLOC;
	  goto domain_cleanup;
	}

      memcpy(A_scaled.v, A->v, szA);
      A_scaled.n = A->n;

      for (size_t i = 0 ; i < A->n ; i++)
	{
	  A_scaled.v[i].centre = vscale(M, vsub(A->v[i].centre, v0));
	  A_scaled.v[i].length *= M;
	  A_scaled.v[i].width *= M;
	  A_scaled.v[i].curv = A->v[i].curv / M;
	}

      switch (opt->file.output.format)
	{
	case output_format_eps:

	  if (opt->arrow.sort != sort_none)
	    {
	      int (*s)(const void*, const void*);

	      switch (opt->arrow.sort)
		{
		case sort_longest: s = longest; break;
		case sort_shortest: s = shortest; break;
		case sort_bendiest: s = bendiest; break;
		case sort_straightest: s = straightest; break;
		default:
		  log_error("bad sort type %i", (int)opt->arrow.sort);
		  err = ERROR_BUG;
		  goto arrows_cleanup;
		}

	      qsort(A_scaled.v, A_scaled.n, sizeof(arrow_t), s);
	    }
	  break;

	case output_format_povray:

          if (opt->arrow.sort != sort_none)
            log_warn("sorting has no effect in POV-Ray output");

	  break;
	}
    }

  nbrs_t N_scaled = { .n = 0, .v = NULL };

  if (N->n > 0)
    {
      if (N->v == NULL)
	{
	  err = ERROR_BUG;
	  goto arrows_cleanup;
	}

      size_t szN = N->n * sizeof(nbr_t);

      if ((N_scaled.v = malloc(szN)) == NULL)
	{
	  err = ERROR_MALLOC;
	  goto arrows_cleanup;
	}

      memcpy(N_scaled.v, N->v, szN);
      N_scaled.n = N->n;

      for (size_t i = 0 ; i < N->n ; i++)
	{
	  N_scaled.v[i].a.v = vscale(M, vsub(N->v[i].a.v, v0));
	  N_scaled.v[i].b.v = vscale(M, vsub(N->v[i].b.v, v0));
	}
    }

  err = plot_scaled(st, dom_scaled, &A_scaled, &N_scaled, opt, report);

  free(N_scaled.v);

 arrows_cleanup:

  free(A_scaled.v);

 domain_cleanup:

  domain_destroy(dom_scaled);

  return err;
}

static int plot_report(const domain_t *dom,
                       const arrows_t *A,
                       const nbrs_t *N,
                       const plot_opt_t *opt,
                       bool report)
{
  int err = ERROR_BUG;

  if (A->n > 0)
    {
      if (opt->file.output.path != NULL)
        {
          FILE *steps;

          if ((steps = fopen(opt->file.output.path, "w")) != NULL)
            {
              err = plot_stream(steps, dom, A, N, opt, report);
              fclose(steps);
            }
          else
            {
              log_error("failed to open %s", opt->file.output.path);
              err = ERROR_WRITE_OPEN;
            }
        }
      else
        err = plot_stream(stdout, dom, A, N, opt, report);
    }
  else
    {
      log_error("nothing to plot");
      err = ERROR_NODATA;
    }

  return err;
}

int plot_output(const domain_t *dom,
                const arrows_t *A,
                const nbrs_t *N,
                const plot_opt_t *opt)
{
  return plot_report(dom, A, N, opt, true);
}
int plot_frame(const domain_t *dom,
               const arrows_t *A,
               const nbrs_t *N,
               const plot_opt_t *opt)
{
  return plot_report(dom, A, N, opt, false);
}

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/polygon.h"
#include "plot/macros.h"

#include <geom2d/sincos.h>
#include <log.h>

#ifdef HAVE_JANSSON_H
#include <jansson.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <math.h>

/*
  the polygon struct is simple and open, so we don't actually
  have new/destroy for these, we expect the caller to have a
  polygon as part of a struct (the domain node does this) rather
  than having a pointer to one; so init- anc clear-functions
  instead
*/

int polygon_init(size_t n, polygon_t *p)
{
  vec_t *v;

  if ((v = malloc(n * sizeof(vec_t))) == NULL)
    return 1;

  p->v = v;
  p->n = n;

  return 0;
}

void polygon_clear(polygon_t *p)
{
  if ((p->n > 0) && (p->v != NULL))
    {
      p->n = 0;
      free(p->v);
      p->v = NULL;
    }
}

int polygon_clone(const polygon_t *p, polygon_t *q)
{
  size_t n = p->n;
  vec_t *v = NULL;

  if (n > 0)
    {
      if ((v = malloc(n * sizeof(vec_t))) == NULL)
        return 1;
      memcpy(v, p->v, n * sizeof(vec_t));
    }

  q->n = n;
  q->v = v;

  return 0;
}

/* canned polygon generators (which allocate) */

int polygon_ngon(double r, vec_t O, size_t n, polygon_t *p)
{
  if (n < 3) return 1;

  if (polygon_init(n, p) != 0)
    return 1;

  vec_t *v = p->v;

  for (size_t i = 0 ; i < n ; i++)
    {
      double
        t = i * 2.0 * M_PI / n,
        st, ct;

      sincos(t, &st, &ct);

      v[i].x = O.x + r * ct;
      v[i].y = O.y + r * st;
    }

  return 0;
}

int polygon_rect(bbox_t b, polygon_t *p)
{
  if ((b.x.min > b.x.max) || (b.y.min > b.y.max))
    return 1;

  if (polygon_init(4, p) != 0)
    return 1;

  p->v[0].x = b.x.min;
  p->v[0].y = b.y.min;

  p->v[1].x = b.x.max;
  p->v[1].y = b.y.min;

  p->v[2].x = b.x.max;
  p->v[2].y = b.y.max;

  p->v[3].x = b.x.min;
  p->v[3].y = b.y.max;

  return 0;
}

/*
  test whether a vertex is inside a polygon, by counting the
  intersections with a horizontal line through that vertex, old
  computational geometry hack (a comp.graphics.algorithms faq)
*/

bool polygon_inside(const polygon_t *p, vec_t v)
{
  bool c = false;
  size_t n = p->n;
  const vec_t *pv = p->v;

  for (size_t i = 0, j = n - 1 ; i < n ; j = i++)
    {
      if ((((pv[i].y <= v.y) && (v.y < pv[j].y)) ||
	   ((pv[j].y <= v.y) && (v.y < pv[i].y))) &&
	  (v.x < (pv[j].x - pv[i].x) * (v.y - pv[i].y) /
	   (pv[j].y - pv[i].y) + pv[i].x))
	c = !c;
    }

  return c;
}

/*
  returns true if the polygon p is contained in the polygon q,
  we first check that all verticies of p are inside q, that's
  a quick check, but is not sufficient for the non-convex case.
  for that we check if any of the line segments of p intersect
  any of those of q (and we already know that all of the points
  of p are inside q, so at least one it).
*/

bool polygon_contains(const polygon_t *p, const polygon_t *q)
{
  size_t np = p->n, nq = q->n;
  const vec_t *vp = p->v, *vq = q->v;

  for (size_t i = 0 ; i < np ; i++)
    if (! polygon_inside(q, vp[i]))
      return false;

  for (size_t i = 0 ; i < np ; i++)
    {
      vec_t p1 = vp[i], p2 = vp[(i + 1) % np];

      for (size_t j = 0 ; j < nq ; j++)
        {
          vec_t q1 = vq[j], q2 = vq[(j + 1) % nq];

          if (visect(p1, p2, q1, q2))
            return false;
        }
    }

  return true;
}

/* the integer winding number by summing the external angles */

int polygon_wind(const polygon_t *p)
{
  vec_t *v = p->v;
  size_t n = p->n;
  double sum = 0.0;

  for (size_t i = 0 ; i < n ; i++)
    {
      size_t
	j = (i + 1) % n,
	k = (i + 2) % n;
      vec_t
        a = vsub(v[i], v[j]),
        b = vsub(v[j], v[k]);

      if (vabs(a) == 0)
        log_warn("degenerate segment %zi of polygon at (%f, %f)",
                 i, v[i].x, v[i].y);

      sum += vxtang(a, b);
    }

  return rint(sum / (2 * M_PI));
}

/* bounding box */

bbox_t polygon_bbox(const polygon_t *p)
{
  size_t n = p->n;
  vec_t *v = p->v;
  bbox_t b = { {v[0].x, v[0].x}, {v[0].y, v[0].y} };

  for (size_t i = 0 ; i < n ; i++)
    {
      double x = v[i].x, y = v[i].y;

      b.x.min = MIN(b.x.min, x);
      b.x.max = MAX(b.x.max, x);
      b.y.min = MIN(b.y.min, y);
      b.y.max = MAX(b.y.max, y);
    }

  return b;
}

/*
  reverse the list of vertices in a polygon, and so reverse
  the orientation
*/

int polygon_reverse(polygon_t *p)
{
  size_t n = p->n;
  vec_t *v = p->v;

  for (size_t i = 0 ; i < n / 2 ; i++)
    {
      vec_t vt = v[n - i - 1];
      v[n - i - 1] = v[i];
      v[i] = vt;
    }

  return 0;
}

/*
  The area bounded by a polygon using the classical planimiter
  method [1]. There are actually slightly faster methods, but this
  gets used very little, probably best to keep it as simple as
  possible.

  Note we don't return the "signed area", but the absolute value
  of it; one could exploit the sign in building the corresponding
  area code for domains, but that would introduce a sort-of hidden
  dependency that I'd like to avoid for now.

  The planimiter formula needs the n+1-th point to be the same as
  the first point, our polygons don't have that (the final line
  is implicit) so we work mod-n to simulate the last point.

  [1] https://stackoverflow.com/questions/451426/
*/

double polygon_area(const polygon_t *p)
{
  size_t n = p->n;

  if (n <= 2) return 0;

  const vec_t *v = p->v;
  double sum = 0;

  for (size_t i = 0 ; i < n ; i++)
    sum += vdet(v[i % n], v[(i + 1) % n]);

  return fabs(sum) / 2;
}

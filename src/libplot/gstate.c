#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <string.h>

#include "plot/gstate.h"
#include "plot/error.h"

#include <log.h>

#ifdef HAVE_JANSSON_H
#include <jansson.h>
#endif

#ifdef HAVE_LIBJANSSON

static int jr_point(json_t *point_json, double *x, double *y)
{
  if (! json_is_object(point_json))
    {
      log_error("JSON not an object");
      return 1;
    }

  json_t *x_json;

  if ((x_json = json_object_get(point_json, "x")) == NULL)
    {
      log_error("no key 'x'");
      return 1;
    }

  if (! json_is_real(x_json))
    {
      log_error("value is not real");
      return 1;
    }

  *x = json_real_value(x_json);

  json_t *y_json;

  if ((y_json = json_object_get(point_json, "y")) == NULL)
    {
      log_error("no key 'y'");
      return 1;
    }

  if (! json_is_real(y_json))
    {
      log_error("value is not real");
      return 1;
    }

  *y = json_real_value(y_json);

  return 0;
}

static int jr_arrow(json_t *arrow_json, arrow_t *A)
{
  if (! json_is_object(arrow_json))
    {
      log_error("JSON not an object");
      return 1;
    }

  json_t *theta_json;

  if ((theta_json = json_object_get(arrow_json, "theta")) == NULL)
    {
      log_error("no key 'theta'");
      return 1;
    }

  if (! json_is_real(theta_json))
    {
      log_error("value is not real");
      return 1;
    }

  A->theta = json_real_value(theta_json);

  json_t *length_json;

  if ((length_json = json_object_get(arrow_json, "length")) == NULL)
    {
      log_error("no key 'length'");
      return 1;
    }

  if (! json_is_real(length_json))
    {
      log_error("value is not real");
      return 1;
    }

  A->length = json_real_value(length_json);

  json_t *width_json;

  if ((width_json = json_object_get(arrow_json, "width")) == NULL)
    {
      log_error("no key 'width'");
      return 1;
    }

  if (! json_is_real(width_json))
    {
      log_error("value is not real");
      return 1;
    }

  A->width = json_real_value(width_json);

  json_t *curve_json;

  if ((curve_json = json_object_get(arrow_json, "curve")) == NULL)
    {
      log_error("no key 'curve'");
      return 1;
    }

  if (! json_is_real(curve_json))
    {
      log_error("value is not real");
      return 1;
    }

  double C = json_real_value(curve_json);

  if (C < 0)
    {
      A->bend = leftward;
      A->curv = -C;
    }
  else
    {
      A->bend = rightward;
      A->curv = C;
    }

  json_t *centre_json;

  if ((centre_json = json_object_get(arrow_json, "centre")) == NULL)
    {
      log_error("no key 'centre'");
      return 1;
    }

  if (jr_point(centre_json, &(A->centre.x), &(A->centre.y)) != 0)
    {
      log_error("failed read of centre");
      return 1;
    }

  return 0;
}

static int jr_arrows(json_t *arrows_json, arrows_t *A)
{
  if (! json_is_array(arrows_json))
    {
      log_error("JSON not an array");
      return 1;
    }

  size_t nA = json_array_size(arrows_json);

  if (nA == 0)
    {
      A->n = 0;
      A->v = NULL;
      return 0;
    }

  arrow_t *vA;

  if ((vA = calloc(nA, sizeof(arrow_t))) == NULL)
    {
      log_error("failed alloc of %zi arrows", nA);
      return 1;
    }

  size_t i;
  json_t *arrow_json;
  int err = 0;

  json_array_foreach(arrows_json, i, arrow_json)
    {
      if (jr_arrow(arrow_json, vA + i) != 0)
        err++;
    }

  if (err == 0)
    {
      A->n = nA;
      A->v = vA;
      return 0;
    }

  log_error("failed read of %i arrows", err);
  free(vA);

  return 1;
}

static int jr_node(json_t *node_json, uint32_t *id, vec_t *v)
{
  if (! json_is_object(node_json))
    {
      log_error("JSON not an object");
      return 1;
    }

  json_t *id_json;

  if ((id_json = json_object_get(node_json, "id")) == NULL)
    {
      log_error("no key 'id'");
      return 1;
    }

  if (! json_is_integer(id_json))
    {
      log_error("value not integer");
      return 1;
    }

  *id = json_integer_value(id_json);

  json_t *centre_json;

  if ((centre_json = json_object_get(node_json, "centre")) == NULL)
    {
      log_error("no key 'centre'");
      return 1;
    }

  if (jr_point(centre_json, &(v->x), &(v->y)) != 0)
    {
      log_error("failed read of point");
      return 1;
    }

  return 0;
}

static int jr_nbr(json_t *nbr_json, nbr_t *N)
{
  if (! json_is_array(nbr_json))
    {
      log_error("JSON not an array");
      return 1;
    }

  if (json_array_size(nbr_json) != 2)
    {
      log_error("array is not a pair");
      return 1;
    }

  json_t *node0_json;

  if ((node0_json = json_array_get(nbr_json, 0)) != NULL)
    {
      if (jr_node(node0_json, &(N->a.id), &(N->a.v)) != 0)
        {
          log_error("failed read of 1st node");
          return 1;
        }
    }
  else
    {
      log_error("failed get of 1st array element");
      return 1;
    }

  json_t *node1_json;

  if ((node1_json = json_array_get(nbr_json, 1)) != NULL)
    {
      if (jr_node(node1_json, &(N->b.id), &(N->b.v)) != 0)
        {
          log_error("failed read of 2st node");
          return 1;
        }
    }
  else
    {
      log_error("failed get of 2nd array element");
      return 1;
    }

  return 0;
}

static int jr_nbrs(json_t *nbrs_json, nbrs_t *N)
{
  if (! json_is_array(nbrs_json))
    {
      log_error("JSON not an array");
      return 1;
    }

  size_t nN = json_array_size(nbrs_json);

  if (nN == 0)
    {
      N->n = 0;
      N->v = NULL;
      return 0;
    }

  nbr_t *vN;

  if ((vN = calloc(nN, sizeof(nbr_t))) == NULL)
    {
      log_error("failed alloc of %zi neighbours", nN);
      return 1;
    }

  size_t i;
  json_t *nbr_json;
  int err = 0;

  json_array_foreach(nbrs_json, i, nbr_json)
    {
      if (jr_nbr(nbr_json, vN + i) != 0)
        err++;
    }

  if (err == 0)
    {
      N->n = nN;
      N->v = vN;
      return 0;
    }

  free(vN);
  log_error("failed read of %i neighnours", err);

  return 1;
}

static int jr_gstate(json_t *state_json, gstate_t *G)
{
  if (! json_is_object(state_json))
    {
      log_error("JSON not an object");
      return 1;
    }

  json_t *arrows_json;

  if ((arrows_json = json_object_get(state_json, "arrows")) != NULL)
    {
      if (jr_arrows(arrows_json, &(G->arrows)) != 0)
        {
          log_error("failed read of arrows");
          return 1;
        }
    }
  else
    {
      log_error("no key 'arrows'");
      return 1;
    }

  json_t *nbrs_json;

  if ((nbrs_json = json_object_get(state_json, "nbrs")) != NULL)
    {
      if (jr_nbrs(nbrs_json, &(G->nbrs)) != 0)
        {
          log_error("failed read of 'nbrs'");
          return 1;
        }
    }
  else
    {
      log_error("no key 'nbrs'");
      return 1;
    }

  return 0;
}

int gstate_read(FILE *stream, gstate_t *G)
{
  const size_t flags = JSON_REJECT_DUPLICATES;
  json_t *state_json;

  if ((state_json = json_loadf(stream, flags, NULL)) != NULL)
    {
      int err = jr_gstate(state_json, G);
      json_decref(state_json);
      return err;
    }

  return 1;
}

static json_t* jw_point(double x, double y)
{
  json_t *centre_json;

  if ((centre_json = json_object()) == NULL)
    return NULL;

  json_object_set_new(centre_json, "x", json_real(x));
  json_object_set_new(centre_json, "y", json_real(y));

  return centre_json;
}

static json_t* jw_arrow(const arrow_t *A)
{
  json_t *arrow_json;

  if ((arrow_json = json_object()) == NULL)
    return NULL;

  json_object_set_new(arrow_json, "centre", jw_point(A->centre.x, A->centre.y));
  json_object_set_new(arrow_json, "theta", json_real(A->theta));
  json_object_set_new(arrow_json, "length", json_real(A->length));
  json_object_set_new(arrow_json, "width", json_real(A->width));

  int sign = (A->bend == rightward) ? 1 : -1;
  json_object_set_new(arrow_json, "curve", json_real(sign * A->curv));

  return arrow_json;
}

static json_t* jw_arrows(const arrows_t *A)
{
  json_t *arrows_json;

  if ((arrows_json = json_array()) == NULL)
    return NULL;

  for (size_t i = 0 ; i < A->n ; i++)
    {
      json_t *arrow_json = jw_arrow(A->v + i);
      json_array_append_new(arrows_json, arrow_json);
    }

  return arrows_json;
}

static json_t* jw_node(uint32_t id, vec_t v)
{
  json_t *node_json;

  if ((node_json = json_object()) == NULL)
    return NULL;

  json_object_set_new(node_json, "id", json_integer(id));
  json_object_set_new(node_json, "centre", jw_point(v.x, v.y));

  return node_json;
}

static json_t* jw_nbr(const nbr_t *N)
{
  json_t *nbr_json;

  if ((nbr_json = json_array()) == NULL)
    return NULL;

  json_array_append_new(nbr_json, jw_node(N->a.id, N->a.v));
  json_array_append_new(nbr_json, jw_node(N->b.id, N->b.v));

  return nbr_json;
}

static json_t* jw_nbrs(const nbrs_t *N)
{
  json_t *nbrs_json;

  if ((nbrs_json = json_array()) == NULL)
    return NULL;

  for (size_t i = 0 ; i < N->n ; i++)
    {
      json_t *nbr_json = jw_nbr(N->v + i);
      json_array_append_new(nbrs_json, nbr_json);
    }

  return nbrs_json;
}

static json_t* jw_gstate(const gstate_t *G)
{
  json_t *state_json;

  if ((state_json = json_object()) == NULL)
    return NULL;

  json_object_set_new(state_json, "arrows", jw_arrows(&(G->arrows)));
  json_object_set_new(state_json, "nbrs", jw_nbrs(&(G->nbrs)));

  return state_json;
}

int gstate_write(FILE *stream, const gstate_t *G)
{
  json_t *state_json;
  int err = 0;
  size_t flags = JSON_INDENT(2) | JSON_REAL_PRECISION(10);

  if ((state_json = jw_gstate(G)) != NULL)
    {
      if (json_dumpf(state_json, stream, flags) != 0)
        err++;
      json_decref(state_json);
    }
  else
    err++;

  return ((err > 0) ? 1 : 0);
}

#else

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

int gstate_read(FILE *st, gstate_t *G)
{
  return 1;
}

int gstate_write(FILE *st, const gstate_t *G)
{
  return 1;
}

#pragma GCC diagnostic pop

#endif

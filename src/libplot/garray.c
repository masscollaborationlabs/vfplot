#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/garray.h"

void** garray_new(size_t rows, size_t cols, size_t size)
{
  if ((rows == 0) || (cols) == 0)
    return NULL;

  void **vpp = malloc(rows*sizeof(void*));

  if (vpp != NULL)
    {
      void *vp = malloc(rows*cols*size);

      if (vp != NULL)
	{
	  for (size_t i = 0 ; i < rows ; i++)
	    vpp[i] = (void*)((char*)vp + i*cols*size);

	  return vpp;
	}

      free(vpp);
    }

  return NULL;
}

void garray_destroy(void **vpp)
{
  free(*vpp);
  free(vpp);
}

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#ifdef HAVE_JANSSON_H
#include <jansson.h>
#endif

#include <log.h>

#include "plot/polygon-geojson.h"
#include "plot/domain.h"
#include "plot/domain-geojson.h"
#include "plot/domain-type.h"

#ifdef HAVE_LIBJANSSON

static int gjw_coords_node(const domain_node_t *node, json_t *node_json)
{
  if (node == NULL)
    return 0;

  json_t *polygon_json;

  if ((polygon_json = polygon_geojson_serialise(&(node->p))) == NULL)
    {
      log_error("failed polygon to JSON pair-array");
      return 1;
    }

  if (json_array_append_new(node_json, polygon_json) == -1)
    {
      log_error("failed JSON append");
      return 1;
    }

  return 0;
}

static json_t* gjw_coords_polygon(const domain_node_t *node)
{
  json_t *coords_json;

  if ((coords_json = json_array()) != NULL)
    {
      int err = gjw_coords_node(node, coords_json);

      for (const domain_node_t *c = node->child ; c ; c = c->peer)
        err += gjw_coords_node(c, coords_json);

      if (err == 0)
        return coords_json;

      json_decref(coords_json);
    }

  return NULL;
}

static int gjw_coords_polygons(const domain_node_t *node, json_t *polygons_json)
{
  if (node == NULL)
    return 0;

  json_t *polygon_json;

  if ((polygon_json = gjw_coords_polygon(node)) == NULL)
    {
      log_error("failed polygon-coordinate JSON for self");
      return 1;
    }

  if (json_array_append_new(polygons_json, polygon_json) == -1)
    {
      log_error("failed JSON append");
      return 1;
    }

  for (const domain_node_t *c = node->child ; c ; c = c->peer)
    if (gjw_coords_polygons(c->child, polygons_json) != 0)
      {
        log_error("failed polygon-coordinate JSON for chile");
        return 1;
      }

  if (gjw_coords_polygons(node->peer, polygons_json) != 0)
    {
      log_error("failed JSON append");
      return 1;
    }

  return 0;
}

static json_t* gjw_coords_multipolygon(const domain_node_t *node)
{
  json_t *polygons_json;

  if ((polygons_json = json_array()) != NULL)
    {
      if (gjw_coords_polygons(node, polygons_json) == 0)
        return polygons_json;

      json_decref(polygons_json);
    }

  return NULL;
}

static json_t* gjw_wrap_geometry(const char *name, json_t *coords_json)
{
  json_t *type_json;

  if ((type_json = json_string(name)) != NULL)
    {
      json_t *geometry_json;

      if ((geometry_json = json_object()) != NULL)
        {
          json_object_set_new(geometry_json, "type", type_json);
          json_object_set(geometry_json, "coordinates", coords_json);
          return geometry_json;
        }

      json_decref(type_json);
    }

  return NULL;
}

static json_t* gjw_wrap_polygon(json_t *coords_json)
{
  return gjw_wrap_geometry("Polygon", coords_json);
}

static json_t* gjw_wrap_multipolygon(json_t *coords_json)
{
  return gjw_wrap_geometry("MultiPolygon", coords_json);
}

static json_t* gjw_root_polygon(const domain_node_t *node)
{
  json_t *coords_json;

  if ((coords_json = gjw_coords_polygon(node)) != NULL)
    return gjw_wrap_polygon(coords_json);

  log_error("failed polygon JSON write");

  return NULL;
}

static json_t* gjw_root_multipolygon(const domain_node_t *node)
{
  json_t *coords_json;

  if ((coords_json = gjw_coords_multipolygon(node)) != NULL)
    return gjw_wrap_multipolygon(coords_json);

  log_error("failed multi-polygon JSON write");

  return NULL;
}

static int domain_write_stream(FILE *st, const domain_t *dom)
{
  json_t *node_json;

  if ((domain_width(dom) > 1) || (domain_depth(dom) > 2))
    node_json = gjw_root_multipolygon(dom->head);
  else
    node_json = gjw_root_polygon(dom->head);

  int err = 0;
  size_t flags = JSON_INDENT(2) | JSON_REAL_PRECISION(10);

  if (node_json != NULL)
    {
      if (json_dumpf(node_json, st, flags) != 0)
        err++;
      json_decref(node_json);
    }
  else
    err++;

  return ((err > 0) ? 1 : 0);
}

int domain_write(const char *path, const domain_t *dom)
{
  int err;

  if (path == NULL)
    err = domain_write_stream(stdout, dom);
  else
    {
      FILE *st;
      if ((st = fopen(path, "w")) == NULL)
        return 1;
      err = domain_write_stream(st, dom);
      fclose(st);
    }

  return err;
}

/*
  deserialise - the idea here is to iterate through the geojson
  tree carrying and empty domain_t with us, if we find a Polygon
  or MultiPolygon, then we add the polygon_t to the domain and
  return the count of such; if we error (including GeoJSON syntax
  errors) then we return -1.  So we continue the iteration while
  these functions return 0;
*/

static bool geojson_is_type(json_t *geo_json, const char *geo_type)
{
  if (! json_is_object(geo_json))
    return false;

  json_t *type_json = json_object_get(geo_json, "type");

  if (type_json == NULL)
    return false;

  if (! json_is_string(type_json))
    return false;

  const char *type = json_string_value(type_json);

  if (type == NULL)
    return false;

  return strcmp(geo_type, type) == 0;
}

/*
  pscoords is am array ps pcoords, and a pcoords is an array
  of pairs of real, which polygon_geojson_deserialise can
  deserialise to a polygon_t
*/

static int gjr_pscoords(json_t *pscoords_json, domain_t *dom)
{
  if (! json_is_array(pscoords_json))
    {
      log_error("polygon coordinates not an array");
      return -1;
    }

  size_t n = json_array_size(pscoords_json);

  if (n < 1)
    {
      log_error("no polygon coordinates");
      return -1;
    }

  polygon_t p;
  json_t *pcoords_json;
  size_t i;
  int status = 0;

  json_array_foreach(pscoords_json, i, pcoords_json)
    {
      if (polygon_geojson_deserialise(pcoords_json, &p) == 0)
        {
          if (domain_insert(dom, &p) == 0)
            status++;
          else
            {
              log_error("failed domain insert of polygon");
              status = -1;
            }

          polygon_clear(&p);
        }
      else
        {
          log_error("failed deserialise of polygon coordinates");
          status = -1;
        }

      if (status < 0)
        break;
    }

  return status;
}

static int gjr_mpscoords(json_t *mpscoords_json, domain_t *dom)
{
  if (! json_is_array(mpscoords_json))
    {
      log_error("MultiPolygon coordinates not an array");
      return -1;
    }

  size_t n = json_array_size(mpscoords_json);

  if (n < 1)
    {
      log_warn("MultPolygon is empty");
      return 0;
    }

  json_t *pscoords_json;
  size_t i;
  int status = 0;

  json_array_foreach(mpscoords_json, i, pscoords_json)
    {
      int pscoords_status = gjr_pscoords(pscoords_json, dom);
      if (pscoords_status < 0)
        {
          log_error("failed MultiPolygon item %zi", i);
          return -1;
        }
      status += pscoords_status;
    }

  return status;
}

static int gjr_polygon(json_t *geometry_json, domain_t *dom)
{
  if (! geojson_is_type(geometry_json, "Polygon"))
    return 0;

  json_t *pscoords_json =
    json_object_get(geometry_json, "coordinates");

  if (pscoords_json == NULL)
    {
      log_error("Polygon object without coordinates");
      return -1;
    }

  return gjr_pscoords(pscoords_json, dom);
}

static int gjr_multipolygon(json_t *geometry_json, domain_t *dom)
{
  if (! geojson_is_type(geometry_json, "MultiPolygon"))
    return 0;

  json_t *mpscoords_json =
    json_object_get(geometry_json, "coordinates");

  if (mpscoords_json == NULL)
    {
      log_error("MultiPolygon object without coordinates");
      return -1;
    }

  return gjr_mpscoords(mpscoords_json, dom);
}

static int gjr_geometry(json_t *geometry_json, domain_t *dom)
{
  int status;

  if ((status = gjr_polygon(geometry_json, dom)) ||
      (status = gjr_multipolygon(geometry_json, dom)))
    return status;

  return 0;
}

static int gjr_feature(json_t *feature_json, domain_t *dom)
{
  if (! geojson_is_type(feature_json, "Feature"))
    return 0;

  json_t *geometry_json = json_object_get(feature_json, "geometry");

  if (geometry_json == NULL)
    {
      log_error("Feature object without geometry");
      return -1;
    }

  return gjr_geometry(geometry_json, dom);
}

static int gjr_features(json_t *features_json, domain_t *dom)
{
  if (! json_is_array(features_json))
    return -1;

  size_t i;
  json_t *feature_json;
  int status = 0;

  json_array_foreach(features_json, i, feature_json)
    {
      if ((status = gjr_feature(feature_json, dom)) != 0)
        break;
    }

  return status;
}

static int gjr_feature_collection(json_t *geo_json, domain_t *dom)
{
  if (! geojson_is_type(geo_json, "FeatureCollection"))
    return 0;

  json_t *features_json = json_object_get(geo_json, "features");

  if (features_json == NULL)
    {
      log_error("FeatureCollection object without features");
      return -1;
    }

  return gjr_features(features_json, dom);
}

static int gjr_geometry_collection(json_t *geo_json, domain_t *dom)
{
  if (! geojson_is_type(geo_json, "GeometryCollection"))
    return 0;

  json_t *geometries_json = json_object_get(geo_json, "geometries");

  if (geometries_json == NULL)
    {
      log_error("GeometryCollection object without geometries");
      return -1;
    }

  size_t i;
  json_t *geometry_json;
  int status = 0;

  json_array_foreach(geometries_json, i, geometry_json)
    {
      if ((status = gjr_geometry(geometry_json, dom)) != 0)
        break;
    }

  return status;
}

static domain_t* gjr_domain(json_t *geo_json)
{
  domain_t *dom;

  if ((dom = domain_new()) != NULL)
    {
      int status;

      if ((status = gjr_polygon(geo_json, dom)) ||
          (status = gjr_multipolygon(geo_json, dom)) ||
          (status = gjr_feature(geo_json, dom)) ||
          (status = gjr_feature_collection(geo_json, dom)) ||
          (status = gjr_geometry_collection(geo_json, dom)))
        {
          if (status > 0)
            {
              log_info("domain of %i polygon%s",
                       status,
                       (status == 1) ? "" : "s");
              return dom;
            }
          else if (status == 0)
            log_error("no polygons found in GeoJSON");
          else
            log_error("failed to read GeoJSON");
        }

      domain_destroy(dom);
    }
  else
    log_error("failed create of domain");

  return NULL;
}

static domain_t* domain_read_stream(FILE *st)
{
  json_t *geo_json;
  domain_t *dom = NULL;

  if ((geo_json = json_loadf(st, 0, NULL)) != NULL)
    {
      dom = gjr_domain(geo_json);
      json_decref(geo_json);
    }
  else
    log_error("failed parse of JSON");

  return dom;
}

domain_t* domain_read(const char *path)
{
  domain_t *dom;

  if (path == NULL)
    dom = domain_read_stream(stdin);
  else
    {
      FILE *st;
      if ((st = fopen(path, "r")) == NULL)
        return NULL;
      dom = domain_read_stream(st);
      fclose(st);
    }

  if (dom != NULL)
    {
      if (domain_hierarchy_check(dom) == 0)
        {
          if (domain_orientate(dom) == 0)
            return dom;
          else
            log_error("failed orientation of input domain");
        }
      else
	{
	  log_error("hierarchy violation in input domain");
	  log_error("things are going to go wrong ...");
	}

      domain_destroy(dom);
    }

  log_error("no domain");

  return NULL;
}

#else

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

int domain_write(const char *path, const domain_t *dom)
{
  log_error("cannot write to %s, no JSON support", path);
  return 1;
}

domain_t* domain_read(const char *path)
{
  log_error("cannot read from %s, no JSON support", path);
  return NULL;
}

#pragma GCC diagnostic pop

#endif

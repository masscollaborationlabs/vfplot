#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <errno.h>

#include "plot/margin.h"

/* changing this will break a couple of unit tests */

#define RAMP 0.1

/*
  this is a qudratic spline through (0, b) spliced with y = mx
  if m > 0, or a linear ramp down to L * b if m < 0

  m^2 x^2
  ------- + b  (for 0<x<2b/m, m>0)
    4b

  mx           (for 2b/m<x, m>0)

  mx + b       (for 0<x<(L-1)b/m, m<0)

  Lb           (for (L-1)b/m<x, m<0)

  the linear ramp could be made into a proper spline
*/

double margin(double x, double b, double m)
{
  if (x < 0)
    {
      errno = EDOM;
      return 0.0;
    }

  double mx = m * x;

  if (m > 0.0)
    {
      if (mx < 2 * b)
        return mx * mx / (4 * b) + b;
      else
        return mx;
    }
  else
    {
      if (mx > (RAMP - 1) * b)
        return b + mx;
      else
        return RAMP * b;
    }
}

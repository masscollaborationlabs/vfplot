#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>

#include "plot/evaluate.h"
#include "plot/curvature.h"
#include "plot/aspect.h"
#include "plot/limits.h"
#include "plot/error.h"

/*
  this function is used by all placement strategies and so is
  extracted here -- it takes the (x, y) coordinates of the arrow
  and caculates the arrow geometry
*/

int complete_arrow(const evaluate_t *e, arrow_t *A)
{
  double
    x = A->centre.x,
    y = A->centre.y,
    theta, mag, curv;

  if (e->fv(e->field, x, y, &theta, &mag) != 0)
    return ERROR_NODATA;

  if (e->fc)
    {
      if (e->fc(e->field, x, y, &curv) != 0)
        return ERROR_NODATA;
    }
  else
    {
      if (curvature(e->fv, e->field, x, y, e->aspect, &curv) != 0)
        return ERROR_NODATA;
    }

  bend_t bend = (curv > 0 ? rightward : leftward);
  curv = fabs(curv);

  double len, wdt;
  aspect_fixed(e->aspect, mag, &len, &wdt);

  if (len > LENGTH_MAX)
    return ERROR_NODATA;

  A->theta = theta;
  A->width = wdt;
  A->length = len;
  A->curv = curv;
  A->bend = bend;

  return ERROR_OK;
}

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/polygon-geojson.h"
#include "plot/polygon.h"
#include "plot/macros.h"

#include <geom2d/sincos.h>
#include <log.h>

#ifdef HAVE_JANSSON_H
#include <jansson.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef HAVE_LIBJANSSON

/* write (serialse) to GeoJSON */

static json_t* gjw_vector(vec_t v)
{
  json_t *vector_json;

  if ((vector_json = json_array()) == NULL)
    return NULL;

  json_array_append_new(vector_json, json_real(v.x));
  json_array_append_new(vector_json, json_real(v.y));

  return vector_json;
}

static json_t* gjw_polygon(const polygon_t *p)
{
  if (p->n < 3)
    {
      log_error("polygon with %zi points is invalid GeoJSON");
      return NULL;
    }

  json_t *polygon_json;

  if ((polygon_json = json_array()) == NULL)
    return NULL;

  size_t n = p->n;

  for (size_t i = 0 ; i < n + 1 ; i++)
    {
      json_t *vector_json;

      if ((vector_json = gjw_vector(p->v[i % n])) == NULL)
        return NULL;
      json_array_append_new(polygon_json, vector_json);
    }

  return polygon_json;
}

struct json_t* polygon_geojson_serialise(const polygon_t *p)
{
  return gjw_polygon(p);
}

/* read (deserialse) from GeoJSON */

int gjr_num(const json_t *num_json, double *x)
{
  if (! json_is_number(num_json))
    {
      log_error("not a number");
      return 1;
    }

  *x = json_number_value(num_json);

  return 0;
}

int gjr_point(const json_t *point_json, vec_t *point)
{
  if (! json_is_array(point_json))
    {
      log_error("not an array");
      return 1;
    }

  size_t n = json_array_size(point_json);

  if (n != 2)
    {
      log_error("array has size %zi, expected 2", n);
      return 1;
    }

  if (gjr_num(json_array_get(point_json, 0), &(point->x)) != 0)
    {
      log_error("failed read of x-coordinate");
      return 1;
    }

  if (gjr_num(json_array_get(point_json, 1), &(point->y)) != 0)
    {
      log_error("failed read of y-coordinate");
      return 1;
    }

  return 0;
}

int gjr_polygon(const json_t *polygon_json, polygon_t *p)
{
  if (! json_is_array(polygon_json))
    {
      log_error("expected polygon coordinates, but not an array");
      return 1;
    }

  size_t n = json_array_size(polygon_json);

  if (n < 4)
    {
      log_error("need 4 points in polygon, found %zi", n);
      return 1;
    }

  if (polygon_init(n - 1, p) != 0)
    return 1;

  for (size_t i = 0 ; i < n - 1 ; i++)
    {
      json_t *point_json;

      if ((point_json = json_array_get(polygon_json, i)) == NULL)
        return 1;

      if (gjr_point(point_json, p->v + i) != 0)
        {
          log_error("failed read of point %zi", i);
          return 1;
        }
    }

  return 0;
}

int polygon_geojson_deserialise(const struct json_t *json, polygon_t *p)
{
  return gjr_polygon(json, p);
}

#else

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

struct json_t* polygon_geojson_serialise(const polygon_t *p)
{
  return NULL;
}

int polygon_geojson_deserialise(const struct json_t *json, polygon_t *p)
{
  return 1;
}

#pragma GCC diagnostic pop

#endif

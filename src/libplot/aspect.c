#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>

#include "plot/aspect.h"

/*
  the vector magnitude is interpreted as the area of the arrow shaft,
  this function should return the length and width of the required
  arrow having this area -- this to be extended, this should be user
  configurable.
*/

void aspect_fixed(double aspect, double area, double *lp, double *wp)
{
  double
    wdt = sqrt(area / aspect),
    len = aspect * wdt;
  *wp = wdt;
  *lp = len;
}

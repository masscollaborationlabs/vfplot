#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>

#include "plot/potential.h"

/*
  The potential is

  linear with gradient -1 for  0 < x < x0
  order POTENTIAL_ORDER for   x0 < x < 1
  zero for                     1 < x

  and continuous. If POTENTIAL_ORDER is not defined
  then 2 is assumed
*/

/*
  The argument x should not be negative, but this
  is not checked.

  The argument x0 should be between 0 and 1
*/

#ifdef POTENTIAL_ORDER

double potential_V(double x, double x0)
{
  if (x0 < 1)
    {
      if (x < x0)
	return
          -x + ((POTENTIAL_ORDER - 1) * x0 + 1)
          / POTENTIAL_ORDER;

      if (x < 1)
	return
          pow((1 - x) / (1 - x0), (POTENTIAL_ORDER - 1)) * (1 - x)
          / POTENTIAL_ORDER;
    }
  else if (x < 1)
    return 1 - x;

  return 0;
}

double potential_dV(double x, double x0)
{
  if (x0 < 1)
    {
      if (x < x0) return -1;

      if (x < 1)
	return -pow((1 - x) / (1 - x0), POTENTIAL_ORDER - 1);
    }
  else if (x < 1)
    return -1;

  return 0;
}

#else

double potential_V(double x, double x0)
{
  if (x0 < 1)
    {
      if (x < x0) return -x + (x0 + 1) / 2;
      if (x < 1) return (1 - x) * (1 - x) / (2 * (1 - x0));
    }
  else if (x < 1)
    return 1 - x;

  return 0;
}

double potential_dV(double x, double x0)
{
  if (x0 < 1)
    {
      if (x < x0) return -1;
      if (x < 1) return -(1 - x) / (1 - x0);
    }
  else if (x < 1)
    return -1;

  return 0;
}

#endif

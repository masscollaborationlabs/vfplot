#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/bilinear.h"
#include "plot/anynan.h"
#include "plot/garray.h"
#include "plot/gstack.h"
#include "plot/macros.h"
#include "plot/bbox.h"

#include <geom2d/vec.h>
#include <geom2d/mat.h>

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

struct bilinear_t
{
  struct {
    size_t x, y, z;
  } n;
  bbox_t bbox;
  double *v;
};

bilinear_t* bilinear_new(size_t nx, size_t ny, size_t nz)
{
  bilinear_t *B;

  if ((B = malloc(sizeof(bilinear_t))) != NULL)
    {
      size_t n = nx * ny * nz;
      double *v;

      if ((v = calloc(n, sizeof(double))) != NULL)
        {
          for (size_t i = 0 ; i < n ; i++)
            v[i] = NAN;

          B->n.x = nx;
          B->n.y = ny;
          B->n.z = nz;
          B->v = v;

          return B;
        }
      free(B);
    }
  return NULL;
}

void bilinear_destroy(bilinear_t *B)
{
  if (B != NULL)
    free(B->v);
  free(B);
}

void bilinear_bbox_set(bilinear_t *B, bbox_t bbox)
{
  B->bbox = bbox;
}

bbox_t bilinear_bbox_get(const bilinear_t *B)
{
  return B->bbox;
}

size_t bilinear_nx_get(const bilinear_t *B)
{
  return B->n.x;
}

size_t bilinear_ny_get(const bilinear_t *B)
{
  return B->n.y;
}

void bilinear_xy_get(const bilinear_t *B, size_t i, size_t j, double xy[2])
{
  size_t
    nx = B->n.x,
    ny = B->n.y;
  bbox_t
    bbox = B->bbox;

  xy[0] = (i * bbox.x.max + (nx - 1 - i) * bbox.x.min) / (nx - 1);
  xy[1] = (j * bbox.y.max + (ny - 1 - j) * bbox.y.min) / (ny - 1);
}

/* index of tuple, multiply by arity to get location in double arry */

static size_t tuple_index(const bilinear_t *B, size_t i, size_t j)
{
  return j * B->n.x + i;
}

/*
  we allow integer indices here, since we want to represent large
  grid of points extending in all directions (truncation of putative
  (x, y) pairs would give those), even though we can only interpolate
  on that subgrid rectangle of non-negative integers.

  this should be called prior to bilinear_z_get etc. if there
  is any possibility that those indices might be out of bounds, but
  one does not need to do that in manually iterating over the grid
  in function this module, that would be wasted effort.
*/

bool bilinear_valid(const bilinear_t *B, int i, int j)
{
  return
    (0 <= i) && ((size_t)i < B->n.x) &&
    (0 <= j) && ((size_t)j < B->n.y);
}

void bilinear_z_get(const bilinear_t *B, size_t i, size_t j, double *z)
{
  size_t nz = B->n.z;
  double *tuple = B->v + (tuple_index(B, i, j) * nz);
  memcpy(z, tuple, nz * sizeof(double));
}

void bilinear_z_set(bilinear_t *B, size_t i, size_t j, const double *z)
{
  size_t nz = B->n.z;
  double *tuple = B->v + (tuple_index(B, i, j) * nz);
  memcpy(tuple, z, nz * sizeof(double));
}

int bilinear_each(bilinear_t *B, bilinear_iterator_t *f, void *arg)
{
  size_t
    nx = B->n.x,
    ny = B->n.y;

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
        {
          int err;

          if ((err = f(B, i, j, arg)) != 0)
            return err;
        }
    }

  return 0;
}

static int csv_write(bilinear_t *B, size_t i, size_t j, void *arg)
{
  size_t nz = B->n.z;
  double z[nz];
  double xy[2];

  bilinear_xy_get(B, i, j, xy);
  bilinear_z_get(B, i, j, z);

  if (! anynan(nz, z))
    {
      FILE *stream = arg;

      fprintf(stream, "%g,%g", xy[0], xy[1]);

      for (size_t k = 0 ; k < nz ; k++)
        fprintf(stream, ",%g", z[k]);

      fprintf(stream, "\r\n");
    }

  return 0;
}

int bilinear_csv_write(const bilinear_t *B, FILE *stream)
{
  return bilinear_each((bilinear_t*)B, csv_write, stream);
}

/* helper for eval */

static void zij(const bilinear_t *B, int i, int j, double *z)
{
  if (bilinear_valid(B, i, j))
    bilinear_z_get(B, i, j, z);
  else
    {
      for (size_t k = 0 ; k < B->n.z ; k++)
        z[k] = NAN;
    }
}

/*
  This was bilinear() in the earlier scalar implementation, that
  behaved a little differently retruning an OK/NODATA/ERROR value,
  but since we might have some NODATA values, that would be a bit
  awkward, so now we just have an OK/ERROR return value, and the
  caller should inspect z[] for NANs if they care about that.
*/

int bilinear_eval(const bilinear_t *B, double x, double y, double *z)
{
  bbox_t bbox = B->bbox;
  size_t
    nx = B->n.x,
    ny = B->n.y,
    nz = B->n.z;
  double
    x0 = (nx - 1) * (x - bbox.x.min) / (bbox.x.max - bbox.x.min),
    y0 = (ny - 1) * (y - bbox.y.min) / (bbox.y.max - bbox.y.min);
  int
    i = floor(x0),
    j = floor(y0);
  double
    X = x0 - i,
    Y = y0 - j,
    z00[nz],
    z01[nz],
    z10[nz],
    z11[nz];

  zij(B, i, j, z00);
  zij(B, i, j + 1, z01);
  zij(B, i + 1, j, z10);
  zij(B, i + 1, j + 1, z11);

  for (size_t k = 0 ; k < nz ; k++)
    {
      double
        z00k = z00[k],
        z01k = z01[k],
        z10k = z10[k],
        z11k = z11[k];
      size_t nan_count =
        (isnan(z00k) ? 1 : 0) +
        (isnan(z01k) ? 1 : 0) +
        (isnan(z10k) ? 1 : 0) +
        (isnan(z11k) ? 1 : 0);

      z[k] = NAN;

      switch (nan_count)
        {
        case 0:

          z[k] =
            (z00k * (1 - X) + z10k * X) * (1 - Y) +
            (z01k * (1 - X) + z11k * X) * Y;
          break;

        case 1:

          if (isnan(z11k))
            {
              if (X + Y < 1)
                z[k] =
                  (z10k - z00k) * X +
                  (z01k - z00k) * Y +
                  z00k;
            }
          else if (isnan(z01k))
            {
              if (X > Y)
                z[k] =
                  (z10k - z00k) * X +
                  (z11k - z10k) * Y +
                  z00k;
            }
          else if (isnan(z10k))
            {
              if (X < Y)
                z[k] =
                  (z11k - z01k) * X +
                  (z01k - z00k) * Y +
                  z00k;
            }
          else
            {
              if (X + Y > 1)
                z[k] =
                  (z11k - z01k) * X +
                  (z11k - z10k) * Y +
                  z10k + z01k - z11k;
            }

          break;

        case 2:
        case 3:
        case 4:

          break;

        default:

          return 1;
        }
    }

  return 0;
}

static double bilinear_dx(const bilinear_t *B)
{
  double w = bbox_width(&(B->bbox));
  size_t nx = B->n.x;

  return w / (nx - 1);
}

static double bilinear_dy(const bilinear_t *B)
{
  double h = bbox_height(&(B->bbox));
  size_t ny = B->n.y;

  return h / (ny - 1);
}

static vec_t vecij(const bilinear_t *B, size_t i, size_t j)
{
  double z[2];
  bilinear_z_get(B, i, j, z);
  vec_t v = {z[0], z[1]};
  return v;
}

/*
  nandif(a, b, c) returns one of c - b,  b - a or (c-a)/2 if the result
  can be non-nan (ie, if at least 2 of them are non-nan) otherwise it
  returns nan
*/

static double nandif(double a, double b, double c)
{
  if (isnan(a))
    return ((isnan(b) || isnan(c)) ? NAN : c - b);

  if (isnan(c))
    return (isnan(b) ? NAN : b - a);

  return (c - a) / 2;
}

/*
  returns a newly allocated bilinear_t which holds the curvature of the
  input: the value is Ju where u is the unit vector field and J the
  Jacobian of u = (u, v)

  U = [ du/dx du/dy ]
      [ dv/dx dv/dy ]

  Note that there is a 1-pixel boundary of NaNs around the edge (as in
  the scalar version of this function), I think that could be removed
  using the "nandiff", FIXME
*/

bilinear_t* bilinear_curvature(const bilinear_t *B)
{
  if (B->n.z != 2)
    return NULL;

  size_t
    nx = B->n.x,
    ny = B->n.y;
  bilinear_t *Bc;

  if ((Bc = bilinear_new(nx, ny, 1)) == NULL)
    return NULL;

  bilinear_bbox_set(Bc, B->bbox);

  double
    dx = bilinear_dx(B),
    dy = bilinear_dy(B);

  for (size_t i = 1 ; i < nx - 1 ; i++)
    {
      for (size_t j = 1 ; j < ny - 1 ; j++)
        {
          vec_t
            v0 = vecij(B, i, j),
            vt = vecij(B, i, j + 1),
            vb = vecij(B, i, j - 1),
            vl = vecij(B, i - 1, j),
            vr = vecij(B, i + 1, j);

          vec_t
            u0 = vunit(v0),
            ut = vunit(vt),
            ub = vunit(vb),
            ul = vunit(vl),
            ur = vunit(vr);

          double
            dudx = nandif(ul.x, u0.x, ur.x) / dx,
            dudy = nandif(ub.x, u0.x, ut.x) / dy,
            dvdx = nandif(ul.y, u0.y, ur.y) / dx,
            dvdy = nandif(ub.y, u0.y, ut.y) / dy;

          mat_t M = {dudx, dudy, dvdx, dvdy};
          vec_t vk = mvmul(M, u0);
          double k = (bend2v(u0, vk) == rightward ? 1 : -1) * vabs(vk);

          bilinear_z_set(Bc, i, j, &k);
        }
    }

  return Bc;
}

/*
  integrate bilinear over the passed domain, we test whether a grid
  square is to be included in the integrand by checking whether its
  centre is inside the domain, we don't do anything fancy like trying
  to intersect the grid-square with the domain and using Green's theorem
  (that remains an option).  So there are plenty of edge-cases when this
  will be inaccurate.  But we don't need that much accuracy here, it's
  only to estimate the the ellipse density in calulating the intial fill.
*/

static inline double defint(double a, double b, double c, double d,
                            double X, double Y)
{
  double
    X2 = X / 2,
    Y2 = Y / 2;

  return X * Y * ( (a * (1 - X2) + b * X2) * (1 - Y2) +
                   (c * (1 - X2) + d * X2) * Y2 );
}

int bilinear_integrate(const bilinear_t *B, const domain_t *dom, double *I)
{
  size_t
    nx = B->n.x,
    ny = B->n.y,
    nz = B->n.z;
  bbox_t
    bbox = B->bbox;
  double
    dx = bilinear_dx(B),
    dy = bilinear_dy(B),
    sum[nz];

  for (size_t k = 0 ; k < nz ; k++)
    sum[k] = 0;

  for (size_t i = 0 ; i < nx - 1 ; i++)
    {
      double x = i * dx + bbox.x.min;

      for (size_t j = 0 ; j < ny - 1 ; j++)
        {
          double y = j * dy + bbox.y.min;
          vec_t midpoint = {x + dx / 2, y + dy / 2};

          if (! domain_inside(dom, midpoint))
            continue;

          double z00[nz], z01[nz], z10[nz], z11[nz];

          zij(B, i, j, z00);
          zij(B, i, j + 1, z01);
          zij(B, i + 1, j, z10);
          zij(B, i + 1, j + 1, z11);

          for (size_t k = 0 ; k < nz ; k++)
            {
              double
                z00k = z00[k],
                z01k = z01[k],
                z10k = z10[k],
                z11k = z11[k];

              if (isnan(z00k) || isnan(z01k) || isnan(z10k) || isnan(z11k))
                continue;

              sum[k] +=
                defint(z00k, z10k, z01k, z11k, 1, 1) -
                defint(z00k, z10k, z01k, z11k, 0, 1) -
                defint(z00k, z10k, z01k, z11k, 1, 0) +
                defint(z00k, z10k, z01k, z11k, 0, 0);
            }
        }
    }

  for (size_t k = 0 ; k < nz ; k++)
    I[k] = sum[k] * dx * dy;

  return 0;
}

/*
  returns a domain_t structure which is the piecewise linear boundary
  of the region on which the interpolant is defined.

  This is rather complicated, probably should be looked at again.

  We use as a basic data structure a cell_t, a square with a datapoint
  at each corner

  TL   TR
    * *
    * *
  BL   BR

  encoded in an unsigned char
*/

typedef unsigned char cell_t;

#define CELL_BL ((cell_t) (1 << 0))
#define CELL_BR ((cell_t) (1 << 1))
#define CELL_TL ((cell_t) (1 << 2))
#define CELL_TR ((cell_t) (1 << 3))

#define CELL_NONE ((cell_t)0)
#define CELL_ALL  (CELL_BL | CELL_BR | CELL_TL | CELL_TR)

static void point(size_t i, size_t j, cell_t m, gstack_t *st)
{
  switch (m)
    {
    case CELL_BL: break;
    case CELL_BR: i++ ; break;
    case CELL_TL: j++ ; break;
    case CELL_TR: i++ ; j++ ; break;
    }

  size_t v[2] = {i, j};

  gstack_push(st, (void*)v);
}

static int i2eq(const size_t *a, const size_t *b)
{
  return (a[0] == b[0]) && (a[1] == b[1]);
}

static void i2sub(const size_t *a, const size_t *b, size_t *c)
{
  for (size_t i = 0 ; i < 2 ; i++)
    c[i] = a[i] - b[i];
}

static bool i2colinear(const size_t *a, const size_t *b, const size_t *c)
{
  size_t u[2], v[2];

  i2sub(b, a, u);
  i2sub(c, b, v);

  return u[1] * v[0] == u[0] * v[1];
}

/* integer points & flag, used in trace() */

typedef struct
{
  size_t p[2];
  bool del;
} ipf_t;

/*
   given a list of n ipf_t, move all of the elements with a the del
   flag not set to the start of the list retaining the order, return
   the number of such
*/

static size_t compact(ipf_t *ipf, size_t n)
{
  size_t j = 0;

  for (size_t i = 0 ; i < n ; i++)
    {
      if (ipf[i].del)
        continue;
      ipf[j] = ipf[i];
      j++;
    }

  return j;
}

static size_t deldups(ipf_t *ipf, size_t n)
{
  for (size_t i = 0 ; i < n - 1 ; i++)
    if ( i2eq(ipf[i].p, ipf[i + 1].p) )
      ipf[i + 1].del = true;

  if ( i2eq(ipf[n - 1].p, ipf[0].p) )
    ipf[0].del = true;

  return compact(ipf, n);
}

/* delete colinear points in an list of ifps */

static size_t delcols(ipf_t *ipf, size_t n)
{
  if ( i2colinear(ipf[n-1].p, ipf[0].p, ipf[1].p) )
    ipf[0].del = true;

  for (size_t i = 0 ; i < n - 2 ; i++)
    if ( i2colinear(ipf[i].p, ipf[i + 1].p, ipf[i + 2].p) )
      ipf[i + 1].del = true;

  if ( i2colinear(ipf[n - 2].p, ipf[n - 1].p, ipf[0].p))
    ipf[n - 1].del = true;

  return compact(ipf, n);
}

static int trace_stack(cell_t **c, size_t i, size_t j, gstack_t *st)
{
  /*
    machine startup
    FIXME handle CELL_TL | CELL_BR here too
  */

  switch (c[i][j])
    {
    case CELL_TR:
    case CELL_TR | CELL_TL:
    case CELL_TR | CELL_TL | CELL_BL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TR, st);
      goto move_right;
    case CELL_TL:
    case CELL_TL | CELL_BL:
    case CELL_TL | CELL_BL | CELL_BR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TL, st);
      goto move_up;
    case CELL_BL:
    case CELL_BL | CELL_BR:
    case CELL_BL | CELL_BR | CELL_TR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BL, st);
      goto move_left;
    case CELL_BR:
    case CELL_BR | CELL_TR:
    case CELL_BR | CELL_TR | CELL_TL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BR, st);
      goto move_down;
    default:
      return 1;
    }

  /*
    simple motion states
  */

 move_right:

  i++;
  switch (c[i][j])
    {
    case CELL_NONE:
      goto move_end;
    case CELL_TL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TL, st);
      goto move_up;
    case CELL_TL | CELL_BR:
      c[i][j] = CELL_BR;
      point(i, j, CELL_TL, st);
      goto move_up;
    case CELL_TL | CELL_TR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TR, st);
      goto move_right;
    case CELL_TL | CELL_TR | CELL_BR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BR, st);
      goto move_down;
    default:
      return 1;
    }

 move_up:

  j++;
  switch (c[i][j])
    {
    case CELL_NONE:
      goto move_end;
    case CELL_BL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BL, st);
      goto move_left;
    case CELL_BL | CELL_TR:
      c[i][j] = CELL_TR;
      point(i, j, CELL_BL, st);
      goto move_left;
    case CELL_BL | CELL_TL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TL, st);
      goto move_up;
    case CELL_BL | CELL_TL | CELL_TR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TR, st);
      goto move_right;
    default:
      return 1;
    }

 move_left:

  i--;
  switch (c[i][j])
    {
    case CELL_NONE:
      goto move_end;
    case CELL_BR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BR, st);
      goto move_down;
    case CELL_BR | CELL_TL:
      c[i][j] = CELL_TL;
      point(i, j, CELL_BR, st);
      goto move_down;
    case CELL_BR | CELL_BL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BL, st);
      goto move_left;
    case CELL_BR | CELL_BL | CELL_TL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TL, st);
      goto move_up;
    default:
      return 1;
    }

 move_down:

  j--;
  switch (c[i][j])
    {
    case CELL_NONE:
      goto move_end;
    case CELL_TR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TR, st);
      goto move_right;
    case CELL_TR | CELL_BL:
      c[i][j] = CELL_BL;
      point(i, j, CELL_TR, st);
      goto move_right;
    case CELL_TR | CELL_BR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BR, st);
      goto move_down;
    case CELL_TR | CELL_BR | CELL_BL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BL, st);
      goto move_left;
    default:
      return 1;
    }

 move_end:

  return 0;
}

static int ipf_load(gstack_t *st, size_t n, ipf_t *ipf)
{
  for (size_t i = 0 ; i < n ; i++)
    {
      if (gstack_pop(st, ipf[i].p) != 0)
        return 1;
      ipf[i].del = false;
    }

  return 0;
}

#define TRSTACK_INIT 512
#define TRSTACK_INCR 512

static int trace(const bilinear_t *B, cell_t **c,
                 size_t i, size_t j, domain_t *dom)
{
  switch (c[i][j])
    {
    case CELL_ALL:
    case CELL_NONE:
      return 0;
    }

  gstack_t *st;

  if ((st = gstack_new(2 * sizeof(size_t), TRSTACK_INIT, TRSTACK_INCR)) == NULL)
    return 1;

  int err = 0;
  ipf_t *ipf = NULL;
  size_t n;

  if (trace_stack(c, i, j, st) != 0)
    err++;
  else
    {
      n = gstack_size(st);

      switch (n)
	{
	case 0:
	  err++;
	  break;

	case 1:
	case 2:
	  /* obviously degenerate, ignore and succeed  */
	  break;

	default:
	  if ((ipf = malloc(n * sizeof(ipf_t))) != NULL)
	    {
	      if (ipf_load(st, n, ipf) != 0)
		err++;
	    }

	}
    }

  gstack_destroy(st);

  if ((ipf == NULL) || err)
    return err;

  /*
    first look for consecutive points which are equal and delete the
    later one
  */

  if ((n = deldups(ipf, n)) < 3)
    {
      free(ipf);
      return 0;
    }

  /*
    mark colinear - note that if the ifp array contained consecutive
    equal points then both of these would be marked for deletion (in
    a-b-b-c, a-b-b and b-b-c are colinear so that both bs are deleted).
  */

  if ((n = delcols(ipf, n)) < 3)
    {
      free(ipf);
      return 0;
    }

  /*
    removing colinear nodes can introduce duplicates, so check for
    those again
  */

  if ((n = deldups(ipf, n)) < 3)
    {
      free(ipf);
      return 0;
    }

  /* transfer to polygon with m segments */

  polygon_t p;

  if (polygon_init(n, &p) != 0)
    {
      free(ipf);
      return 1;
    }

  for (size_t i = 0 ; i < n ; i++)
    {
      double xy[2];

      bilinear_xy_get(B, ipf[i].p[0] - 1, ipf[i].p[1] - 1, xy);

      p.v[i].x = xy[0];
      p.v[i].y = xy[1];
    }

  free(ipf);

  if ((domain_insert(dom, &p)) != 0)
    {
      return 1;
    }

  polygon_clear(&p);

  if (domain_orientate(dom) != 0)
    return 1;

  return 0;
}

domain_t* bilinear_domain(const bilinear_t *B)
{
  size_t
    nx = B->n.x,
    ny = B->n.y,
    nz = B->n.z;
  cell_t **g;

  /*
    create garray (generic array) of cell_t with the interior detemined
    by the nan-ness of the nodes of the bilinear grid
  */

  /* coverity[suspicious_sizeof : FALSE] */

  if (!(g = (cell_t**)garray_new(nx + 1, ny + 1, sizeof(cell_t))))
    return NULL;

  for (size_t i = 0 ; i < nx + 1 ; i++)
    for (size_t j = 0 ; j < ny + 1 ; j++)
      g[i][j] = CELL_NONE;

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
        {
          double z[nz];
          bilinear_z_get(B, i, j, z);

          if (! anynan(nz, z))
            {
              g[i + 1][j + 1] |= CELL_BL;
              g[i + 1][j] |= CELL_TL;
              g[i][j + 1] |= CELL_BR;
              g[i][j] |= CELL_TR;
            }
        }
    }

  domain_t *dom;

  if ((dom = domain_new()) == NULL)
    goto cleanup;

  /* edge trace from each cell */

  for (size_t i = 0 ; i < nx + 1 ; i++)
    {
      for (size_t j = 0 ; j < ny + 1 ; j++)
	{
	  if (trace(B, g, i, j, dom) != 0)
	    {
	      domain_destroy(dom);
	      dom = NULL;

	      goto cleanup;
	    }
	}
    }

 cleanup:

  garray_destroy((void**)g);

  return dom;
}

#ifndef PLOT_BBOX_H
#define PLOT_BBOX_H

typedef struct
{
  struct
  {
    double min, max;
  } x, y;
} bbox_t;

void bbox_join(const bbox_t*, const bbox_t*, bbox_t*);
double bbox_width(const bbox_t*);
double bbox_height(const bbox_t*);
double bbox_area(const bbox_t*);

#endif

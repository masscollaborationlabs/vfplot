#ifndef PLOT_FILL_H
#define PLOT_FILL_H

typedef enum { fill_none, fill_grey, fill_rgb } fill_type_t;

typedef unsigned char grey_t;
typedef struct { unsigned char r, g, b; } rgb_t;

typedef struct
{
  fill_type_t type;
  union
  {
    grey_t grey;
    rgb_t rgb;
  };
} fill_t;

#endif

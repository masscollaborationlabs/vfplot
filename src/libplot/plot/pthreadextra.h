#ifndef PLOT_PTHREADEXTRA_H
#define PLOT_PTHREADEXTRA_H

/*
  This is taken from Aleksey Demakov [1] (with just cosmetic
  changes) implementing the pthread barrier functionality
  missing from OSX.  The licence is BSD 2-Clause "Simplified"

  [1] https://github.com/ademakov/DarwinPthreadBarrier
*/

#include <pthread.h>

#if !defined(PTHREAD_BARRIER_SERIAL_THREAD)
#define PTHREAD_BARRIER_SERIAL_THREAD (1)
#endif

#if !defined(PTHREAD_PROCESS_PRIVATE)
#define PTHREAD_PROCESS_PRIVATE (42)
#endif

#if !defined(PTHREAD_PROCESS_SHARED)
#define PTHREAD_PROCESS_SHARED (43)
#endif

typedef struct {

} pthread_barrierattr_t;

typedef struct {
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  unsigned int limit;
  unsigned int count;
  unsigned int phase;
} pthread_barrier_t;

int pthread_barrierattr_init(pthread_barrierattr_t*);
int pthread_barrierattr_destroy(pthread_barrierattr_t*);
int pthread_barrierattr_getpshared(const pthread_barrierattr_t *restrict,
                                   int *restrict);
int pthread_barrierattr_setpshared(pthread_barrierattr_t*, int);
int pthread_barrier_init(pthread_barrier_t *restrict,
                         const pthread_barrierattr_t *restrict,
                         unsigned int);
int pthread_barrier_destroy(pthread_barrier_t*);
int pthread_barrier_wait(pthread_barrier_t*);

#endif

#ifndef PLOT_GSTATE_H
#define PLOT_GSTATE_H

#include <stdlib.h>
#include <stdio.h>

/*
  this reads and writes the collection of arrows and
  network neighbours to/from a file. The read function
  allocates for the data and the caller is expected to
  free it.
*/

#include <plot/arrows.h>
#include <plot/nbrs.h>

typedef struct {
  arrows_t arrows;
  nbrs_t nbrs;
} gstate_t;

int gstate_read(FILE*, gstate_t*);
int gstate_write(FILE*, const gstate_t*);

#endif

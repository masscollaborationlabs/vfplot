#ifndef PLOT_ELLIPSE_BASE_H
#define PLOT_ELLIPSE_BASE_H

#include <plot/bilinear.h>
#include <plot/arrow.h>
#include <plot/bbox.h>
#include <plot/evaluate.h>

#include <geom2d/ellipse.h>

#include <stddef.h>

typedef bilinear_t ellipse_base_t;

ellipse_base_t* ellipse_base_new(bbox_t, size_t, size_t,
                                 const evaluate_t*, const arrow_opt_t*);
void ellipse_base_destroy(ellipse_base_t*);
int ellipse_base_get(const ellipse_base_t*, size_t, size_t, ellipse_t*);
size_t ellipse_base_nx(const ellipse_base_t*);
size_t ellipse_base_ny(const ellipse_base_t*);
bbox_t ellipse_base_bbox(const ellipse_base_t*);



#endif

#ifndef PLOT_ANYNAN_H
#define PLOT_ANYNAN_H

#include <stdbool.h>
#include <stddef.h>

bool anynan(size_t, const double*);

#endif

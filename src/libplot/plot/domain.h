#ifndef PLOT_DOMAIN_H
#define PLOT_DOMAIN_H

#include <plot/bbox.h>
#include <plot/polygon.h>

#include <geom2d/vec.h>

#include <stdbool.h>

typedef struct domain_t domain_t;

domain_t* domain_new(void);
domain_t* domain_clone(const domain_t*);
void domain_destroy(domain_t*);

typedef int (*difun_t)(polygon_t*, void*, int);
int domain_each(domain_t*, difun_t, void*);

int domain_insert(domain_t*, const polygon_t*);
int domain_orientate(domain_t*);
bool domain_inside(const domain_t*, vec_t);
double domain_area(const domain_t*);
int domain_bbox(const domain_t*, bbox_t*);
int domain_scale(domain_t*, double, double, double);
int domain_hierarchy_check(const domain_t*);
int domain_depth(const domain_t*);
int domain_width(const domain_t*);

#endif

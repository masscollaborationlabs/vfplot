#ifndef PLOT_GARRAY_H
#define PLOT_GARRAY_H

#include <stdlib.h>

void** garray_new(size_t, size_t, size_t);
void garray_destroy(void**);

#endif

#ifndef PLOT_CSV_READ_H
#define PLOT_CSV_READ_H

#include <stdio.h>

typedef struct
{
  double min, max;
} minmax_t;

typedef struct
{
  FILE *st;
  struct
  {
    size_t n[2];
    minmax_t bnd[2];
  } grid;
} csv_read_t;

#define CSV_READ_OK 0
#define CSV_READ_NODATA 1
#define CSV_READ_ERROR 2
#define CSV_READ_EOF 3

int csv_read_open(const char*, csv_read_t*);
int csv_read_line(const csv_read_t*, size_t[static 2], double[static 2]);
void csv_read_close(csv_read_t*);

#endif

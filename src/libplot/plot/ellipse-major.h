#ifndef PLOT_ELLIPSE_MAJOR_H
#define PLOT_ELLIPSE_MAJOR_H

#include <plot/ellipse-base.h>

#include <stddef.h>

typedef bilinear_t ellipse_major_t;

ellipse_major_t* ellipse_major_new(const ellipse_base_t*);
void ellipse_major_destroy(ellipse_major_t*);
int ellipse_major_integrate(const ellipse_major_t*, const domain_t*, double*);
double ellipse_major_get(const ellipse_major_t*, size_t, size_t);

#endif

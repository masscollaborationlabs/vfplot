#ifndef PLOT_PLOT_H
#define PLOT_PLOT_H

#include <stdbool.h>
#include <stddef.h>

typedef bool bool_t;

#include <plot/error.h>
#include <plot/fill.h>
#include <plot/domain.h>
#include <plot/page.h>
#include <plot/nbrs.h>
#include <plot/arrows.h>
#include <plot/vfun.h>

/*
  sorting strategy, for sort_longest the longest arrows are plotted
  last (and so are the top layers)
*/

typedef enum
{
  sort_none,
  sort_longest,
  sort_shortest,
  sort_bendiest,
  sort_straightest
} sort_type_t;

/* symbol type */

typedef enum
{
  symbol_arrow,
  symbol_triangle
} symbol_t;

/* output format type */

typedef enum
{
  output_format_eps,
  output_format_povray
} output_format_t;

/* pen */

typedef struct { double width; int grey; } pen_t;

/* adaptive specific options */

typedef enum
{
  break_none,
  break_grid,
  break_super,
  break_midclean,
  break_postclean
} break_t;

typedef struct
{
  unsigned int main, euler, populate;
} iterations_t;

typedef struct
{
  bool active;
  struct {
    double min, max;
  } x, y;
} crop_t;

/*
  plot options structure passed to library, describes how to do the
  plotting
*/

typedef struct
{
  int threads;

  /* placement specific options */

  union
  {
    struct
    {
      const char *animate;
      break_t breakout;
      iterations_t iter;
      int mtcache;
      double overfill;
      double timestep;
      double kedrop;
      char *histogram;

      struct {
	pen_t pen;
      } domain;

      struct {
	pen_t pen;
      } network;

      struct {
	double major, minor, rate;
      } margin;

    } adaptive;

    /*
      previously we had a struct 'hedgehog' here with a couple
      of values, but those are no-longer used, and an empty
      placeholder struct generates a -pedantic waring

      struct { } hedgehog;
    */

  } place;

  /* output files */

  struct
  {
    struct
    {
      output_format_t format;
      char *path;
    } output;
    struct
    {
      char *path;
    } dump;
  } file;

  /*
    n       : maximum number of arrows
    epsilon : straight-arrow threshold (see manual)
    scale   : arrow scaling
    fill    : arrow fill
    pen     : pen width
    head    : ratios of head length & width with shaft width.
  */

  struct
  {
    double aspect;
    double epsilon;
    double scale;
    int n;
    fill_t fill;
    sort_type_t sort;
    pen_t pen;
    symbol_t symbol;
    struct { double length, width; } head;
    struct { double max, min; } length;
    bool autoscale;
  } arrow;

  struct
  {
    pen_t pen;
    fill_t fill;
  } ellipse;

  struct
  {
    pen_t pen;
  } domain;

  /*
    plot geometry

    the domain has the x-y coordinates used by the plot constructors
    below. Once constructed the xy domain is shifted to the origin
    and scaled accordingly

    these need to be initaialised with plot_opt_init() before calling
  */

  bbox_t bbox;
  page_t page;

  /*
    the action of the cropping depends on the page size, but does
    not modify it.
  */

  crop_t crop;

} plot_opt_t;

/*
  the options struct must be initialised with iniopt() (setting the
  geometry) before calling a constructor
*/

int plot_opt_init(bbox_t, plot_opt_t*);

/*
  These output functions create the plots, they differ only in the
  reporting thet they do (the frame version is used in making an
  animation, so is called hundreds of times, so that is quiet).
*/

int plot_output(const domain_t*, const arrows_t*, const nbrs_t*, const plot_opt_t*);
int plot_frame(const domain_t*, const arrows_t*, const nbrs_t*, const plot_opt_t*);

#endif

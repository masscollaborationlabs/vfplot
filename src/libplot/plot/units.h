#ifndef PLOT_UNITS_H
#define PLOT_UNITS_H

#include <stdio.h>

double unit_ppt(char);
const char* unit_name(char);
int unit_list_stream(FILE*);

#endif

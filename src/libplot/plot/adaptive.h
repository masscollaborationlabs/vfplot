#ifndef PLOT_ADAPTIVE_H
#define PLOT_ADAPTIVE_H

#include <plot/plot.h>

#include <plot/arrows.h>
#include <plot/nbrs.h>

int plot_adaptive(const domain_t*,
                  vfun_t*, cfun_t*, void*,
                  plot_opt_t*,
                  arrows_t*,
                  nbrs_t*);

#endif

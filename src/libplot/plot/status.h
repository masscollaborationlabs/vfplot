#ifndef PLOT_STATUS_H
#define PLOT_STATUS_H

void status_set_length(int, int);
void status(const char*, int);

#endif

#ifndef PLOT_ELLIPSE_MT_H
#define PLOT_ELLIPSE_MT_H

#include <plot/ellipse-base.h>

#include <geom2d/mat.h>

#include <stddef.h>

/*
  holds the metric tensor, a function on the rectangle
  with matrix values

    [a b]
    [b c]

  ie, it is symmetric-matrix valued. We represent it with
  3 bilinear meshes (bilinear.h). In addition we hold a mesh
  of the area of the corresponding ellipse at that point.

  The original version of this struct was 5 1-bilinear meshs,
  three for a, b, c; then one each for area and the major
  axis.  This is just a single 3-bilinear mesh (so opaque)
*/

typedef bilinear_t ellipse_mt_t;

ellipse_mt_t* ellipse_mt_new(const ellipse_base_t*);
void ellipse_mt_destroy(ellipse_mt_t*);
int ellipse_mt_eval(const ellipse_mt_t*, vec_t, mat_t*);
int ellipse_mt_mat_get(const ellipse_mt_t*, size_t, size_t, mat_t*);
size_t ellipse_mt_nx(const ellipse_mt_t*);
size_t ellipse_mt_ny(const ellipse_mt_t*);
bbox_t ellipse_mt_bbox(const ellipse_mt_t*);

#endif

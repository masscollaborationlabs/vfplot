#ifndef PLOT_NBR_H
#define PLOT_NBR_H

#include <geom2d/vec.h>

#include <stdint.h>

typedef struct
{
  struct
  {
    uint32_t id;
    vec_t v;
  } a, b;
} nbr_t;

#endif

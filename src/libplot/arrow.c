#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/arrow.h"
#include "plot/limits.h"
#include "plot/margin.h"

#include <geom2d/sincos.h>

#include <stdlib.h>
#include <math.h>

/*
  arrow_ellipse() - calculate the major/minor axis of the ellipse
  proximal to the arrow
*/

static void arrow_proximal_ellipse(const arrow_t *A, ellipse_t *E)
{
  double
    wdt = A->width,
    len = A->length,
    curv = A->curv;

  ellipse_t e;

  if (curv * RADCRV_MAX > 1)
    {
      double
        r = 1 / curv,
        psi = len * curv,
        sp2, cp2;

      sincos(psi / 2, &sp2, &cp2);

      e.minor = r * (1 - cp2) + wdt / 2;
      e.major = r * sp2 + wdt / 2;
    }
  else
    {
      /*
	 arrow almost straight, so psi is small -- use the
	 first term in the taylor series for the cos/sin
      */

      e.minor = len * len * curv / 8 + wdt / 2;
      e.major = len / 2 + wdt / 2;
    }

  e.centre = A->centre;
  e.theta = A->theta;

  *E = e;
}

void arrow_ellipse(const arrow_t *A, const arrow_opt_t *opt, ellipse_t *E)
{
  arrow_proximal_ellipse(A, E);

  E->major += margin((E->major) * opt->scale, opt->bmaj, opt->M) / opt->scale;
  E->minor += margin((E->minor) * opt->scale, opt->bmin, opt->M) / opt->scale;
}

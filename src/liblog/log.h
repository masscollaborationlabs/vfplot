/*
  Copyright (c) 2020 rxi

  This library is free software; you can redistribute it and/or modify it
  under the terms of the MIT license. See `logger.c` for details.
*/

#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <time.h>

#define LOGGER_VERSION "0.1.0"

typedef struct {
  va_list ap;
  const char *fmt;
  const char *file;
  struct tm *time;
  void *udata;
  int line;
  int level;
} log_event_t;

typedef void (log_lock_t)(bool, void*);

enum { LOG_TRACE, LOG_DEBUG, LOG_INFO, LOG_WARN, LOG_ERROR, LOG_FATAL };

#define log_trace(...) log_log(LOG_TRACE, __FILE__, __LINE__, __VA_ARGS__)
#define log_debug(...) log_log(LOG_DEBUG, __FILE__, __LINE__, __VA_ARGS__)
#define log_info(...) log_log(LOG_INFO, __FILE__, __LINE__, __VA_ARGS__)
#define log_warn(...) log_log(LOG_WARN, __FILE__, __LINE__, __VA_ARGS__)
#define log_error(...) log_log(LOG_ERROR, __FILE__, __LINE__, __VA_ARGS__)
#define log_fatal(...) log_log(LOG_FATAL, __FILE__, __LINE__, __VA_ARGS__)

const char* log_level_string(int);
int log_level_of_string(const char*);
void log_set_lock(log_lock_t*, void*);
void log_set_level(int);
int log_add_decorated(FILE*, int);
int log_add_plain(FILE*, int);
void log_log(int, const char*, int, const char*, ...);
void log_reset(void);

#endif

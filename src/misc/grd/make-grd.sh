#!/bin/sh

set -e

xyz2grd -Gnode3.grd -R0/1/1/2 -I1 <<EOF
0 1 0.1
0 2 0.2
1 1 0.3
1 2 0.4
EOF
nc3tonc4 --quiet=1 -o node3.grd node4.grd
ncdump node3.grd > node3.dump
ncdump node4.grd > node4.dump

xyz2grd -Gpixel3.grd -R0/1/1/2 -I0.5 -r <<EOF
0.25 1.25 0.1
0.25 1.75 0.2
0.75 1.25 0.3
0.75 1.75 0.4
EOF
nc3tonc4 --quiet=1 -o pixel3.grd pixel4.grd
ncdump pixel3.grd > pixel3.dump
ncdump pixel4.grd > pixel4.dump

rm -f gmt.history

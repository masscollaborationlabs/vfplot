GMT netCDF files
----------------

For development: some minimal examples of node (gridline) and
pixel aligned GMT grd files. Each is 2x2 and covers the range
[0, 1] x [1, 2].  The files are numbered by netCDF version, the
`3` versions are **xyz2grd** output (from CSV), the `4` versions
are generated from the `3` using **nc3tonc4**, and the dump
files are generated using **ncdump** for all cases.

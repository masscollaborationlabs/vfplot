#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "stats.h"

#ifdef HAVE_JANSSON_H
#include <jansson.h>
#endif

#ifdef HAVE_LIBJANSSON

static json_t* cpu_json(const stats_t *stats)
{
  json_t *json;

  if ((json = json_object()) == NULL)
    return NULL;

  switch (stats->cpu_type)
    {
    case cpu_rusage:
      json_object_set_new(json, "user", json_real(stats->cpu.rusage.user));
      json_object_set_new(json, "sys", json_real(stats->cpu.rusage.sys));
      break;
    case cpu_wall:
      json_object_set_new(json, "wall", json_real(stats->cpu.wall));
      break;
    case cpu_error:
      break;
    }

  return json;
}

static json_t* stats_json(const stats_t *stats)
{
  json_t *json;

  if ((json = json_object()) == NULL)
    return NULL;

  json_object_set_new(json, "version", json_string(stats->version));
  json_object_set_new(json, "cpu", cpu_json(stats));

  switch (stats->elapsed_type)
    {
    case elapsed_present:
      json_object_set_new(json, "elapsed", json_real(stats->elapsed));
      break;
    case elapsed_absent:
      break;
    }

  switch (stats->size_type)
    {
    case size_present:
      json_object_set_new(json, "size", json_integer(stats->size));
      break;
    case size_absent:
      break;
    }

  return json;
}

int stats_write(FILE *stream, const stats_t *stats)
{
  json_t *json;
  int err = 0;
  size_t flags = JSON_INDENT(2) | JSON_REAL_PRECISION(10);

  if ((json = stats_json(stats)) != NULL)
    {
      if (json_dumpf(json, stream, flags) != 0)
        err++;
      json_decref(json);
    }
  else
    err++;

  return ((err > 0) ? 1 : 0);
}

#else

/*
  silent error in case this is actually called, it really shouldn't
  be, instead the command-line parser should error if --statistics
  in given but there in no JSON support at configure, this is just
  a stub implementation to keep the compiler happy.
*/

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
int stats_write(FILE *st, const stats_t *stats)
{
  return 1;
}
#pragma GCC diagnostic pop

#endif

/*
  spvol.h
  Copyright (c) J.J. Green 2020
*/

#ifndef SPVOL_H
#define SPVOL_H

#include <stddef.h>

int spvol(size_t, double*);

#endif

/*
  page.h
  Copyright (c) J.J. Green 2019
*/

#ifndef PAGE_H
#define PAGE_H

#include <stdlib.h>

int page_size(size_t*);

#endif

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/arrow.h>
#include "tests-arrow.h"

CU_TestInfo tests_arrow[] =
  {
   {"ellipse of semi-circle", test_arrow_ellipse_semicircle},
   CU_TEST_INFO_NULL,
  };

/*
  shaft is a radius 1 semi-circle centred on (0, 0), so the
  bounding ellipse is the unit disk
*/

void test_arrow_ellipse_semicircle(void)
{
  double eps = 1e-10;
  arrow_t A = {{0, 0}, rightward, 0, M_PI, 0, 1};
  ellipse_t E;
  arrow_opt_t opt = {1, 0, 0, 1};

  arrow_ellipse(&A, &opt, &E);

  CU_ASSERT_DOUBLE_EQUAL(E.centre.x, 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(E.centre.y, 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(E.major, 2, eps);
  CU_ASSERT_DOUBLE_EQUAL(E.minor, 2, eps);
  CU_ASSERT_DOUBLE_EQUAL(E.theta, 0, eps);
}

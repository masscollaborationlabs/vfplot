#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/bbox.h>
#include "assert-bbox.h"
#include "tests-bbox.h"

CU_TestInfo tests_bbox[] = {
  {"height", test_bbox_height},
  {"width", test_bbox_width},
  {"area", test_bbox_area},
  {"join", test_bbox_join},
  CU_TEST_INFO_NULL,
};

static double eps = 1e-10;

void test_bbox_width(void)
{
  bbox_t a = {{0, 1}, {0, 2}};
  CU_ASSERT_DOUBLE_EQUAL(bbox_width(&a), 1, eps);
}

void test_bbox_height(void)
{
  bbox_t a = {{0, 1}, {0, 2}};
  CU_ASSERT_DOUBLE_EQUAL(bbox_height(&a), 2, eps);
}

void test_bbox_area(void)
{
  bbox_t a = {{0, 1}, {0, 2}};
  CU_ASSERT_DOUBLE_EQUAL(bbox_area(&a), 2, eps);
}

void test_bbox_join(void)
{
  bbox_t
    a = {{0, 1}, {0, 1}},
    b = {{1, 2}, {1, 2}},
    c0,
    c1 = {{0, 2}, {0, 2}};

  bbox_join(&a, &b, &c0);
  CU_ASSERT_BBOX_EQUAL(c0, c1, eps);
}

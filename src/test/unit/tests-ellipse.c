#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <geom2d/ellipse.h>

#include "tests-ellipse.h"

CU_TestInfo tests_ellipse[] =
  {
    {"ellipse to metric tensor", test_ellipse_mat},
    {"metric tensor to ellipse", test_mat_ellipse},
    {"tangent points", test_ellipse_tangent_points},
    CU_TEST_INFO_NULL,
  };

static double eps = 1e-10;

void test_ellipse_mat(void)
{
  ellipse_t e = {2, 1, M_PI / 2, {1, 0}};
  mat_t M;

  CU_ASSERT_EQUAL(ellipse_mat(&e, &M), 0);

  CU_ASSERT_DOUBLE_EQUAL(M.a, 1.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(M.b, 0.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(M.c, 0.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(M.d, 4.0, eps);
}

/*
   the major and minor axes are easy, the angle
   is the hard-case so test a few of those
*/

void test_mat_ellipse(void)
{
  ellipse_t e0 = {4, 3, 0, {1, 3}}, e1;
  int n = 20;

  for (int i = 0 ; i < n ; i++)
    {
      e0.theta = 4 * (i - 1) * M_PI / n;

      mat_t M;

      CU_ASSERT_EQUAL(ellipse_mat(&e0, &M), 0);
      CU_ASSERT_EQUAL(mat_ellipse(&M, &e1), 0);
      CU_ASSERT_DOUBLE_EQUAL(e1.major, e0.major, eps);
      CU_ASSERT_DOUBLE_EQUAL(e1.minor, e0.minor, eps);

      /*
        we identify angles which differ by an integer
        multiple of pi, so check that the sin of the
        difference is zero
      */

      CU_ASSERT_DOUBLE_EQUAL(sin(e0.theta - e1.theta), 0.0, eps);
    }

  {
    ellipse_t e2 = {2, 2, 0, {1, 3}}, e3;
    mat_t M;

    CU_ASSERT_EQUAL(ellipse_mat(&e2, &M), 0);
    CU_ASSERT_EQUAL(mat_ellipse(&M, &e3), 0);
    CU_ASSERT_DOUBLE_EQUAL(e3.major, e2.major, eps);
    CU_ASSERT_DOUBLE_EQUAL(e3.minor, e2.minor, eps);
  }
}

/*
  ellipse in [0, 2] x [-2, 2], tested at angles 0 and 90
  degrees
*/

void test_ellipse_tangent_points(void)
{
  int type;
  ellipse_t e = {2, 1, M_PI / 2, {1, 0}};
  vec_t v[2], a, b;

  /* horizontal tangents, a is the higher point */

  CU_ASSERT_EQUAL(ellipse_tangent_points(&e, 0, v), 0);

  type = v[0].y < v[1].y;
  a = v[type];
  b = v[!type];

  CU_ASSERT_DOUBLE_EQUAL(a.x, 1.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(a.y, 2.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(b.x, 1.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(b.y, -2.0, eps);

  CU_ASSERT(ellipse_tangent_points(&e, M_PI / 2, v) == 0);

  /* vertical tangents, s is the righter point */

  type = v[0].x < v[1].x;
  a = v[type];
  b = v[!type];

  CU_ASSERT_DOUBLE_EQUAL(a.x, 2.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(a.y, 0.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(b.x, 0.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(b.y, 0.0, eps);
}

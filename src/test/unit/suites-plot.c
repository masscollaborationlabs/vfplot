#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <CUnit/CUnit.h>

#include "tests-arrow.h"
#include "tests-aspect.h"
#include "tests-bbox.h"
#include "tests-bilinear.h"
#include "tests-contain.h"
#include "tests-csv-read.h"
#include "tests-csv-write.h"
#include "tests-curvature.h"
#include "tests-domain.h"
#include "tests-domain-geojson.h"
#include "tests-margin.h"
#include "tests-ellipse-mt.h"
#include "tests-polygon.h"
#include "tests-potential.h"
#include "tests-units.h"

#include "cunit-compat.h"

#define ENTRY(a, b) CU_Suite_Entry(a, NULL, NULL, (b))

static CU_SuiteInfo suites[] =
  {
    ENTRY("arrows", tests_arrow),
    ENTRY("aspect ratio", tests_aspect),
    ENTRY("bounding boxes", tests_bbox),
    ENTRY("bilinear", tests_bilinear),
    ENTRY("CSV read", tests_csv_read),
    ENTRY("CSV write", tests_csv_write),
    ENTRY("contain", tests_contain),
    ENTRY("curvature", tests_curvature),
    ENTRY("domain", tests_domain),
    ENTRY("domain-geojson", tests_domain_geojson),
    ENTRY("ellipse-mt", tests_ellipse_mt),
    ENTRY("margin", tests_margin),
    ENTRY("polygon", tests_polygon),
    ENTRY("potential", tests_potential),
    ENTRY("units", tests_units),
    CU_SUITE_INFO_NULL,
  };

void suites_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr,
              "plot suite registration failed - %s\n",
	      CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_ellipse_mt[];

void test_ellipse_mt_construct(void);
void test_ellipse_mt_nxy(void);
void test_ellipse_mt_bbox(void);

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>

#include <geom2d/contact.h>
#include <geom2d/ellipse.h>

#include "tests-contact.h"

CU_TestInfo tests_contact[] =
  {
    {"evaluate", test_contact_evaluate},
    {"intersect", test_contact_intersect},
    CU_TEST_INFO_NULL,
  };

void test_contact_evaluate(void)
{
  double eps = 1e-10;

  ellipse_t A, B;

  A.centre.x = 0.0;
  A.centre.y = 0.0;
  A.major = 1.0;
  A.minor = 1.0;
  A.theta = 0.0;

  B.centre.x = 2.0;
  B.centre.y = 0.0;
  B.major = 1.0;
  B.minor = 1.0;
  B.theta = 0.0;

  double z;

  CU_ASSERT_EQUAL(contact(&A, &B, &z, NULL), 0);
  CU_ASSERT_DOUBLE_EQUAL(z, 1.0, eps);
}

/*
  the point of the contact function is that it is 1.0 when
  the ellipes touch, so allowing nice geometric tests
*/

static bool ellipse_intersect(const ellipse_t *e1, const ellipse_t *e2)
{
  double d;
  contact(e1, e2, &d, NULL);
  return d <= 1;
}

void test_contact_intersect(void)
{
  double abit = 1e-6;

  /* intersecting */

  double W = sqrt(2) - abit;

  ellipse_t e[3] = {
    {2, 1, -M_PI/4, {0, 0}},
    {3, 1, -M_PI/4, {W, -W}},
    {4, 1, -M_PI/4, {2*W, -2*W}}
  };

  CU_ASSERT_TRUE(ellipse_intersect(&e[0], &e[1]));
  CU_ASSERT_TRUE(ellipse_intersect(&e[1], &e[0]));
  CU_ASSERT_TRUE(ellipse_intersect(&e[1], &e[2]));
  CU_ASSERT_TRUE(ellipse_intersect(&e[2], &e[1]));

  /* disjoint isotropic */

  ellipse_t f[3] = {
    {2, 1, 0, {0, -1}},
    {2, 1, 0, {0, 1 + abit}},
    {2, 1, 0, {4 + abit, -1}}
  };

  CU_ASSERT_FALSE(ellipse_intersect(&f[0], &f[1]));
  CU_ASSERT_FALSE(ellipse_intersect(&f[0], &f[2]));

  CU_ASSERT_FALSE(ellipse_intersect(&f[1], &f[0]));
  CU_ASSERT_FALSE(ellipse_intersect(&f[1], &f[2]));

  CU_ASSERT_FALSE(ellipse_intersect(&f[2], &f[0]));
  CU_ASSERT_FALSE(ellipse_intersect(&f[2], &f[1]));

  /* disjoint anisotropic */

  ellipse_t g[3] = {
    {2, 1, 0, {0, 0}},
    {2, 1, M_PI/4, {0, 4 + abit}},
    {2, 1, M_PI/2, {4 + abit, 0}}
  };

  CU_ASSERT_FALSE(ellipse_intersect(&g[0], &g[1]));
  CU_ASSERT_FALSE(ellipse_intersect(&g[0], &g[2]));

  CU_ASSERT_FALSE(ellipse_intersect(&g[1], &g[0]));
  CU_ASSERT_FALSE(ellipse_intersect(&g[1], &g[2]));

  CU_ASSERT_FALSE(ellipse_intersect(&g[2], &g[0]));
  CU_ASSERT_FALSE(ellipse_intersect(&g[2], &g[1]));

  /* this tripped a degeneracy bug */

  ellipse_t h[2] = {
    {2, 1, 0, {0, -1}},
    {3, 1, 0, {0, 1 + abit}}
  };

  CU_ASSERT_FALSE(ellipse_intersect(&h[0], &h[1]));
  CU_ASSERT_FALSE(ellipse_intersect(&h[1], &h[0]));

  /* predicted bad case */

  ellipse_t k[2] = {
    {1.99, 1, M_PI / 24, {0, 0}},
    {2.01, 1, -M_PI / 24, {5, 0}}
  };

  CU_ASSERT_FALSE(ellipse_intersect(&k[0], &k[1]));
  CU_ASSERT_FALSE(ellipse_intersect(&k[1], &k[0]));
}

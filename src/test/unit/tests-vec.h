#include <CUnit/CUnit.h>

extern CU_TestInfo tests_vector[];

void test_vsub(void);
void test_vadd(void);
void test_vscale(void);
void test_vabs(void);
void test_vabs2(void);
void test_vang(void);
void test_vdet(void);
void test_vdot(void);
void test_vxtang(void);
void test_vunit_nonzero(void);
void test_vunit_zero(void);
void test_visect(void);
void test_bend(void);

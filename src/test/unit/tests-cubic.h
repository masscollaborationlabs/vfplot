#include <CUnit/CUnit.h>

extern CU_TestInfo tests_cubic[];

void test_cubic_roots_1(void);
void test_cubic_roots_2(void);
void test_cubic_roots_3(void);
void test_cubic_roots_4(void);
void test_cubic_roots_5(void);
void test_cubic_roots_6(void);
void test_cubic_roots_7(void);
void test_cubic_roots_8(void);

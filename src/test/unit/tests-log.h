#include <CUnit/CUnit.h>

extern CU_TestInfo tests_log[];

extern void test_log_plain(void);
extern void test_log_decorated(void);
extern void test_log_level_of_string(void);

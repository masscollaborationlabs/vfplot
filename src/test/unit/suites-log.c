#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <CUnit/CUnit.h>

#include "tests-log.h"

#include "cunit-compat.h"

#define ENTRY(a, b) CU_Suite_Entry(a, NULL, NULL, (b))

static CU_SuiteInfo suites[] =
  {
    ENTRY("log", tests_log),
    CU_SUITE_INFO_NULL,
  };

void suites_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr,
              "log suite registration failed - %s\n",
	      CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}

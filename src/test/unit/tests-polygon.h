#include <CUnit/CUnit.h>

extern CU_TestInfo tests_polygon[];

void test_polygon_init(void);
void test_polygon_clear(void);
void test_polygon_clone(void);
void test_polygon_ngon(void);
void test_polygon_rect(void);
void test_polygon_reverse(void);
void test_polygon_inside(void);
void test_polygon_contains_ngon(void);
void test_polygon_contains_nonconvex(void);
void test_polygon_wind(void);
void test_polygon_area_degenerate0(void);
void test_polygon_area_degenerate1(void);
void test_polygon_area_degenerate2(void);
void test_polygon_area_square_clock(void);
void test_polygon_area_square_anticlock(void);
void test_polygon_area_square_rotated(void);
void test_polygon_area_rectangle(void);
void test_polygon_area_nonconvex(void);

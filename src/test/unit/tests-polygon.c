#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>

#include <plot/polygon.h>

#include "assert-bbox.h"
#include "assert-polygon.h"
#include "tests-polygon.h"

CU_TestInfo tests_polygon[] =
  {
    {"init", test_polygon_init},
    {"clear", test_polygon_clear},
    {"clone", test_polygon_clone},
    {"n-gon", test_polygon_ngon},
    {"rectangle", test_polygon_rect},
    {"reverse", test_polygon_reverse},
    {"inside", test_polygon_inside},
    {"contains: n-gon", test_polygon_contains_ngon},
    {"contains: nonconvex", test_polygon_contains_nonconvex},
    {"winding number", test_polygon_wind},
    {"area, 0-point", test_polygon_area_degenerate0},
    {"area, 1-point", test_polygon_area_degenerate1},
    {"area, 2-point", test_polygon_area_degenerate2},
    {"area, square clockwise", test_polygon_area_square_clock},
    {"area, square anticlockwise", test_polygon_area_square_anticlock},
    {"area, square rotated", test_polygon_area_square_rotated},
    {"area, rectangle", test_polygon_area_rectangle},
    {"area, nonconvex", test_polygon_area_nonconvex},
    CU_TEST_INFO_NULL
  };

void test_polygon_init(void)
{
  polygon_t p = { .n = 0, .v = NULL };

  CU_ASSERT_EQUAL_FATAL(polygon_init(5, &p), 0);
  CU_ASSERT_EQUAL(p.n, 5);
  CU_ASSERT_PTR_NOT_NULL(p.v);
}

void test_polygon_clear(void)
{
  polygon_t p = { .n = 0, .v = NULL };

  CU_ASSERT_EQUAL_FATAL(polygon_init(5, &p), 0);
  CU_ASSERT_EQUAL_FATAL(p.n, 5);
  CU_ASSERT_PTR_NOT_NULL_FATAL(p.v);
  polygon_clear(&p);
  CU_ASSERT_EQUAL(p.n, 0);
  CU_ASSERT_PTR_NULL(p.v);
}

void test_polygon_clone(void)
{
  vec_t v[1] = { {1, 2} };
  polygon_t p = { .n = 1, .v = v }, q;

  CU_ASSERT_EQUAL_FATAL(polygon_clone(&p, &q), 0);
  CU_ASSERT_POLYGON_EQUAL(p, q, 1e-10);
  polygon_clear(&q);
}

void test_polygon_ngon(void)
{
  size_t n = 13;
  vec_t v = {3, 4};
  polygon_t p;
  double r = 2.5;

  CU_ASSERT_EQUAL_FATAL(polygon_ngon(r, v, n, &p), 0);
  CU_ASSERT_EQUAL_FATAL(p.n, n);
  CU_ASSERT_PTR_NOT_NULL_FATAL(p.v);

  for (size_t i = 0 ; i < n ; i++)
    CU_ASSERT_DOUBLE_EQUAL(vabs(vsub(p.v[i], v)), r, 1e-10);

  polygon_clear(&p);
}

void test_polygon_rect(void)
{
  bbox_t b0 = {{1, 2}, {2, 3}};
  polygon_t p;

  CU_ASSERT_EQUAL_FATAL(polygon_rect(b0, &p), 0);

  bbox_t b1 = polygon_bbox(&p);

  CU_ASSERT_BBOX_EQUAL(b0, b1, 1e-10);
  polygon_clear(&p);
}

void test_polygon_reverse(void)
{
  vec_t v = {3, 4};
  polygon_t p0, p1;

  CU_ASSERT_EQUAL_FATAL(polygon_ngon(5, v, 10, &p0), 0);
  CU_ASSERT_EQUAL_FATAL(polygon_clone(&p0, &p1), 0);
  CU_ASSERT_EQUAL_FATAL(polygon_reverse(&p0), 0);
  CU_ASSERT_EQUAL_FATAL(polygon_reverse(&p0), 0);

  CU_ASSERT_POLYGON_EQUAL(p0, p1, 1e-10);

  polygon_clear(&p0);
  polygon_clear(&p1);
}

void test_polygon_inside(void)
{
  bbox_t b0 = {{1, 2}, {2, 3}};
  polygon_t p;

  CU_ASSERT_EQUAL_FATAL(polygon_rect(b0, &p), 0);

  vec_t vos[4] = {
    {1.50, 1.99},
    {1.50, 3.01},
    {0.99, 2.50},
    {2.01, 2.50}
  };

  for (int i = 0 ; i < 4 ; i++)
    CU_ASSERT_FALSE(polygon_inside(&p, vos[i]));

  vec_t vis[4] = {
    {1.50, 2.01},
    {1.50, 2.99},
    {1.01, 2.50},
    {1.99, 2.50}
  };

  for (int i = 0 ; i < 4 ; i++)
    CU_ASSERT_TRUE(polygon_inside(&p, vis[i]));

  polygon_clear(&p);
}

void test_polygon_contains_nonconvex(void)
{
  vec_t v[8] = {
    { 1,  1},
    { 2,  1},
    { 2, -1},
    {-2, -1},
    {-2,  1},
    {-1,  1},
    {-1,  0},
    { 1,  0}
  };
  polygon_t pv = { .n = 8, .v = v };
  vec_t u[4] = {
    { 1.5,  0.5},
    { 1.5, -0.5},
    {-1.5, -0.5},
    {-1.5,  0.5}
  };
  polygon_t pu = { .n = 4, .v = u };

  CU_ASSERT_FALSE(polygon_contains(&pu, &pv));
}

void test_polygon_contains_ngon(void)
{
  vec_t v = {3, 2};
  polygon_t p, q;

  CU_ASSERT_EQUAL_FATAL(polygon_ngon(3, v, 7, &p), 0);
  CU_ASSERT_EQUAL_FATAL(polygon_ngon(2, v, 7, &q), 0);

  CU_ASSERT_FALSE(polygon_contains(&p, &q));
  CU_ASSERT_TRUE(polygon_contains(&q, &p));
}

void test_polygon_wind(void)
{
  vec_t v = {3, 2};
  polygon_t p;

  for (int n = 4 ; n < 10 ; n++)
    {
      CU_ASSERT_EQUAL_FATAL(polygon_ngon(3, v, n, &p), 0);
      CU_ASSERT_EQUAL(polygon_wind(&p), 1);
      polygon_reverse(&p);
      CU_ASSERT_EQUAL(polygon_wind(&p), -1);
      polygon_clear(&p);
    }
}

void test_polygon_area_degenerate0(void)
{
  polygon_t p = { .n = 0, .v = NULL };
  double area = polygon_area(&p);
  CU_ASSERT_DOUBLE_EQUAL(area, 0, 1e-15);
}

void test_polygon_area_degenerate1(void)
{
  vec_t v[] = { {0, 0} };
  size_t n = sizeof(v) / sizeof(vec_t);
  polygon_t p = { .n = n, .v = v };
  double area = polygon_area(&p);
  CU_ASSERT_DOUBLE_EQUAL(area, 0, 1e-15);
}

void test_polygon_area_degenerate2(void)
{
  vec_t v[] = { {0, 0}, {0, 1} };
  size_t n = sizeof(v) / sizeof(vec_t);
  polygon_t p = { .n = n, .v = v };
  double area = polygon_area(&p);
  CU_ASSERT_DOUBLE_EQUAL(area, 0, 1e-15);
}

void test_polygon_area_square_clock(void)
{
  vec_t v[] = { {0, 0}, {0, 1}, {1, 1}, {1, 0} };
  size_t n = sizeof(v) / sizeof(vec_t);
  polygon_t p = { .n = n, .v = v };
  double area = polygon_area(&p);
  CU_ASSERT_DOUBLE_EQUAL(area, 1, 1e-15);
}

void test_polygon_area_square_anticlock(void)
{
  vec_t v[] = { {0, 0}, {1, 0}, {1, 1}, {0, 1} };
  size_t n = sizeof(v) / sizeof(vec_t);
  polygon_t p = { .n = n, .v = v };
  double area = polygon_area(&p);
  CU_ASSERT_DOUBLE_EQUAL(area, 1, 1e-15);
}

void test_polygon_area_square_rotated(void)
{
  vec_t v[] = { {1, 0}, {1, 1}, {0, 1}, {0, 0} };
  size_t n = sizeof(v) / sizeof(vec_t);
  polygon_t p = { .n = n, .v = v };
  double area = polygon_area(&p);
  CU_ASSERT_DOUBLE_EQUAL(area, 1, 1e-15);
}

void test_polygon_area_rectangle(void)
{
  vec_t v[] = { {0, 0}, {0, 2}, {1, 2}, {1, 0} };
  size_t n = sizeof(v) / sizeof(vec_t);
  polygon_t p = { .n = n, .v = v };
  double area = polygon_area(&p);
  CU_ASSERT_DOUBLE_EQUAL(area, 2, 1e-15);
}

void test_polygon_area_nonconvex(void)
{
  vec_t v[] =
    {
     {0, 0},
     {3, 0},
     {3, 2},
     {2, 2},
     {2, 1},
     {1, 1},
     {1, 2},
     {0, 2}
    };
  size_t n = sizeof(v) / sizeof(vec_t);
  polygon_t p = { .n = n, .v = v };
  double area = polygon_area(&p);
  CU_ASSERT_DOUBLE_EQUAL(area, 5, 1e-15);
}

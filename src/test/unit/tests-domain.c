#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>

#include <plot/domain.h>

#include "assert-bbox.h"
#include "tests-domain.h"
#include "factory-domain.h"

CU_TestInfo tests_domain[] =
  {
    {"new", test_domain_new},
    {"bbox", test_domain_bbox},
    {"coerce orientation", test_domain_orientate},
    {"inside", test_domain_inside},
    {"scale", test_domain_scale},
    {"each", test_domain_each},
    {"insert", test_domain_insert},
    {"clone", test_domain_clone},
    {"area, square", test_domain_area_square},
    {"area, simple", test_domain_area_simple},
    {"depth, empty", test_domain_depth_empty},
    {"depth, square", test_domain_depth_square},
    {"depth, simple", test_domain_depth_simple},
    {"width, empty", test_domain_width_empty},
    {"width, square", test_domain_width_square},
    {"width, simple", test_domain_width_simple},
    CU_TEST_INFO_NULL
  };

void test_domain_new(void)
{
  domain_t *dom = domain_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  domain_destroy(dom);
}

void test_domain_bbox(void)
{
  domain_t *dom = factory_domain_simple();
  bbox_t bbox0, bbox1 = {{0, 60}, {0, 60}};
  CU_ASSERT_EQUAL_FATAL(domain_bbox(dom, &bbox0), 0);
  CU_ASSERT_BBOX_EQUAL(bbox0, bbox1, 1e-10);
  domain_destroy(dom);
}

void test_domain_orientate(void)
{
  domain_t *dom = factory_domain_simple();
  CU_ASSERT_EQUAL(domain_orientate(dom), 0);
  domain_destroy(dom);
}

static void check_domain_inside(double x, double y,
				const domain_t *dom, bool expected)
{
  vec_t vec = {x, y};
  CU_ASSERT_EQUAL(domain_inside(dom, vec), expected);
}

void test_domain_inside(void)
{
  domain_t *dom = factory_domain_simple();

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  /* at the edges */

  check_domain_inside(30, 61, dom, false);
  check_domain_inside(30, 59, dom, true);
  check_domain_inside(30,  1, dom, true);
  check_domain_inside(30, -1, dom, false);

  check_domain_inside(61, 30, dom, false);
  check_domain_inside(59, 30, dom, true);
  check_domain_inside( 1, 30, dom, true);
  check_domain_inside(-1, 30, dom, false);

  /* in a hole */

  check_domain_inside(15, 15, dom, false);

  /* in an island in a hole */

  check_domain_inside(40, 40, dom, true);

  domain_destroy(dom);
}

static void check_domain_scale_shift(double x, double y, const domain_t *dom_orig)
{
  double eps = 1e-10;
  domain_t *dom = domain_clone(dom_orig);

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  bbox_t b0;

  CU_ASSERT_EQUAL(domain_bbox(dom, &b0), 0);
  CU_ASSERT_EQUAL(domain_scale(dom, 1.0, x, y), 0);

  bbox_t b1;

  CU_ASSERT_EQUAL(domain_bbox(dom, &b1), 0);
  CU_ASSERT_DOUBLE_EQUAL(b0.x.max - x, b1.x.max, eps);
  CU_ASSERT_DOUBLE_EQUAL(b0.x.min - x, b1.x.min, eps);
  CU_ASSERT_DOUBLE_EQUAL(b0.y.max - y, b1.y.max, eps);
  CU_ASSERT_DOUBLE_EQUAL(b0.y.min - y, b1.y.min, eps);
}

static void check_domain_scale_M(double M, const domain_t *dom_orig)
{
  double eps = 1e-10;
  domain_t *dom = domain_clone(dom_orig);

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  bbox_t b0;

  CU_ASSERT_EQUAL(domain_bbox(dom, &b0), 0);
  CU_ASSERT_EQUAL(domain_scale(dom, M, 0, 0), 0);

  bbox_t b1;

  CU_ASSERT_EQUAL(domain_bbox(dom, &b1), 0);
  CU_ASSERT_DOUBLE_EQUAL(fabs(M)*(b0.x.max - b0.x.min),
			 b1.x.max - b1.x.min, eps);
  CU_ASSERT_DOUBLE_EQUAL(fabs(M)*(b0.y.max - b0.y.min),
			 b1.y.max - b1.y.min, eps);
}

void test_domain_scale(void)
{
  domain_t *dom = factory_domain_simple();

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  check_domain_scale_shift( 0, 0, dom);
  check_domain_scale_shift( 2, 2, dom);
  check_domain_scale_shift(-2, 2, dom);

  check_domain_scale_M(1, dom);
  check_domain_scale_M(3, dom);
  check_domain_scale_M(0, dom);
  check_domain_scale_M(-1, dom);

  domain_destroy(dom);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int level_iter(polygon_t *p, void *arg, int level)
{
  int *level_max = arg;

  if (*level_max < level)
    *level_max = level;

  return 0;
}

#pragma GCC diagnostic pop

static void check_domain_each_context(const domain_t *dom)
{
  int i = 0;
  CU_ASSERT_EQUAL(domain_each((domain_t*)dom, level_iter, &i), 0);
  CU_ASSERT_EQUAL(i, 3);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int sc_iter(polygon_t *p, void *arg, int level)
{
  int *count = arg;
  (*count)++;
  return 123;
}

#pragma GCC diagnostic pop

static void check_domain_each_short_circuit(const domain_t *dom)
{
  int i = 0;
  CU_ASSERT_EQUAL(domain_each((domain_t*)dom, sc_iter, &i), 123);
  CU_ASSERT_EQUAL(i, 1);
}

void test_domain_each(void)
{
  domain_t *dom = factory_domain_simple();

  check_domain_each_context(dom);
  check_domain_each_short_circuit(dom);

  domain_destroy(dom);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int count_iter(polygon_t *p, void *arg, int level)
{
  int *count = arg;
  (*count)++;
  return 0;
}

#pragma GCC diagnostic pop

static int count_nodes(domain_t *dom)
{
  int count = 0;
  CU_ASSERT_EQUAL(domain_each((domain_t*)dom, count_iter, &count), 0);
  return count;
}

void test_domain_insert(void)
{
  domain_t *dom = domain_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  int m = 10;

  for (int n = m ; n > 0 ; n--)
    {
      polygon_t p;
      bbox_t bbox = {{-n, n}, {-n, n}};

      CU_ASSERT_EQUAL(polygon_rect(bbox, &p), 0);
      CU_ASSERT_EQUAL(domain_insert(dom, &p), 0);

      polygon_clear(&p);
    }

  CU_ASSERT_EQUAL(count_nodes(dom), m);

  domain_destroy(dom);
}

void test_domain_clone(void)
{
  domain_t *dom = factory_domain_simple();
  domain_t *clone = domain_clone(dom);
  CU_ASSERT_PTR_NOT_NULL_FATAL(clone);

  CU_ASSERT_EQUAL(count_nodes(dom), count_nodes(clone));

  domain_destroy(dom);
  domain_destroy(clone);
}

void test_domain_area_square(void)
{
  domain_t *dom = factory_domain_square();
  double area = domain_area(dom);
  CU_ASSERT_DOUBLE_EQUAL(area, 3600, 1e-15);
  domain_destroy(dom);
}

void test_domain_area_simple(void)
{
  domain_t *dom = factory_domain_simple();
  double area = domain_area(dom);
  CU_ASSERT_DOUBLE_EQUAL(area, 3600 - 100 - 400 + 36, 1e-15);
  domain_destroy(dom);
}

void test_domain_depth_empty(void)
{
  domain_t *dom = domain_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  CU_ASSERT_EQUAL(domain_depth(dom), 0);
  domain_destroy(dom);
}

void test_domain_depth_simple(void)
{
  domain_t *dom = factory_domain_simple();
  CU_ASSERT_EQUAL(domain_depth(dom), 3);
  domain_destroy(dom);
}

void test_domain_depth_square(void)
{
  domain_t *dom = factory_domain_square();
  CU_ASSERT_EQUAL(domain_depth(dom), 1);
  domain_destroy(dom);
}

void test_domain_width_empty(void)
{
  domain_t *dom = domain_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  CU_ASSERT_EQUAL(domain_width(dom), 0);
  domain_destroy(dom);
}

void test_domain_width_simple(void)
{
  domain_t *dom = factory_domain_simple();
  CU_ASSERT_EQUAL(domain_width(dom), 1);
  domain_destroy(dom);
}

void test_domain_width_square(void)
{
  domain_t *dom = factory_domain_square();
  CU_ASSERT_EQUAL(domain_width(dom), 1);
  domain_destroy(dom);
}

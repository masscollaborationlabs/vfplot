#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <geom2d/sdcontact.h>

#include "tests-sdcontact.h"
#include "assert-vec.h"


CU_TestInfo tests_sdcontact[] =
  {
    {"circle-radial", test_sdcontact_circle_radial},
    {"circle-tangent (middle)", test_sdcontact_circle_tangent_mid},
    {"circle-tangent (end)", test_sdcontact_circle_tangent_end},
    {"circle-tangent (extended)", test_sdcontact_circle_tangent_ext},
    {"circle-skew", test_sdcontact_circle_skew_end},
    {"rotation invariance", test_sdcontact_rotation_invariance},
    {"non-negativity on grid", test_sdcontact_non_negativity},
    {"non-negativity regression 1", test_sdcontact_non_negativity_regress_1},
    {"non-negativity regression 2", test_sdcontact_non_negativity_regress_2},
    {"zero-division regression", test_sdcontact_div_zero_regress},
    CU_TEST_INFO_NULL,
  };

/*
  A circle at the origin and a line on the x-axis (so touching the
  circle radially)
*/

void test_sdcontact_circle_radial(void)
{
  double eps = 1e-10;
  ellipse_t A = {
    .centre = {
      .x = 2.0,
      .y = 0.0
    },
    .major = 1.0,
    .minor = 0.0,
    .theta = 0.0
  };
  ellipse_t B = {
    .centre = {
      .x = 0.0,
      .y = 0.0
    },
    .major = 1.0,
    .minor = 1.0,
    .theta = 0.0
  };

  double z;
  vec_t v, vexp = {-1, 0};

  CU_ASSERT_EQUAL(sdcontact(&A, &B, &z, &v), 0);
  CU_ASSERT_DOUBLE_EQUAL(z, 1.0, eps);
  CU_ASSERT_VEC_EQUAL(v, vexp, eps);
}


/*
  a line tangent to a circle, with its middle touching it
*/

void test_sdcontact_circle_tangent_mid(void)
{
  double eps = 1e-10;
  ellipse_t A = {
    .centre = {
      .x = 0.0,
      .y = 1.0
    },
    .major = 1.0,
    .minor = 0.0,
    .theta = 0.0
  };
  ellipse_t B = {
    .centre = {
      .x = 0.0,
      .y = 0.0
    },
    .major = 1.0,
    .minor = 1.0,
    .theta = 0.0
  };

  double z;
  vec_t v, vexp = {0, -1};

  CU_ASSERT_EQUAL(sdcontact(&A, &B, &z, &v), 0);
  CU_ASSERT_DOUBLE_EQUAL(z, 1.0, eps);
  CU_ASSERT_VEC_EQUAL(v, vexp, eps);
}


/*
  a line tangent to a circle, with its end touching it; this case has
  the derivative of fAB zero at zero, so at an algorithmic crossover
*/

void test_sdcontact_circle_tangent_end(void)
{
  double eps = 1e-10;
  ellipse_t A = {
    .centre = {
      .x = 1.0,
      .y = 1.0
    },
    .major = 1.0,
    .minor = 0.0,
    .theta = 0.0
  };
  ellipse_t B = {
    .centre = {
      .x = 0.0,
      .y = 0.0
    },
    .major = 1.0,
    .minor = 1.0,
    .theta = 0.0
  };

  double z;
  vec_t v, vexp = {0, -1};

  CU_ASSERT_EQUAL(sdcontact(&A, &B, &z, &v), 0);
  CU_ASSERT_DOUBLE_EQUAL(z, 1.0, eps);
  CU_ASSERT_VEC_EQUAL(v, vexp, eps);
}


/*
  a circle with a line whose extension is tangent to it, but
  does not actually touch it (so contact value is > 1)
*/

void test_sdcontact_circle_tangent_ext(void)
{
  ellipse_t A = {
    .centre = {
      .x = 1.1,
      .y = 1.0
    },
    .major = 1.0,
    .minor = 0.0,
    .theta = 0.0
  };
  ellipse_t B = {
    .centre = {
      .x = 0.0,
      .y = 0.0
    },
    .major = 1.0,
    .minor = 1.0,
    .theta = 0.0
  };

  double z;

  CU_ASSERT_EQUAL(sdcontact(&A, &B, &z, NULL), 0);
  CU_ASSERT_TRUE( z > 1.0 );
}


/*
  half-way between radial and tangent
*/

void test_sdcontact_circle_skew_end(void)
{
  double eps = 1e-10, t = 1/sqrt(2);
  ellipse_t A = {
    .centre = {
      .x = 1 + t,
      .y = t
    },
    .major = 1.0,
    .minor = 0.0,
    .theta = 0.0
  };
  ellipse_t B = {
    .centre = {
      .x = 0.0,
      .y = 0.0
    },
    .major = 1.0,
    .minor = 1.0,
    .theta = 0.0
  };

  double z;

  CU_ASSERT_EQUAL(sdcontact(&A, &B, &z, NULL), 0);
  CU_ASSERT_DOUBLE_EQUAL(z, 1.0, eps);
}


/*
  the contact function is invariant to rotations of the plane
*/

static double contact_rotated(double theta)
{
  ellipse_t A = {
    .centre = {
      .x = cos(theta),
      .y = sin(theta)
    },
    .major = 1.0,
    .minor = 0.0,
    .theta = M_PI/5 + theta
  };
  ellipse_t B = {
    .centre = {
      .x = 0.0,
      .y = 0.0
    },
    .major = 1.0,
    .minor = 2.0,
    .theta = theta
  };

  double z;

  sdcontact(&A, &B, &z, NULL);

  return z;
}

void test_sdcontact_rotation_invariance(void)
{
  int n = 60;
  double
    eps = 1e-10,
    cr0 = contact_rotated(0);

  for (int i = 1 ; i < n ; i++)
    {
      double theta = 2 * i * M_PI / n;

      CU_ASSERT_DOUBLE_EQUAL(contact_rotated(theta), cr0, eps);
    }
}


/*
  the contact function is non-negative; we check on a coarse grid
  to have a first look for problems.
*/

void test_sdcontact_non_negativity(void)
{
  ellipse_t A = {
    .major = 1.0,
    .minor = 0.0,
    .theta = M_PI/5
  };
  ellipse_t B = {
    .centre = {
      .x = 0.0,
      .y = 0.0
    },
    .major = 1.0,
    .minor = 2.0,
    .theta = M_PI/4
  };

  int n = 30;

  for (int i = 0 ; i <= n ; i++)
    {
      A.centre.x = -3 + i * 6.0 / n;

      for (int j = 0 ; j <= n ; j++)
	{
	  A.centre.y = -3 + j * 6.0 / n;

          double z;

          CU_ASSERT_EQUAL(sdcontact(&A, &B, &z, NULL), 0);
	  CU_ASSERT_TRUE( z >= 0.0 );
	}
    }
}

/*
  Some error cases from the above investigations, converted
  to regression tests.  These all seem to have the maximum of
  fAB(t) to be maximised at small t, presumably as we approach
  degeneracy we should expect that.  The quagratic Taylor
  approximation seems to fix these.
*/

static void error_case(double x, double y)
{
  ellipse_t A = {
    .centre = {
      .x = x,
      .y = y
    },
    .major = 1.0,
    .minor = 0.0,
    .theta = M_PI / 5
  };
  ellipse_t B = {
    .centre = {
      .x = 0.0,
      .y = 0.0
    },
    .major = 1.0,
    .minor = 2.0,
    .theta = M_PI / 4
  };

  double z;

  CU_ASSERT_EQUAL(sdcontact(&A, &B, &z, NULL), 0);
  CU_ASSERT_TRUE( z >= 0.0 );
}

void test_sdcontact_non_negativity_regress_1(void)
{
  error_case(-0.250000, 0.942000);
  error_case(0.250000, -0.942000);
  error_case(0.500000, -1.884000);
}


/*
  This came up in testing contain.c at high resolution, so
  in terms of the sdcontact_mt() variant -- I guess this is
  the same as the above, annoying, I though the p(1) = 0
  condition had fixed that ...

  Ooh, in fact this might be a bit weirder than first thought,
  we have f(0) = 0.14, f'(0) = 0.025 but f is pretty-much
  linear for 0 < t < 0.9, then f dives down to zero as t -> 1.
  The Newton iteration actally works here, we just need to
  up the maximum iterations to 25 to get a pass, it starts
  at 0.07, the quadratic estimate for the maximisung t, then
  take 5 iterations to get to 0.9.  The key point is how
  awful the quadratic is as approximant here, using it
  would give a contact of 0.14, but the actual value is 0.17

  We get A is (5.16398 1.29099) at angle 0.0, B has eigenvalues
  (0, 10000). The vector v is odd, since the B ellipse is from
  the square.dom fixture, it is a 60 unit line ??
*/

void test_sdcontact_non_negativity_regress_2(void)
{
  vec_t v = { -1.973244, 41.387960 }, w;
  mat_t
    A = {26.666667, 0.0, 0.0, 1.666667},
    B = {0.0, 0.0, 0.0, 10000.0};
  double d;

  CU_ASSERT_EQUAL(sdcontact_mt(v, B, A, &d, &w), 0);
}

/*
  This came from a floating point exception in the "house" integration
  test:

  *** Process received signal ***
  Signal: Floating point exception (8)
  Signal code: Floating point divide-by-zero (3)
  Failing at address: 0x55f75beb30ef
  [ 0] /lib/x86_64-linux-gnu/libpthread.so.0(+0x153c0)[0x7f246e6a33c0]
  [ 1] ./vfplot(vunit+0x2f)[0x55f75beb30ef]
  [ 2] ./vfplot(sdcontact_mt+0x3fa)[0x55f75bec2efa]
  :

  We extract the same values passed to the call to sdcontact_mt
  and use them here.  Clearly the problem is that we are passing
  (0, 0) to vunit() which is undefined -- in the implementation
  we have vscale(1 / vabs(v), v), and the 1 / vabs(v) would give the
  division-by-zero exception, but oddly, in this test we get NANs
  for the components; different optimisation levels perhaps?

  Note that it is to be expected that we get a zero vector here
  occasionally, this will happen at a point exactly on a boundary
  segment, and we calulate the metric tensor on a grid, so on
  rectangular domains this could happen quite a bit.

  In any case, we modify vunit() to handle the zero argument case in
  returning zero, that is asserted here
*/

void test_sdcontact_div_zero_regress(void)
{
  mat_t
    A = {1.952701e-03, -6.149016e-08, -6.149016e-08, 9.554796e-04},
    B = {1.525879e-05, 0, 0, 0};
  vec_t rAB = {-4.796063e-02, 0}, u;
  double d;

  CU_ASSERT_EQUAL(sdcontact_mt(rAB, B, A, &d, &u), 0);
  CU_ASSERT_EQUAL(u.x, 0);
  CU_ASSERT_EQUAL(u.y, 0);
}

/*
  assert_polygon.c
  J.J. Green 2015
*/

#include <CUnit/CUnit.h>
#include "assert-polygon.h"
#include "assert-vec.h"

void assert_polygon_equal(polygon_t p, polygon_t q, double eps)
{
  CU_ASSERT_EQUAL_FATAL(p.n, q.n);
  for (size_t i = 0 ; i < p.n ; i++)
    CU_ASSERT_VEC_EQUAL(p.v[i], q.v[i], eps);
}

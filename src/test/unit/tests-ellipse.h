#include <CUnit/CUnit.h>

extern CU_TestInfo tests_ellipse[];

void test_ellipse_tangent_points(void);
void test_ellipse_intersect(void);
void test_ellipse_mat(void);
void test_mat_ellipse(void);

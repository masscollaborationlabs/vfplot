/*
  assert_vect.c
  J.J. Green 2015
*/

#include <CUnit/CUnit.h>
#include "assert-vec.h"

void assert_vec_equal(vec_t u, vec_t v, double eps)
{
  CU_ASSERT_DOUBLE_EQUAL(u.x, v.x, eps);
  CU_ASSERT_DOUBLE_EQUAL(u.y, v.y, eps);
}

/*
  assert_vec.h
  Copyright (c) J.J. Green 2015
*/

#ifndef ASSERT_VECTOR_H
#define ASSERT_VECTOR_H

#include <geom2d/vec.h>

#define CU_ASSERT_VEC_EQUAL(A, B, eps) assert_vec_equal(A, B, eps)

void assert_vec_equal(vec_t, vec_t, double);

#endif

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <CUnit/CUnit.h>

#include "tests-contact.h"
#include "tests-cubic.h"
#include "tests-ellipse.h"
#include "tests-mat.h"
#include "tests-sdcontact.h"
#include "tests-vec.h"

#include "cunit-compat.h"

#define ENTRY(a, b) CU_Suite_Entry((a), NULL, NULL, (b))

static CU_SuiteInfo suites[] =
  {
    ENTRY("contact", tests_contact),
    ENTRY("cubic", tests_cubic),
    ENTRY("ellipse", tests_ellipse),
    ENTRY("matrix", tests_matrix),
    ENTRY("semi-degenerate contact", tests_sdcontact),
    ENTRY("vector", tests_vector),
    CU_SUITE_INFO_NULL,
  };

void suites_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr,
              "geom2d suite registration failed - %s\n",
	      CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}

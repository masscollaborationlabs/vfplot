#include <CUnit/CUnit.h>

extern CU_TestInfo tests_bbox[];

void test_bbox_width(void);
void test_bbox_height(void);
void test_bbox_area(void);
void test_bbox_join(void);
void test_bbox_intersect(void);

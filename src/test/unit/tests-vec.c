#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <geom2d/vec.h>

#include "assert-vec.h"
#include "tests-vec.h"

CU_TestInfo tests_vector[] =
  {
    {"subtraction", test_vsub},
    {"addition", test_vadd},
    {"scalar multiplication", test_vscale},
    {"norm", test_vabs},
    {"norm squared", test_vabs2},
    {"vector-pair determinant", test_vdet},
    {"scalar product", test_vdot},
    {"angle", test_vang},
    {"external angle", test_vxtang},
    {"unit vector - nonzero", test_vunit_nonzero},
    {"unit vector - nzero", test_vunit_zero},
    {"segment intersect", test_visect},
    {"bend", test_bend},
    CU_TEST_INFO_NULL
  };

static double eps = 1e-10;

void test_vsub(void)
{
  vec_t
    u[] = {{0, 1}, {1, 0}},
    v = vsub(u[0], u[1]),
    res = {-1, 1};

  CU_ASSERT_VEC_EQUAL(v, res, eps);
}

void test_vadd(void)
{
  vec_t
    u[] = {{0, 1}, {1, 0}},
    v = vadd(u[0], u[1]),
    res = {1, 1};

  CU_ASSERT_VEC_EQUAL(v, res, eps);
}

void test_vscale(void)
{
  vec_t
    u = {0, 1},
    v = vscale(2.0, u),
    res = {0, 2};

  CU_ASSERT_VEC_EQUAL(v, res, eps);
}

void test_vabs(void)
{
  vec_t u = {3, 4};
  double d = vabs(u);

  CU_ASSERT_DOUBLE_EQUAL(d, 5.0, eps);
}

void test_bend(void)
{
  vec_t a = {1, 0}, b = {0, 0}, c = {0, 1};

  CU_ASSERT_EQUAL(bend3p(a, b, c), rightward);
  CU_ASSERT_EQUAL(bend3p(c, b, a), leftward);
  CU_ASSERT_EQUAL(bend3p(a, c, b), leftward);
  CU_ASSERT_EQUAL(bend3p(b, c, a), rightward);
  CU_ASSERT_EQUAL(bend3p(b, a, c), leftward);
  CU_ASSERT_EQUAL(bend3p(c, a, b), rightward);
}

void test_vabs2(void)
{
  vec_t u = {3, 4};
  double d = vabs2(u);

  CU_ASSERT_DOUBLE_EQUAL(d, 25.0, eps);
}

void test_vdet(void)
{
  vec_t u = {1, 2}, v = {1, 1};
  double d = vdet(u, v);

  CU_ASSERT_DOUBLE_EQUAL(d, -1.0, eps);
}

void test_vdot(void)
{
  vec_t u = {3, 4}, v = {1, 2};
  double d = vdot(u, v);

  CU_ASSERT_DOUBLE_EQUAL(d, 11.0, eps);
}

#define PI4 (M_PI/4.0)

void test_vang(void)
{
  vec_t u[5] = {{ 4,  0},
		   { 4,  4},
		   { 4, -4},
		   {-4,  4},
		   {-4, -4}};

  CU_ASSERT_DOUBLE_EQUAL(vang(u[0]), 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(vang(u[1]), PI4, eps);
  CU_ASSERT_DOUBLE_EQUAL(vang(u[2]), -PI4, eps);
  CU_ASSERT_DOUBLE_EQUAL(vang(u[3]), 3*PI4, eps);
  CU_ASSERT_DOUBLE_EQUAL(vang(u[4]), -3*PI4, eps);
}

void test_vxtang(void)
{
  vec_t u = {4, 0}, v = {0, 4};
  double d = vxtang(u, v);

  CU_ASSERT_DOUBLE_EQUAL(d, M_PI / 2.0, eps);
}

void test_vunit_nonzero(void)
{
  vec_t
    u = {1, 1},
    v = vunit(u),
    res = {1.0 / sqrt(2.0), 1.0 / sqrt(2.0)};

  CU_ASSERT_VEC_EQUAL(v, res, eps);
}

void test_vunit_zero(void)
{
  vec_t v = {0, 0}, u = vunit(v);
  CU_ASSERT_VEC_EQUAL(v, u, eps);
}

void test_visect_01(void)
{
  vec_t
    u0 = { 1, 0 },
    u1 = { 0, 1 },
    v0 = { 0, 0 },
    v1 = { 1, 1 };
  CU_ASSERT_TRUE(visect(u0, u1, v0, v1));
}

void test_visect_02(void)
{
  vec_t
    u0 = { 1, 0 },
    u1 = { 0, 1 },
    v0 = { 0, 0 },
    v1 = { 0.51, 0.51 };
  CU_ASSERT_TRUE(visect(u0, u1, v0, v1));
}

void test_visect_03(void)
{
  vec_t
    u0 = { 1, 0 },
    u1 = { 0, 1 },
    v0 = { 0, 0 },
    v1 = { 0.49, 0.49 };
  CU_ASSERT_FALSE(visect(u0, u1, v0, v1));
}

void test_visect_04(void)
{
  vec_t
    u0 = { 1, 0 },
    u1 = { 0, 1 },
    v0 = { 0, 0 },
    v1 = { 2, -0.01 };
  CU_ASSERT_FALSE(visect(u0, u1, v0, v1));
}

void test_visect_05(void)
{
  vec_t
    u0 = { 1, 0 },
    u1 = { 0, 1 },
    v0 = { 0, 0 },
    v1 = { 2, 0.01 };
  CU_ASSERT_TRUE(visect(u0, u1, v0, v1));
}

void test_visect_06(void)
{
  vec_t
    u0 = { 1, 0 },
    u1 = { 0, 1 },
    v0 = { 0, 0 },
    v1 = { -0.01, 2 };
  CU_ASSERT_FALSE(visect(u0, u1, v0, v1));
}

void test_visect_07(void)
{
  vec_t
    u0 = { 1, 0 },
    u1 = { 0, 1 },
    v0 = { 0, 0 },
    v1 = { 0.01, 2 };
  CU_ASSERT_TRUE(visect(u0, u1, v0, v1));
}

void test_visect_08(void)
{
  vec_t
    u0 = { 1, 0 },
    u1 = { 0, 1 },
    v0 = { 1.1, 0 },
    v1 = { 0, 1.1 };
  CU_ASSERT_FALSE(visect(u0, u1, v0, v1));
}

void test_visect_09(void)
{
  vec_t
    u0 = { 1, 0 },
    u1 = { 0, 1 },
    v0 = { 0.9, 0 },
    v1 = { 0, 1.1 };
  CU_ASSERT_TRUE(visect(u0, u1, v0, v1));
}

void test_visect_10(void)
{
  vec_t
    u0 = { 1, 0 },
    u1 = { 0, 1 },
    v0 = { 1.1, 0 },
    v1 = { 0, 0.9 };
  CU_ASSERT_TRUE(visect(u0, u1, v0, v1));
}

void test_visect_11(void)
{
  vec_t
    u0 = { 1, 0 },
    u1 = { 0, 1 },
    v0 = { 0.9, 0 },
    v1 = { 0, 0.9 };
  CU_ASSERT_FALSE(visect(u0, u1, v0, v1));
}

void test_visect(void)
{
  test_visect_01();
  test_visect_02();
  test_visect_03();
  test_visect_04();
  test_visect_05();
  test_visect_06();
  test_visect_07();
  test_visect_08();
  test_visect_09();
  test_visect_10();
  test_visect_11();
}

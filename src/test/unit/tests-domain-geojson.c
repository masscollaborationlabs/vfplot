#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "tests-domain-geojson.h"

#ifdef HAVE_LIBJANSSON

#include <unistd.h>

#include <plot/domain-geojson.h>

#include "fixture.h"

CU_TestInfo tests_domain_geojson[] =
  {
    {"read, no polygons", test_domain_read_nopolys},
    {"read, square", test_domain_read_square},
    {"read, nested squares", test_domain_read_nested_squares},
    {"read, adjacent squares", test_domain_read_adjacent_squares},
    {"read, feature", test_domain_read_feature},
    {"read, feature collection", test_domain_read_feature_collection},
    {"read, geometry collection", test_domain_read_geometry_collection},
    {"write", test_domain_write},
    CU_TEST_INFO_NULL
  };

void test_domain_read_nopolys(void)
{
  const char *path = fixture("geojson/no-polygons.geojson");
  domain_t *dom = domain_read(path);
  CU_ASSERT_PTR_NULL(dom);
}

void test_domain_read_square(void)
{
  const char *path = fixture("geojson/square.geojson");
  domain_t *dom = domain_read(path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  domain_destroy(dom);
}

void test_domain_read_nested_squares(void)
{
  const char *path = fixture("geojson/nested-squares.geojson");
  domain_t *dom = domain_read(path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  domain_destroy(dom);
}

void test_domain_read_adjacent_squares(void)
{
  const char *path = fixture("geojson/adjacent-squares.geojson");
  domain_t *dom = domain_read(path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  domain_destroy(dom);
}

void test_domain_read_feature(void)
{
  const char *path = fixture("geojson/feature.geojson");
  domain_t *dom = domain_read(path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  domain_destroy(dom);
}

void test_domain_read_feature_collection(void)
{
  const char *path = fixture("geojson/feature-collection.geojson");
  domain_t *dom = domain_read(path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  domain_destroy(dom);
}

void test_domain_read_geometry_collection(void)
{
  const char *path = fixture("geojson/geometry-collection.geojson");
  domain_t *dom = domain_read(path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  domain_destroy(dom);
}

void test_domain_write(void)
{
  const char *fixture_path = fixture("geojson/nested-squares.geojson");
  const char tmp_path[] = "tmp/test-domain-read.geojson";
  domain_t *dom;

  dom = domain_read(fixture_path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  CU_ASSERT_EQUAL_FATAL(domain_write(tmp_path, dom), 0);
  domain_destroy(dom);

  dom = domain_read(tmp_path);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  domain_destroy(dom);

  CU_ASSERT_EQUAL(unlink(tmp_path), 0);
}

#else

CU_TestInfo tests_domain_geojson[] = { CU_TEST_INFO_NULL };

#endif

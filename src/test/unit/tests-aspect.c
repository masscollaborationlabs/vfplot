#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/aspect.h>

#include "tests-aspect.h"

CU_TestInfo tests_aspect[] = {
  {"example", test_aspect_example},
  CU_TEST_INFO_NULL,
};

void test_aspect_example(void)
{
  double length, width, eps = 1e-10;

  aspect_fixed(2, 2, &length, &width);

  CU_ASSERT_DOUBLE_EQUAL(length, 2, eps);
  CU_ASSERT_DOUBLE_EQUAL(width, 1, eps);
}

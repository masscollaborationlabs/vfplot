#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <geom2d/cubic.h>

#include "tests-cubic.h"

CU_TestInfo tests_cubic[] =
  {
    {"x(x-1)(x+1)", test_cubic_roots_1},
    {"x(x^2+1)", test_cubic_roots_2},
    {"(x-1)(x+1)", test_cubic_roots_3},
    {"(x-2)(x+1)", test_cubic_roots_4},
    {"x+1", test_cubic_roots_5},
    {"(x+1)^2", test_cubic_roots_6},
    {"x^3", test_cubic_roots_7},
    {"(x-2)(x+1)^2", test_cubic_roots_8},
    CU_TEST_INFO_NULL
  };

/* x(x-1)(x+1) */

void test_cubic_roots_1(void)
{
  double
    s[3], eps = 1e-10,
    c[4] = {0, -1, 0, 1};

  CU_ASSERT_EQUAL(cubic_roots(c, s), 3);
  CU_ASSERT_DOUBLE_EQUAL(s[0], 1, eps);
  CU_ASSERT_DOUBLE_EQUAL(s[1], 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(s[2], -1, eps);
}

/* x(x^2+1) */

void test_cubic_roots_2(void)
{
  double
    s[3], eps = 1e-10,
    c[4] = {0, 1, 0, 1};

  CU_ASSERT_EQUAL(cubic_roots(c, s), 1);
  CU_ASSERT_DOUBLE_EQUAL(s[0], 0, eps);
}

/* (x-1)(x+1) (degenerate) */

void test_cubic_roots_3(void)
{
  double
    s[3], eps = 1e-10,
    c[4] = {-1, 0, 1, 0};

  CU_ASSERT_EQUAL(cubic_roots(c, s), 2);
  CU_ASSERT_DOUBLE_EQUAL(s[0], 1, eps);
  CU_ASSERT_DOUBLE_EQUAL(s[1], -1, eps);
}

/* (x-2)(x+1) (degenerate) */

void test_cubic_roots_4(void)
{
  double
    s[3], eps = 1e-10,
    c[4] = {-2, -1, 1, 0};

  CU_ASSERT_EQUAL(cubic_roots(c, s), 2);
  CU_ASSERT_DOUBLE_EQUAL(s[0], 2, eps);
  CU_ASSERT_DOUBLE_EQUAL(s[1], -1, eps);
}

/* x+1 (degenerate) */

void test_cubic_roots_5(void)
{
  double
    s[3], eps = 1e-10,
    c[4] = {1, 1, 0, 0};

  CU_ASSERT_EQUAL(cubic_roots(c, s), 1);
  CU_ASSERT_DOUBLE_EQUAL(s[0], -1, eps);
}

/* (x+1)^2 (degenerate) */

void test_cubic_roots_6(void)
{
  double
    s[3], eps = 1e-10,
    c[4] = {1, 2, 1, 0};

  CU_ASSERT_EQUAL(cubic_roots(c, s), 1);
  CU_ASSERT_DOUBLE_EQUAL(s[0], -1, eps);
}

/* x^3 */

void test_cubic_roots_7(void)
{
  double
    s[3], eps = 1e-10,
    c[4] = {0, 0, 0, 1};

  CU_ASSERT_EQUAL(cubic_roots(c, s), 1);
  CU_ASSERT_DOUBLE_EQUAL(s[0], 0, eps);
}

/* (x-2)(x+1)^2 */

void test_cubic_roots_8(void)
{
  double
    s[3], eps = 1e-10,
    c[4] = {-2, -3, 0, 1};

  CU_ASSERT_EQUAL(cubic_roots(c, s), 2);
  CU_ASSERT_DOUBLE_EQUAL(s[0], 2, eps);
  CU_ASSERT_DOUBLE_EQUAL(s[1], -1, eps);
}

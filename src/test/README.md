Tests
=====

We have

- unit tests for the C code (using [CUnit][1])
- acceptance tests for the programs (using [BATS][2])
- memory tests for C programs (using [valgrind][3])
- visual tests, which make example plots

To run these,

    ./configure --enable-unit-tests
    make
    make test-unit

or

    ./configure --enable-acceptance-tests
    make
    make test-accept

or

    ./configure --enable-memory-tests
    make
    make test-memory

or

    ./configure --enable-visual-tests
    make
    make test-visual

at the bottom level.  The configuration step will fail if the
required libraries or programs are not found.

One can also run

    ./configure --enable-tests

which configures for all test-types, and

    make test

which runs all tests which have been configured.

[1]: http://cunit.sourceforge.net/
[2]: https://github.com/bats-core/bats-core
[3]: https://valgrind.org/

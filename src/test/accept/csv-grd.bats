#!/usr/bin/env bats

load 'shared'

setup()
{
    skip_unless_with_netcdf
    program='csv-grd'
    csv_dir="${fixture_dir}/csv"
    path="${src_dir}/${program}/${program}"
    grd_u_path="${output_dir}/u.grd"
    grd_v_path="${output_dir}/v.grd"
}

teardown()
{
    rm -f "${grd_u_path}" "${grd_v_path}" 'gmt.history'
    rmdir "${output_dir}"
}

@test 'csv-grd, --version' {
    run $path --version
    [ $status -eq 0 ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'csv-grd, --help' {
    $path --help
}

@test 'csv-grd, default behaviour' {
    csv_path="${csv_dir}/simple.csv"
    run $path -i $csv_path $grd_u_path $grd_v_path
    [ $status -eq 0 ]
    [ -e "${grd_u_path}" ]
    [ -e "${grd_v_path}" ]
}

@test 'csv-grd, --pixel' {
    csv_path="${csv_dir}/simple.csv"
    run $path --pixel -i $csv_path $grd_u_path $grd_v_path
    [ $status -eq 0 ]
    [ -e "${grd_u_path}" ]
    [ -e "${grd_v_path}" ]
}

@test 'csv-grd, invalid option' {
    csv_path="${csv_dir}/simple.csv"
    run $path --invalid -i $csv_path $grd_u_path $grd_v_path
    [ $status -ne 0 ]
    [ ! -e "${grd_u_path}" ]
    [ ! -e "${grd_v_path}" ]
}

@test 'csv-grd, reading input from stdin fails' {
    csv_path="${csv_dir}/simple.csv"
    run $path $grd_u_path $grd_v_path < $csv_path
    [ $status -ne 0 ]
    [ ! -e "${grd_u_path}" ]
    [ ! -e "${grd_v_path}" ]
}

@test 'csv-grd, one output file' {
    csv_path="${csv_dir}/simple.csv"
    run $path -i $csv_path $grd_u_path
    [ $status -ne 0 ]
    [ ! -e "${grd_u_path}" ]
}

@test 'csv-grd, absent input file' {
    csv_path="${csv_dir}/no-such-file.csv"
    run $path -i $csv_path $grd_u_path $grd_v_path
    [ $status -ne 0 ]
    [ ! -e "${grd_u_path}" ]
    [ ! -e "${grd_v_path}" ]
}

@test 'csv-grd, input file with bad line' {
    csv_path="${csv_dir}/bad-line.csv"
    run $path -i $csv_path $grd_u_path $grd_v_path
    [ $status -ne 0 ]
    [ ! -e "${grd_u_path}" ]
    [ ! -e "${grd_v_path}" ]
}

@test 'csv-grd, invalid grid-size' {
    csv_path="${csv_dir}/simple.csv"
    run $path --grid-size 3 -i $csv_path $grd_u_path $grd_v_path
    [ $status -ne 0 ]
    [ ! -e "${grd_u_path}" ]
    [ ! -e "${grd_v_path}" ]
}

@test 'csv-grd, infeasible grid-size' {
    csv_path="${csv_dir}/simple.csv"
    run $path --grid-size 3/1 -i $csv_path $grd_u_path $grd_v_path
    [ $status -ne 0 ]
    [ ! -e "${grd_u_path}" ]
    [ ! -e "${grd_v_path}" ]
}

@test 'csv-grd, invalid grid-range' {
    csv_path="${csv_dir}/simple.csv"
    run $path --grid-range 0/1/0 -i $csv_path $grd_u_path $grd_v_path
    [ $status -ne 0 ]
    [ ! -e "${grd_u_path}" ]
    [ ! -e "${grd_v_path}" ]
}

@test 'csv-grd, infeasible grid-range' {
    csv_path="${csv_dir}/simple.csv"
    run $path --grid-range 0/1/1/0 -i $csv_path $grd_u_path $grd_v_path
    [ $status -ne 0 ]
    [ ! -e "${grd_u_path}" ]
    [ ! -e "${grd_v_path}" ]
}

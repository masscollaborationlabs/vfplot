#!/usr/bin/env bats

load 'shared'

setup()
{
    program='vfplot'
    path="${src_dir}/vfplot/${program}"
    geometry="-m 4/4/0 -w 4i"
}

teardown()
{
    rmdir "${output_dir}"
}

@test 'vfplot, --version' {
    run $path --version
    [ $status -eq 0 ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'vfplot, --help' {
    $path --help
}

@test 'vfplot, --placement list' {
    $path --placement list
}

@test 'vfplot, --unknown' {
    run $path --unknown
    [ $status -ne 0 ]
}

@test 'vfplot, --placement hedgehog' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path -p hedgehog $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --placement adaptive' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path -p adaptive -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --placement unknown' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path -p unknown -i 30/5 $geometry -t $plot -o $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --number --scale (hedgehog)' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path -p hedgehog -n 100 -s 1 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --number --scale (adaptive)' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path -p adaptive -n 100 -s 1 $geometry -t $plot -o $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --number (adaptive)' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path -v -p adaptive -n 100 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --scale (adaptive)' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path -v -p adaptive -s 1 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --crop X (bad argument)' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --crop X -i 30/5 $geometry -t $plot -o $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --crop 1x (bad unit)' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --crop 1x -i 30/5 $geometry -t $plot -o $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --crop 1c' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --crop 1c -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --crop 1c/0' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --crop 1c/0 -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --crop 1c/0/1c' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --crop 1c/0/1c -i 30/5 $geometry -t $plot -o $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --crop 1c/1c/0/1c' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --crop 1c/1c/0/1c -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --domain' {
    skip_unless_with_json
    plot='circular'
    eps="${output_dir}/${plot}.eps"
    geojson="${fixture_dir}/geojson/nested-squares.geojson"
    run $path -v -d $geojson -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --graphic-state' {
    skip_unless_with_json
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    state="${output_dir}/${plot}.json"
    log="${output_dir}/${plot}.log"
    run $path -v --graphic-state $state -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${state}" ]
    rm -f $eps
    run $path -v --graphic-state $state $geometry -t $plot -L $log -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${state}" ]
    rm -f $eps $state $log
}

@test 'vfplot, --dump-domain' {
    skip_unless_with_json
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    geojson="${output_dir}/${plot}.geojson"
    $path --dump-domain $geojson -i 30/5 $geometry -t $plot -o $eps
    [ -e "${geojson}" ]
    rm -f $eps
    run $path --domain $geojson -i 30/5 $geometry -t $plot -o $eps
    [ -e "${eps}" ]
    rm -f $eps $geojson
}

@test 'vfplot, --dump-vectors' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    csv="${output_dir}/${plot}.csb"
    run $path --dump-vectors $csv -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${csv}" ]
    rm -f $eps $csv
}

@test 'vfplot, --histogram' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    hst="${output_dir}/${plot}.hst"
    run $path --histogram $hst -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${hst}" ]
    rm -f $eps $hst
}

@test 'vfplot, --symbol list' {
    $path --symbol list
}

@test 'vfplot, --symbol arrow' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --symbol arrow -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --symbol triangle' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --symbol triangle -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --aspect positive' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --aspect 10 -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --aspect zero' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --aspect 0 -i 30/5 $geometry -t $plot -o $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --log-path' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    log="${output_dir}/${plot}.log"
    run $path --log-path $log -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${log}" ]
    rm -f $eps $log
}

@test 'vfplot, --log-level' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    log="${output_dir}/${plot}.log"
    run $path --log-path $log --log-level DEBUG \
        -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${log}" ]
    rm -f $eps $log
}

@test 'vfplot, --statistics' {
    skip_unless_with_json
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    json="${output_dir}/${plot}-statistics.json"
    run $path -v --statistics $json -i 30/5 -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${json}" ]
    # FIXME: make some assertions on the contents of $json, this
    # will need jq and some wrapper functions ...
    rm -f $eps $json
}

@test 'vfplot, --output-format list' {
    $path --output-format list
}

@test 'vfplot, --output-format eps' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --output-format eps -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --output-format povray' {
    plot='cylinder'
    pov="${output_dir}/${plot}.pov"
    run $path --output-format povray -i 30/5 $geometry -t $plot -o $pov
    [ $status -eq 0 ]
    [ -e "${pov}" ]
    rm -f $pov
}

@test 'vfplot, --output-format unknown' {
    plot='cylinder'
    moot="${output_dir}/${plot}.moot"
    run $path --output-format unknown -i 30/5 $geometry -t $plot -o $moot
    [ $status -ne 0 ]
    [ ! -e "${moot}" ]
}

@test 'vfplot, --break list' {
    $path --break list
}

@test 'vfplot, --break grid' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --break grid -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --break super' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --break super -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --break midclean' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --break midclean -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --break postclean' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --break postclean -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --break none' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --break none -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --format list' {
    $path --format list
}

@test 'vfplot, --format grd2, explicit' {
    plot='grd2-input'
    eps="${output_dir}/${plot}.eps"
    grdu="${fixture_dir}/grd/cyl-u.grd"
    grdv="${fixture_dir}/grd/cyl-v.grd"
    run $path --format grd2 -i 30/5 $geometry -o $eps $grdu $grdv
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --format grd2, too many files' {
    plot='grd2-input'
    eps="${output_dir}/${plot}.eps"
    grdu="${fixture_dir}/grd/cyl-u.grd"
    grdv="${fixture_dir}/grd/cyl-v.grd"
    grdw="${fixture_dir}/grd/cyl-v.grd"
    run $path -i 30/5 $geometry -o $eps $grdu $grdv $grdw
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --format grd2, absent files' {
    plot='grd2-input'
    eps="${output_dir}/${plot}.eps"
    grdu="${fixture_dir}/grd/absent-u.grd"
    grdv="${fixture_dir}/grd/absent-v.grd"
    run $path -i 30/5 $geometry -o $eps $grdu $grdv
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --format grd2 --grid-size' {
    plot='grd2-input'
    eps="${output_dir}/${plot}.eps"
    grdu="${fixture_dir}/grd/cyl-u.grd"
    grdv="${fixture_dir}/grd/cyl-v.grd"
    run $path --format grd2 --grid-size 100/100 \
        -i 30/5 $geometry -o $eps $grdu $grdv
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --format grd2, implicit' {
    plot='grd2-input'
    eps="${output_dir}/${plot}.eps"
    grdu="${fixture_dir}/grd/cyl-u.grd"
    grdv="${fixture_dir}/grd/cyl-v.grd"
    run $path -i 30/5 $geometry -o $eps $grdu $grdv
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

# realistic csv files are large, so rather than have a fixture
# we generate them on-the-fly

@test 'vfplot, --format csv, explicit' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    csv="${output_dir}/${plot}.csv"
    $path --dump-vectors $csv --break grid $geometry -t $plot
    [ -e "${csv}" ]
    run $path --format csv --grid-size 128/128 --grid-range 0/1/0/1 \
        -i 30/5 $geometry -o $eps $csv
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps $csv
}

@test 'vfplot, --format csv, implicit' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    csv="${output_dir}/${plot}.csv"
    $path --dump-vectors $csv --break grid $geometry -t $plot
    [ -e "${csv}" ]
    run $path --verbose --grid-size 128/128 --grid-range 0/1/0/1 \
        -i 30/5 $geometry -o $eps $csv
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps $csv
}

@test 'vfplot, --format csv, no --grid-size' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    csv="${output_dir}/${plot}.csv"
    $path --dump-vectors $csv --break grid $geometry -t $plot
    [ -e "${csv}" ]
    run $path --verbose --grid-range 0/1/0/1 \
        -i 30/5 $geometry -o $eps $csv
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps $csv
}

@test 'vfplot, --format gfs, explict, eps' {
    skip_unless_with_gerris
    plot='gfs-input-explicit'
    eps="${output_dir}/${plot}.eps"
    gfs="${fixture_dir}/gfs/house-001.gfs"
    run $path --format gfs -i 5/5 -w 6i -m 4/4/0.5 -s 3e-4 -o $eps $gfs
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --format gfs, implict, eps' {
    skip_unless_with_gerris
    plot='gfs-input-implicit'
    eps="${output_dir}/${plot}.eps"
    gfs="${fixture_dir}/gfs/house-001.gfs"
    run $path -i 5/5 -w 6i -m 4/4/0.5 -s 3e-4 -o $eps $gfs
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --format gfs, --grid-size' {
    skip_unless_with_gerris
    plot='gfs-input-grid'
    eps="${output_dir}/${plot}.eps"
    gfs="${fixture_dir}/gfs/house-001.gfs"
    run $path --format gfs --grid-size 100/100 \
        -i 5/5 -w 6i -m 4/4/0.5 -s 3e-4 -o $eps $gfs
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --animate' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --animate -i 5/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
    for i in $(seq 0 4)
    do
        for j in $(seq 0 4)
        do
            eps=$(printf "anim.%04i.%04i.eps" $i $j)
            [ -e "${eps}" ]
            rm -f $eps
        done
    done
}

@test 'vfplot, --animate-dir' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    animate_dir=$(mktemp -d)
    run $path --animate-dir $animate_dir -i 5/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
    for i in $(seq 0 4)
    do
        for j in $(seq 0 4)
        do
            eps=$(printf "%s/anim.%04i.%04i.eps" $animate_dir $i $j)
            [ -e "${eps}" ]
            rm -f $eps
        done
    done
    rmdir $animate_dir
}

@test 'vfplot, --fill grey' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --fill 100 -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --fill RGB' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --fill 10/10/20 -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --fill bad-arguument' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --fill 1000/10 -i 30/5 $geometry -t $plot -o $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --pen' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --pen 2/125 -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --domain-pen' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --domain-pen 2/125 -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --network' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --network 2/125 -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --ellipse-pen' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --ellipse --ellipse-pen 2/125 -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --ellipse-fill' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --ellipse --ellipse-fill 200 -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --sort list' {
    $path --sort list
}

@test 'vfplot, --sort none' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --sort none -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --sort longest' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --sort longest -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --sort shortest' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --sort shortest -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --sort straightest' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --sort straightest -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --sort bendiest' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --sort bendiest -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --height' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --height 5i -i 30/5 -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --width' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --width 5i -i 30/5 -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --height --width' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path --height 5i --width 5i -i 30/5 -t $plot -o $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --test list' {
    $path --test list
}

@test 'vfplot, --test circular' {
    plot='circular'
    eps="${output_dir}/${plot}.eps"
    run $path -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --test cylinder' {
    plot='cylinder'
    eps="${output_dir}/${plot}.eps"
    run $path -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --test electro2' {
    plot='electro2'
    eps="${output_dir}/${plot}.eps"
    run $path -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --test electro3' {
    plot='electro3'
    eps="${output_dir}/${plot}.eps"
    run $path -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --test smoska' {
    plot='smoska'
    eps="${output_dir}/${plot}.eps"
    run $path -v -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

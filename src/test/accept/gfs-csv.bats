#!/usr/bin/env bats

load 'shared'

setup()
{
    skip_unless_with_gerris
    program='gfs-csv'
    path="${src_dir}/${program}/${program}"
    gfs_path="${fixture_dir}/gfs/house-001.gfs"
    output_path="${output_dir}/output.dat"
}

teardown()
{
    rm -f "${output_path}"
    rmdir "${output_dir}"
}

@test 'gfs-csv, --version' {
    run $path --version
    [ $status -eq 0 ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'gfs-csv, --help' {
    $path --help
}

@test 'gfs-csv, --unknown' {
    run $path --unknown
    [ $status -ne 0 ]
}

@test 'gfs-csv, no --scalar' {
    run $path -o $output_path $gfs_path
    [ $status -ne 0 ]
    [ ! -e "${output_path}" ]
}

@test 'gfs-csv, --scalar U' {
    run $path --scalar U -o $output_path $gfs_path
    [ $status -eq 0 ]
    [ -e "${output_path}" ]
    [ $(cat "${output_path}" | wc -l) -gt 100 ]
}

@test 'gfs-csv, --scalar U --scalar V' {
    run $path --scalar U --scalar V -o $output_path $gfs_path
    [ $status -eq 0 ]
    [ -e "${output_path}" ]
    [ $(cat "${output_path}" | wc -l) -gt 100 ]
}

@test 'gfs-csv, --scalar U,V' {
    run $path --scalar 'U,V' -o $output_path $gfs_path
    [ $status -eq 0 ]
    [ -e "${output_path}" ]
    [ $(cat "${output_path}" | wc -l) -gt 100 ]
}

@test 'gfs-csv, --scalar Vorticity' {
    run $path --scalar 'Vorticity' -o $output_path $gfs_path
    [ $status -eq 0 ]
    [ -e "${output_path}" ]
    [ $(cat "${output_path}" | wc -l) -gt 100 ]
}

@test 'gfs-csv, --scalar NoSuch' {
    run $path --scalar 'NoSuch' -o $output_path $gfs_path
    [ $status -ne 0 ]
    [ ! -e "${output_path}" ]
}

@test 'gfs-csv, too many files' {
    run $path --scalar U -o $output_path $gfs_path $gfs_path
    [ $status -ne 0 ]
    [ ! -e "${output_path}" ]
}

@test 'gfs-csv, --verbose' {
    run $path --scalar U --verbose -o $output_path $gfs_path
    [ $status -eq 0 ]
    [ -e "${output_path}" ]
}

@test 'gfs-csv, --verbose, no --output' {
    run $path --scalar U --verbose $gfs_path
    [ $status -ne 0 ]
}

@test 'gfs-csv, write to stdout' {
    run $path --scalar U $gfs_path > $output_path
    [ $status -eq 0 ]
    [ -e "${output_path}" ]
}

@test 'gfs-csv, read from stdin' {
    run $path --scalar U -o $output_path < $gfs_path
    [ $status -eq 0 ]
    [ -e "${output_path}" ]
}

@test 'gfs-csv, imput GFS does not exist' {
    run $path --scalar U -o $output_path 'does/not/exist.gfs'
    [ $status -ne 0 ]
    [ ! -e "${output_path}" ]
}

@test 'gfs-csv, imput GFS corrupt' {
    run $path --scalar U -o $output_path "${fixture_dir}/gfs/corrupt.gfs"
    [ $status -ne 0 ]
    [ ! -e "${output_path}" ]
}

@test 'gfs-csv, output path unwriteable' {
    run $path --scalar U -o "${output_dir}/inexistent/file.dat" $gfs_path
    [ $status -ne 0 ]
}

@test 'gfs-csv, --index' {
    run $path --scalar U --index -o $output_path $gfs_path
    [ $status -eq 0 ]
    [ -e "${output_path}" ]
    [ $(cat "${output_path}" | wc -l) -gt 100 ]
}

@test 'gfs-csv, --delimiter' {
    run $path --scalar U --delimiter '|' -o $output_path $gfs_path
    [ $status -eq 0 ]
    [ -e "${output_path}" ]
    [ $(cat "${output_path}" | wc -l) -gt 100 ]
}

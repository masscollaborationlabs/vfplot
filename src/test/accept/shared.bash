#!/usr/bin/env bash

version=$(cat ../../../VERSION)
fixture_dir='../fixtures'
src_dir='../..'
output_dir=$(mktemp -d)

source 'config.bash'

function skip_unless_with_json
{
    if [ -z "$WITH_JSON" ] ; then
        skip 'no JSON support'
    fi
}

function skip_unless_with_gerris
{
    if [ -z "$WITH_GERRIS" ] ; then
        skip 'no Gerris support'
    fi
}

function skip_unless_with_netcdf
{
    if [ -z "$WITH_NETCDF" ] ; then
        skip 'no NetCDF support'
    fi
}

function skip_unless_with_matlab
{
    if [ -z "$WITH_MATLAB" ] ; then
        skip 'no Matlab support'
    fi
}

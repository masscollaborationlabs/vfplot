#version 3.7;
#include "colors.inc"
#include "shapes.inc"

#declare CD = 330;
#declare MX = 144;
#declare MY = 144;

global_settings {
  assumed_gamma 1
}

camera {
  location <1.3*MX, MY, -CD>
  look_at <MX, MY, 0>
}

light_source {
  <0, 0, -CD>
  color rgb 0.5
  parallel
  point_at <0, 0, 0>
}

light_source {
  <0, MY, -CD>
  color rgb 0.3
}

light_source {
  <2*MX, MY, -CD>
  color rgb 0.3
}

light_source {
  <MX, 2*MY, -CD>
  color rgb 0.4
}

background {
  color rgb 0.9
}

#declare vfplot_arrow_texture =
  texture {
    pigment {
      color Blue
    }
    finish {
      phong 0.5
    }
  };

#include "povray-crop.inc"

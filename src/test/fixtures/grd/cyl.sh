#!/bin/sh

set -e

# create the cylinder test grids, needs vfplot on the path
# and a GMT install -- note that vfplot --dump-vectors writes
# pixel-aligned data, so we need to use the --pixel option
# in the call to csv-grd.  The gedsample call will create
# a warning about aliasing, that's OK

CSV="/tmp/cyl.csv"
EPS="/tmp/cyl.eps"

vfplot -v --scale 1 --dump-vectors $CSV --break grid -t cylinder -o $EPS
csv-grd -v -p -r-1/1/-1/1 -i $CSV cyl-u-128.grd cyl-v-128.grd
grdsample -I64+n cyl-u-128.grd -Gcyl-u.grd
grdsample -I64+n cyl-v-128.grd -Gcyl-v.grd
rm -f $EPS $CSV cyl-u-128.grd cyl-v-128.grd

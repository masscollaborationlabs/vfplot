# placeholder

default: test

include ../Common.mk
include ../../Common.mk

EPS = circular.eps cylinder.eps electro2.eps electro3.eps smoska.eps
PNG = $(EPS:.eps=.png)
RUBBISH = $(EPS) $(PNG)

test: $(PNG)

clean spotless:
	$(RM) $(RUBBISH)

ifdef XDGOPEN
show: test
	$(XDGOPEN) ./index.html
else
show:
	@echo 'no xdg-open found, browse index.html manually'
endif

circular.eps: $(VFPLOT)
	$(VFPLOT) \
	--verbose \
	--test circular \
	--domain $(FIXTURES)/geojson/nested-squares.geojson \
	--scale 0.4 \
	--ellipse \
	--domain-pen 1 \
	--pen 0 \
	--placement adaptive \
	--width 4i \
	--margin 3/3/0 \
	--iterations 150/10 \
	--output circular.eps

cylinder.eps: $(VFPLOT)
	$(VFPLOT) \
	--verbose \
	--test cylinder \
	--domain-pen 2 \
	--scale 0.75 \
	--fill 200 \
	--pen 0 \
	--width 4i \
	--placement adaptive \
	--margin 4/4/0 \
	--ellipse \
	--iterations 50/10 \
	--output cylinder.eps

electro2.eps: $(VFPLOT)
	$(VFPLOT) \
	--verbose \
	--test electro2 \
	--domain-pen 2 \
	--scale 0.4 \
	--fill 200 \
	--pen 0.2m \
	--width 4i \
	--symbol triangle \
	--placement adaptive \
	--margin 4/4/0 \
	--overfill 3 \
	--output electro2.eps

electro3.eps: $(VFPLOT)
	$(VFPLOT) \
	--verbose \
	--test electro3 \
	--domain-pen 2 \
	--scale 0.4 \
	--fill 200 \
	--pen 0.2m \
	--width 4i \
	--symbol triangle \
	--placement adaptive \
	--margin 4/4/0 \
	--length 0/1in \
	--overfill 4 \
	--output electro3.eps

smoska.eps: $(VFPLOT)
	$(VFPLOT) \
	--verbose \
	--test smoska \
	--scale 1 \
	--fill 200 \
	--pen 0.2 \
	--width 5i \
	--crop 0.5i \
	--placement adaptive \
	--margin 3/3/0.1 \
	--iterations 150/10 \
	--output smoska.eps

# no -g given here, that specifies the size of the resulting png,
# but the EPSCrop driver works that out itself from the -r option,
# to get a desired size, one would need to get the width from the
# bounding box, get the ratio of actual size to deisred size, then
# set -r to be 72 times that, bit fiddly for a Makefile, so we
# set that ratio to be 2, which gives us 8i square images.

%.png: %.eps
	$(GS) -q -dSAFER -dNOPAUSE -dBATCH \
	-dEPSCrop \
	-dGraphicsAlphaBits=4 \
	-sDEVICE=png16m  \
	-r144 \
	-sOutputFile=$@ \
	-f$<

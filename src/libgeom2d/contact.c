#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdlib.h>

#include "geom2d/contact.h"

/*
  A 2-dimensional version of the contact function of

  J.W. Perram & M.S. Wertheim "Statistical Mechanics
  of Hard Ellipsiods", J. Comp. Phys., 58, 409-416 (1985)
*/

#define CONTACT_EPS 1e-8
#define CONTACT_ITER 25

int contact(const ellipse_t *A, const ellipse_t *B, double *fmax, vec_t *vmax)
{
  mat_t MA, MB;

  if ((ellipse_mat(A, &MA) != 0) || (ellipse_mat(B, &MB) != 0))
    return 1;

  vec_t rAB = vsub(B->centre, A->centre);

  return contact_mt(rAB, MA, MB, fmax, vmax);
}

static void contact_d(vec_t, mat_t, mat_t, double, double*, double*, double*);
static mat_t contact_v(mat_t, mat_t, double);
static double constrained_subtract(double, double);

/*
  Find the maximum of F by locating the zero of its derivate by a
  Newton-Raphson iteration. This typically takes 3-5 iterations
  to get to 1e-10 accuracy.

  The function returns a value of F with |F'| < eps or negative if
  the iteration did not converge.

  Note
  - the step reduction if the iteration takes us outside [0, 1]
  - that F is strictly convex so F' is increasing and F''
    positive, which saves a check
  - the iteration terminates as soon as a value of F > 1.0 has been
    found, since this corresponds to non-intersecting ellipses, and
    in this case we do not use the value

  This function is also exported, since one might want to cache the
  A and B values calculated in contact()

  The t argument is the starting point for the iteration, in some
  circumstance one might have a better guess than 0.5.
*/

int contact_mt(vec_t rAB, mat_t A, mat_t B, double *fmax, vec_t *vmax)
{
  return contact_mt0(rAB, A, B, 0.5, fmax, vmax);
}

int contact_mt0(vec_t rAB, mat_t A, mat_t B, double t,
                double *fmax, vec_t *vmax)
{
  double F, dF, ddF, dt;

  for (size_t i = 0 ; i < CONTACT_ITER ; i++)
    {
      contact_d(rAB, A, B, t, &F, &dF, &ddF);

      /* only caclulate & assign vmax if non-nil */

      if ((fabs(dF) < CONTACT_EPS) || (F > 1))
        {
          *fmax = F;
          if (vmax != NULL)
            *vmax = mpdir(contact_v(A, B, t), rAB);
          return 0;
        }

      dt = dF / ddF;
      t = constrained_subtract(t, dt);
    }

  return 1;
}

/*
  return t - dt unless the result would be outside [0, 1],
  in which case move halfway towards the offending boundary
*/

static double constrained_subtract(double t, double dt)
{
  const double t1 = t - dt;

  if (t1 <= 0)
    return t / 2;

  if (t1 >= 1)
    return (t + 1) / 2;

  return t1;
}

/*
  Using the formulae

    F   = rDr
    F'  = rD((1-t)^2 A - t^2 B)Dr
    F'' = -rD(ADB + BDA)Dr

  excepting the calculation of D this takes

  - 9 matrix-vector multiplys
  - 5 scalar products
  - some odds & sods,

  using 51 mutiply, 25 add.
*/

static void contact_d(vec_t r, mat_t A, mat_t B, double t,
		      double *F, double *dF, double *ddF)
{
  const double
    s = 1 - t,
    s2 = s * s,
    t2 = t * t;

  const mat_t
    D = minv(madd(msmul(s, A), msmul(t, B)));

  /* 9(4m+2a) */

  const vec_t
    Dr = mvmul(D, r),
    DADr = mvmul(D, mvmul(A, Dr)),
    DBDr = mvmul(D, mvmul(B, Dr)),
    DADBDr = mvmul(D, mvmul(A, DBDr)),
    DBDADr = mvmul(D, mvmul(B, DADr));

  /* 5(2m+1a) */

  const double
    rDr = vdot(r, Dr),
    rDADr = vdot(r, DADr),
    rDBDr = vdot(r, DBDr),
    rDADBDr = vdot(r, DADBDr),
    rDBDADr = vdot(r, DBDADr);

  /* 5m+2a */

  *F = s * t * rDr;
  *dF = s2 * rDADr - t2 * rDBDr;
  *ddF = -(rDADBDr + rDBDADr);
}

static mat_t contact_v(mat_t A, mat_t B, double t)
{
  return minv(madd(msmul(1 - t, A), msmul(t, B)));
}

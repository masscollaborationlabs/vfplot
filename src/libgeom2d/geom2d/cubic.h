#ifndef GEOM2D_CUBIC_H
#define GEOM2D_CUBIC_H

int quadratic_roots(const double[static 3], double[static 2]);
int cubic_roots(const double[static 4], double[static 3]);

#endif

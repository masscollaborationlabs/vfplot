#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "geom2d/ellipse.h"
#include "geom2d/cubic.h"
#include "geom2d/sincos.h"

#include <log.h>

#include <math.h>
#include <stdio.h>

/* the (inverse of the) metric tensor */

int mat_ellipse(const mat_t *pM, ellipse_t *E)
{
  mat_t M = *pM;
  double
    A[3] = { M.a * M.d - M.b * M.c, -M.a - M.d, 1.0 },
    r[2], s[2];
  int n = quadratic_roots(A, r);

  switch (n)
    {
    case 1:

      /* equal eigenvalues, ellipse is a circle */

      if (r[0] > 0)
        {
          double a = sqrt(r[0]);

          E->major = a;
          E->minor = a;
          E->theta = 0.0;

          return 0;
        }
      else
        {
          log_error("bad repeated eigenvalue %f", r[0]);
          return 1;
        }

    case 2:

      if (r[0] > r[1])
        {
          s[0] = r[0];
          s[1] = r[1];
        }
      else
        {
          s[0] = r[1];
          s[1] = r[0];
        }
      break;

    default:
      log_error("bad number of eigenvalues (%i) in metric tensor", n);
      log_debug("quadratic (%.8e, %.8e, %.8e)", A[0], A[1], A[2]);
      log_debug("matrix [%.8e, %.8e]", M.a, M.b);
      log_debug("       [%.8e, %.8e]", M.c, M.d);
      return 1;
    }

  /*
    s contains the eigenvalues, possibly repeated and in
    order of decresing size
  */

  if (!(s[1] > 0))
    {
      log_error("non-positive eigenvalue (%f) in metric tensor", s[1]);
      return 1;
    }

  E->major = sqrt(s[0]);
  E->minor = sqrt(s[1]);

  /*
    Since the si are the eigenvalues, the eigenvectors are
    in the direction of the ellipse's major and minor axes.
    A short calulation shows that the matrix R is

       [ cos^2(t)  sin(t) cos(t) ]
       [ sin(t) cos(t)  sin^2(t) ]

    so that d/c and b/a are tan(t) -- we chose so as to
    avoid nans
  */

  mat_t R = msmul(1 / (s[0] - s[1]), mres(M, s[1]));
  double t = atan((R.a < R.d) ? (R.d / R.c) : (R.b / R.a));

  if (t < 0) t += M_PI;

  E->theta = t;

  return 0;
}

int ellipse_mat(const ellipse_t *e, mat_t *M)
{
  double
    a = e->major,
    b = e->minor;
  mat_t
    A = {a * a, 0, 0, b * b},
    R = mrot(e->theta),
    S = mtranspose(R);

  *M = mmmul(R, mmmul(A, S));

  return 0;
}

int ellipse_tangent_points(const ellipse_t *e, double t, vec_t *v)
{
  /*
    first find the tangent points of an ellipse centered at the
    origin with major axis along the x-axis
  */

  double st, ct;

  sincos(t - e->theta, &st, &ct);

  double
    a = e->major,
    b = e->minor,
    a2 = a * a,
    b2 = b * b,
    D = hypot(a * st, b * ct);

  vec_t u[2] =
    {
     {a2 * st / D, -b2 * ct / D},
     {-a2 * st / D, b2 * ct / D}
    };

  /*
    now rotate those points to the orientation of the ellipse
    and translate it by the position vector of the ellipse's
    centre
  */

  mat_t R = mrot(e->theta);

  for (size_t i = 0 ; i < 2 ; i++)
    v[i] = vadd(e->centre, mvmul(R, u[i]));

  return 0;
}

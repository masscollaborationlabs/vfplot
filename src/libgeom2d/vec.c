#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>

#include "geom2d/vec.h"
#include "geom2d/sincos.h"


vec_t vadd(vec_t u, vec_t v)
{
  return (vec_t){u.x + v.x, u.y + v.y};
}

vec_t vsub(vec_t u, vec_t v)
{
  return (vec_t){u.x - v.x, u.y - v.y};
}

vec_t vmid(vec_t u, vec_t v)
{
  return vscale(0.5, vadd(u, v));
}

vec_t vscale(double C, vec_t u)
{
  return (vec_t){C * u.x, C * u.y};
}

double vabs(vec_t u)
{
  return hypot(u.x, u.y);
}

double vabs2(vec_t u)
{
  return vdot(u, u);
}

double vang(vec_t u)
{
  return atan2(u.y, u.x);
}

double vdot(vec_t u, vec_t v)
{
  return u.x * v.x + u.y * v.y;
}

/* determinant of the matrix [u v] */

double vdet(vec_t u, vec_t v)
{
  return u.x * v.y - u.y * v.x;
}

/* possibly obtuse angle between u and V */

double vxtang(vec_t u, vec_t v)
{
  double
    ct = vdot(u, v),
    st = vdet(u, v);

  return atan2(st, ct);
}

/*
  if the argument is non-zero, then the unit vector in the
  same direction is returned; otherwise the zero vector is
  id returned
*/

vec_t vunit(vec_t v)
{
  double absv = vabs(v);
  if (absv == 0.0)
    return v;
  else
    return vscale(1 / absv, v);
}

bool visect(vec_t u0, vec_t u1, vec_t v0, vec_t v1)
{
  bend_t
    b0 = bend3p(u0, u1, v0),
    b1 = bend3p(u0, u1, v1),
    b2 = bend3p(v0, v1 ,u0),
    b3 = bend3p(v0, v1 ,u1);

  return (b0 != b1) && (b2 != b3);
}

/*
  the bend of the curve v[0] - v[1] - v[2] depends on the
  sign of the cross product of the differences of the vectors
  (since a x b = (ab sin(theta))n.
*/

bend_t bend3p(vec_t v0, vec_t v1, vec_t v2)
{
  vec_t
    w1 = vsub(v1, v0),
    w2 = vsub(v2, v1);
  return bend2v(w1, w2);
}

bend_t bend2v(vec_t w1, vec_t w2)
{
  double a = vdet(w1, w2);
  if (isnan(a))
    return rightward;
  return (a < 0 ? rightward : leftward);
}

The code in this package is copyright © 2007, 2016, 2021 J.J. Green
<j.j.green@gmx.co.uk> with the following exceptions.

# Graphics Gems

The file `src/libplot/cubic.c` contains code derived from that in the
Graphics Gems collection. The latter comes with the following mellow
licence

> EULA: The Graphics Gems code is copyright-protected. In other words,
> you cannot claim the text of the code as your own and resell it. Using
> the code is permitted in any program, product, or library,
> non-commercial or commercial. Giving credit is not required, though is
> a nice gesture. The code comes as-is, and if there are any flaws or
> problems with any Gems code, nobody involved with Gems - authors,
> editors, publishers, or webmasters - are to be held responsible.
> Basically, don't be a jerk, and remember that anything free comes with
> no guarantee.
> 
> — Graphics Gems Licence

# Aleksey Demakov's pthread barrier

The files `src/libplot/pthreadextra.*` implement a simple barrier
mechanism for POSIX threads (pthread) which is not implemented in the
pthread library shipped with some versions of OSX. The code there is a
cosmetically modified version of that by [Aleksey
Demakov](https://github.com/ademakov/DarwinPthreadBarrier) released
under the BSD 2-Clause "Simplified" Licence.

# Bats-core

The package's acceptance tests `src/test/accept` use the
[Bats-core](https://github.com/bats-core/bats-core) package, a copy of
which is installed in the `bats` subdirectory. These files are released
under an MIT-like licence.

# JUnit XSLT

The package's unit tests use the venerable
[CUnit](http://cunit.sourceforge.net/) library which can output a report
in a custom XML format; For integration with GitLab's CI facility, we
convert that file to JUnit format using the [XSLT
stylesheet](https://github.com/shawnliang/cunit-to-junit) written by
Shawn Liang, updated by Schneider Electric, released under the MIT
licence.

# MIT configuration files

The file `config/install-sh` is copyright 1991, Massachusetts Institute
of Technology, under a BSD licence.

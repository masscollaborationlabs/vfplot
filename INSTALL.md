# Executive summary

Building vfplot is quite standard: the usual

    ./configure
    make
    sudo make install

Use the `--prefix` option to `./configure` if you want to install
elsewhere than `/usr/local`.

See `./configure --help` for a full list of configuration options.

# Optional features

## POSIX threads

If the `--enable-pthread` configuration option is given and the pthread
(POSIX thread) library is found then vfplot will support multithreaded
force calculations giving a substantial speedup on multiprocessor
hardware.

This option is enabled by default, if you do not want POSIX threading
support then use the `--disable-pthread` option.

## NetCDF

If the `--enable-netcdf` configuration option given and the
[NetCDF](https://www.unidata.ucar.edu/software/netcdf/) library is found
then vfplot will be able to read GMT grd files.

This option is enabled by default, if you do not want NetCDF support
then use the `--disable-netcdf` option.

## JSON

If the `--enable-json` configuration option is given and the
[Jansson](https://github.com/akheron/jansson) library is found then
vfplot will be able to read and write JSON files.

This option is enabled by default, if you do not want JSON support then
use the `--disable-json` option.

## Matlab

If the `--enable-matlab` configuration option is given and the
[matio](https://sourceforge.net/projects/matio/) library is found then
vfplot will be able to read mat (Matlab binary) files.

This option is disabled by default.

## Gerris

If the `--enable-gerris` configuration option is given and the
[Gerris](https://gfs.sourceforge.net/) libraries are found, then vfplot
will be able to read Gerris GFS files.

This option is disabled by default.

# Platform specific notes

The program should compile on any modern POSIX operating system with a
C99 compiler. It is developed on Linux on the AMD64 architectures with
the GNU C compiler.

## Windows XP with Cygwin

Earlier version of the program were tested on Windows XP with the
[Cygwin](http://www.cygwin.com/) POSIX compatibility layer.

To build, first use the Cygwin startup.exe program to install the gcc
and make packages. Then obtain, compile and install the netcdf and
libmatio libraries listed above. Finally, configure with
`--disable-pthread` then make and install as usual.

## OS X

The POSIX thread (pthread) library supplied with the OS does not include
some functionality on which vfplot relies (in particular, *barriers*).
That functionality can be provided by a built-in implementation with the
`--enable-pthread-extra` configuration option.
